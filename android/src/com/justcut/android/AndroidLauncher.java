package com.justcut.android;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.justcut.JustCutGame;

public class AndroidLauncher extends AndroidApplication
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration()
            {{
                useAccelerometer = false;
                useCompass = false;
                useWakelock = true;
                r = 8;
                g = 8;
                b = 8;
                a = 8;
                depth = 8;
                stencil = 0;
                numSamples = 0;
            }};
		// TODO нужно грузить конфиг из файла, типа настройка языка и т.п.
        JustCutGame game = new JustCutGame();
        initialize(game, cfg);
    }
}