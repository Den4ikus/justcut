precision mediump float;

attribute vec2 a_position;
attribute vec4 a_color;

uniform mat4 u_wvp;

varying vec4 vColor;

void main() 
{
	gl_Position = u_wvp * vec4(a_position.xy, 0.0, 1.0);
	vColor = a_color;
}

#SHADER_SEPARATOR
precision mediump float;

varying vec4 vColor;

void main() 
{
	gl_FragColor = vColor;
}