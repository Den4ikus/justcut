#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

attribute vec2 a_position;

varying vec2 vTexCoord;

const vec2 HalfVec2 = vec2(0.5, 0.5);

void main() 
{
	vTexCoord = a_position * HalfVec2 + HalfVec2;
	gl_Position = vec4(a_position.xy, 0.0, 1.0);
}

#SHADER_SEPARATOR
#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

varying vec2 vTexCoord;

uniform sampler2D u_texture;
uniform vec2 u_texelSize;

const LOWP vec3 RGBToGrayscale = vec3(0.299, 0.587, 0.114);

void main() 
{
	// [x1][x2][x3]
	// [x4]this[x6]
	// [x7][x8][x9]
	vec2 texCoord1 = vec2(vTexCoord.x - u_texelSize.x, vTexCoord.y - u_texelSize.y);
	vec2 texCoord2 = vec2(vTexCoord.x + u_texelSize.x, vTexCoord.y + u_texelSize.y);

	float x1 = dot(texture2D(u_texture, texCoord1).xyz, RGBToGrayscale);
    float x2 = dot(texture2D(u_texture, vec2(vTexCoord.x, texCoord1.y)).xyz, RGBToGrayscale);
    float x3 = dot(texture2D(u_texture, vec2(texCoord2.x, texCoord1.y)).xyz, RGBToGrayscale);
    float x4 = dot(texture2D(u_texture, vec2(texCoord1.x, vTexCoord.y)).xyz, RGBToGrayscale);
    float x6 = dot(texture2D(u_texture, vec2(texCoord2.x, vTexCoord.y)).xyz, RGBToGrayscale);
    float x7 = dot(texture2D(u_texture, vec2(texCoord1.x, texCoord2.y)).xyz, RGBToGrayscale);
    float x8 = dot(texture2D(u_texture, vec2(vTexCoord.x, texCoord2.y)).xyz, RGBToGrayscale);
    float x9 = dot(texture2D(u_texture, texCoord2).xyz, RGBToGrayscale);

	// Отклик фильтра по осям вычисляется по разделенным по вертикали и горизонтали коэффициентам
	float gx = x1 - x3 + x7 - x9 + 2.0 * (x4 - x6);
	float gy = x1 - x7 + x3 - x9 + 2.0 * (x2 - x8);
	gl_FragColor = vec4(texture2D(u_texture, vTexCoord).xyz * clamp(1.15 - sqrt(gx * gx + gy * gy), 0.0, 1.0), 1.0);
}