#version 100
#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

// инвертированный размер экрана
uniform vec2 u_invScreenSize;

attribute vec2 a_position;
attribute vec2 a_texCoord;
varying vec2 texCoord;

void main() 
{
    const vec4 constVec = vec4(2.0, 2.0, -1.0, -1.0);
    texCoord = a_texCoord;
	gl_Position = vec4(a_position * u_invScreenSize * constVec.xy + constVec.zw, 0.0, 1.0);
}

#SHADER_SEPARATOR
#version 100
#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

uniform sampler2D u_texture;
// xy - смещение, zw - ширина и высота
uniform vec4 u_textureRegion;
uniform vec4 u_color;

varying vec2 texCoord;

void main()
{
    const LOWP vec2 zeroVec = vec2(0.0, 0.0);
    const LOWP vec2 oneVec = vec2(1.0, 1.0);
	// Корректируем координаты чтобы корректно делать тайлинг отдельных кусков текстуры
	vec2 repeatTexCoord = max(zeroVec, texCoord - floor(texCoord) - oneVec) + fract(texCoord);
	// На андроиде по каким-то причинам не работает альфа блендинг т.к. текстуры в этом шейдере
	// только черно-белые (во всяком случае пока) делаем блендинг вручную
	gl_FragColor = texture2D(u_texture, repeatTexCoord * u_textureRegion.zw + u_textureRegion.xy) * u_color;
}