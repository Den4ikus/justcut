#version 100
#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

attribute vec2 a_position;

varying vec2 vTexCoord;

void main() 
{
	const LOWP vec2 HalfVec2 = vec2(0.5, 0.5);
	vTexCoord = a_position * HalfVec2 + HalfVec2;
	gl_Position = vec4(a_position.xy, 0.0, 1.0);
}

#SHADER_SEPARATOR
#version 100
#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

uniform sampler2D u_texture;
uniform vec2 u_texelSize;

varying vec2 vTexCoord;

void main() 
{
	const LOWP vec3 RGBToGrayscale = vec3(0.299, 0.587, 0.114);
	// [x1][x2]
	// [x3][x4]
	vec2 texCoord2 = vec2(vTexCoord.x + u_texelSize.x, vTexCoord.y + u_texelSize.y);

    LOWP vec3 sample1 = texture2D(u_texture, vTexCoord).rgb;
    LOWP vec3 sample2 = texture2D(u_texture, vec2(texCoord2.x, vTexCoord.y)).rgb;
    LOWP vec3 sample3 = texture2D(u_texture, vec2(vTexCoord.x, texCoord2.y)).rgb;
    LOWP vec3 sample4 = texture2D(u_texture, texCoord2).rgb;
    vec2 grad = 4.0 * vec2(dot(sample1, RGBToGrayscale) - dot(sample4, RGBToGrayscale),
    					   dot(sample2, RGBToGrayscale) - dot(sample3, RGBToGrayscale));

	LOWP vec3 color = sample1 - min(sqrt(grad.x * grad.x + grad.y * grad.y), 1.0);
	gl_FragColor = vec4(color, 1.0);
}