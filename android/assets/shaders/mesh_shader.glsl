#version 100
#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

attribute vec3 a_position;
attribute vec3 a_normal;

uniform mat4 u_world;
uniform mat4 u_viewProj;
uniform vec3 u_lightPos;
uniform LOWP vec4 u_color;

varying vec3 vLightVec;
varying vec3 vNormalVec;
varying LOWP vec4 vColor;

void main() 
{
	vec4 worldPos = u_world * vec4(a_position, 1.0);
	vLightVec = normalize(u_lightPos - worldPos.xyz);
	vNormalVec = normalize((u_world * vec4(a_normal, 0.0)).xyz);
    vColor = u_color;
    gl_Position = u_viewProj * worldPos;
}

#SHADER_SEPARATOR
#version 100
#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

varying vec3 vLightVec;
varying vec3 vNormalVec;
varying LOWP vec4 vColor;

void main() 
{
    LOWP float brightness = 0.2 + min(dot(normalize(vNormalVec), normalize(vLightVec)), 1.0);
    LOWP vec3 rate = vec3((min(brightness, 0.3) - 0.3) * 3.33, (max(min(brightness, 0.7), 0.3) - 0.3) * 2.5, (max(brightness, 0.7) - 0.7) * 3.33);
    const LOWP vec3 constVec = vec3(0.4, 0.6, 0.8);
    gl_FragColor = vec4(vColor.rgb * dot(constVec, rate), vColor.a);
    //gl_FragColor = vec4(vColor.rgb * brightness, vColor.a);
}