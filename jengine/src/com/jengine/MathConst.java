package com.jengine;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Денис on 19.03.2015.
 */
public final class MathConst
{
    public static final Vector3 VECTOR_3_ZERO = Vector3.Zero;
    public static final Vector3 VECTOR_3_ONE = new Vector3(1f, 1f, 1f);

    public static final Vector3 AXIS_X = new Vector3(1f, 0f, 0f);
    public static final Vector3 AXIS_Y = new Vector3(0f, 1f, 0f);
    public static final Vector3 AXIS_Z = new Vector3(0f, 0f, 1f);

    public static final Quaternion QUATERNION_IDENTITY = new Quaternion(VECTOR_3_ONE, 0f);
}
