package com.jengine.resources;

/**
 * Ссылка на ресурс
 */
public final class ResHandle<T>
{
    public final ResDescriptor descriptor;

    private T _resource = null;
    public T get()
    {
        return _resource;
    }
    void set(T resource)
    {
        _resource = resource;
    }

    private int _referenceCount = 0;
    public int getReferenceCount()
    {
        return _referenceCount;
    }
    public void addReference()
    {
        _referenceCount++;
    }
    public void removeReference()
    {
        _referenceCount--;
    }

    public ResHandle(ResDescriptor descriptor)
    {
        this.descriptor = descriptor;
    }

    public boolean isLoaded()
    {
        return _resource != null;
    }
}
