package com.jengine.resources;/**
 * Created by Денис on 13.06.2015.
 *
 */

import com.badlogic.gdx.assets.loaders.FileHandleResolver;

/**
 *
 */
public interface IFileHandleResolverProvider
{
    FileHandleResolver getResolver();
}
