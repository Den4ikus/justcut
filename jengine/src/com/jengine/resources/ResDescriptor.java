package com.jengine.resources;

/**
 * Описатель ресурса
 */
public final class ResDescriptor
{
    /**
     * Тип ресурса
     */
    public final ResType type;

    /**
     * Уникальное имя ресурса по которому идентифицируется ресурс
     */
    public final String name;

    /**
     * Путь к ресурсу
     */
    public final String path;

    /**
     * Является ли ресурс персистентным.
     * Персистентные ресурсы загружаются при первом запросе и больше никогда не выгружаются
     */
    public final Boolean persistent;

    public ResDescriptor(ResType type, String name, String path, Boolean persistent)
    {
        this.type = type;
        this.name = name;
        this.path = path;
        this.persistent = persistent;
    }
}
