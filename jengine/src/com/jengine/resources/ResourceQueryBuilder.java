package com.jengine.resources;

import java.util.ArrayList;

/**
 *
 */
public final class ResourceQueryBuilder
{
    private ArrayList<ResourceEntry> _resources = new ArrayList<ResourceEntry>();

    public ResourceQueryBuilder request(String resName, Class resType)
    {
        _resources.add(new ResourceEntry(resName, resType));
        return this;
    }

    public ResourceQuery build()
    {
        ResourceEntry[] resources = new ResourceEntry[_resources.size()];
        _resources.toArray(resources);
        return new ResourceQuery(resources);
    }
}
