package com.jengine.resources;

/**
 *
 */
public class ResLink
{
    public final String name;
    public final ResType type;

    public ResLink(String name, ResType type)
    {
        this.name = name;
        this.type = type;
    }
}
