package com.jengine.resources.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.jengine.game.Localization;
import com.jengine.common.logs.Logger;

/**
 * Created by Денис on 10.07.2015.
 * Загрузчик файлов локализации
 */
public class LocalizationLoader extends SynchronousAssetLoader<Localization, LocalizationLoader.LocalizationLoaderParameter>
{
    public LocalizationLoader(final String localizationDirectory)
    {
        // создаем анонимный резолвер
        super(new FileHandleResolver()
        {
            @Override
            public FileHandle resolve(String fileName)
            {
                Logger.info(String.format("%s resolve %s", LocalizationLoader.class.getSimpleName(), fileName));
                try
                {
                    return Gdx.files.internal(localizationDirectory + fileName);
                }
                catch (Exception ex)
                {
                    Logger.error(String.format("%s resolve %s failed: %s", LocalizationLoader.class.getSimpleName(), fileName, ex.getMessage()));
                    return null;
                }
            }
        });
    }

    @Override
    public Localization load(AssetManager assetManager, String fileName, FileHandle file, LocalizationLoaderParameter parameter)
    {
        Logger.info(String.format("%s load %s", this.getClass().getSimpleName(), fileName));
        try
        {
            Json json = new Json();
            return json.fromJson(Localization.class, file);
        }
        catch (Exception ex)
        {
            Logger.error(String.format("%s load %s failed: %s", this.getClass().getSimpleName(), fileName, ex.getMessage()));
            return null;
        }
    }

    /**
     * Returns the assets this asset requires to be loaded first. This method may be called on a thread other than the GL thread.
     *
     * @param fileName  name of the asset to load
     * @param file      the resolved file to load
     * @param parameter parameters for loading the asset
     * @return other assets that the asset depends on and need to be loaded first or null if there are no dependencies.
     */
    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, LocalizationLoaderParameter parameter)
    {
        return null;
    }

    public static class LocalizationLoaderParameter extends AssetLoaderParameters<Localization>
    {}
}
