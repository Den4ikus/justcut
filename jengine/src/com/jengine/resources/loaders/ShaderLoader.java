package com.jengine.resources.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Array;
import com.jengine.common.logs.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Created by Сергей on 19.08.2015.
 */
public class ShaderLoader extends SynchronousAssetLoader<ShaderProgram, ShaderLoader.ShaderLoaderParameter>
{
    //строка разделяющая шейдерные программы
    private final String ShaderSeparator = "#SHADER_SEPARATOR";

    public ShaderLoader()
    {
        super(new FileHandleResolver()
        {
            @Override
            public FileHandle resolve(String fileName)
            {
                try
                {
                    FileHandle file = Gdx.files.internal(fileName);
                    Logger.info(ShaderLoader.class.getSimpleName(), "resolve", fileName, "Succeed", "resource resolved");
                    return file;
                }
                catch (Exception ex)
                {
                    Logger.warning(ShaderLoader.class.getSimpleName(), "resolve", fileName, "Failed", ex.getMessage());
                    return null;
                }
            }
        });
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, ShaderLoaderParameter parameter)
    {
        return null;
    }

    @Override
    public ShaderProgram load(AssetManager assetManager, String fileName, FileHandle file, ShaderLoaderParameter parameter)
    {
        InputStream fileInputStream = file.read();
        InputStreamReader inputStreamReader  = new InputStreamReader(fileInputStream, Charset.forName("UTF-8"));
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        StringBuilder shaderStringBuilder = new StringBuilder();

        String vertexShader;
        String fragmentShader;
        try
        {
            String lineSource;
            while(!(lineSource = bufferedReader.readLine()).startsWith(ShaderSeparator)){
                shaderStringBuilder.append(lineSource);
                shaderStringBuilder.append('\n');
            }
            vertexShader = shaderStringBuilder.toString();

            shaderStringBuilder.setLength(0);

            while((lineSource = bufferedReader.readLine()) != null){
                shaderStringBuilder.append(lineSource);
                shaderStringBuilder.append('\n');
            }
            fragmentShader = shaderStringBuilder.toString();

            ShaderProgram shader = new ShaderProgram(vertexShader, fragmentShader);
            Logger.info(ShaderLoader.class.getSimpleName(), "load", fileName, "Succeed", "resource loaded");

            return shader;
        }
        catch (IOException ex)
        {
            Logger.warning(ShaderLoader.class.getSimpleName(), "load", fileName, "Failed", ex.getMessage());
            return null;
        }
    }

    public static class ShaderLoaderParameter extends AssetLoaderParameters<ShaderProgram>
    {}
}
