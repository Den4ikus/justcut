package com.jengine.resources.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.jengine.common.logs.Logger;
import com.jengine.scripts.ScriptFile;

/**
 * Created by Денис on 13.06.2015.
 *
 */
public class ScriptSourceLoader extends SynchronousAssetLoader<ScriptFile, AssetLoaderParameters<ScriptFile>>
{
    public ScriptSourceLoader(final String scriptsDirectory)
    {
        // создаем анонимный резолвер
        super(new FileHandleResolver()
        {
            @Override
            public FileHandle resolve(String fileName)
            {
                Logger.info(String.format("%s resolve %s", ScriptSourceLoader.class.getSimpleName(), fileName));
                try
                {
                    return Gdx.files.internal(scriptsDirectory + fileName);
                }
                catch (Exception ex)
                {
                    Logger.error(String.format("%s resolve %s failed: %s", ScriptSourceLoader.class.getSimpleName(), fileName, ex.getMessage()));
                    return null;
                }
            }
        });
    }

    @Override
    public ScriptFile load(AssetManager assetManager, String fileName, FileHandle file, AssetLoaderParameters parameter)
    {
		return null;
    }

    /**
     * Returns the assets this asset requires to be loaded first. This method may be called on a thread other than the GL thread.
     *
     * @param fileName  name of the asset to load
     * @param file      the resolved file to load
     * @param parameter parameters for loading the asset
     * @return other assets that the asset depends on and need to be loaded first or null if there are no dependencies.
     */
    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, AssetLoaderParameters parameter)
    {
        return null;
    }
}
