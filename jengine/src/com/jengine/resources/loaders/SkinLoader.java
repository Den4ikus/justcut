package com.jengine.resources.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.jengine.common.logs.Logger;

/**
 * Created by ����� on 03.09.2015.
 * ��������� UI ������
 */
public class SkinLoader extends SynchronousAssetLoader<Skin, SkinLoader.SkinLoaderParameter>
{
    public SkinLoader(final String skinDirectory)
    {
        // ������� ��������� ��������
        super(new FileHandleResolver()
        {
            @Override
            public FileHandle resolve(String fileName)
            {
                Logger.info(String.format("%s resolve %s", SkinLoader.class.getSimpleName(), fileName));
                try
                {
                    return Gdx.files.internal(skinDirectory + fileName);
                }
                catch (Exception ex)
                {
                    Logger.error(String.format("%s resolve %s failed: %s", SkinLoader.class.getSimpleName(), fileName, ex.getMessage()));
                    return null;
                }
            }
        });
    }

    @Override
    public Skin load(AssetManager assetManager, String fileName, FileHandle file, SkinLoaderParameter parameter)
    {
        Logger.info(String.format("%s load %s", this.getClass().getSimpleName(), fileName));
        try
        {
            return new Skin(file);
        }
        catch (Exception ex)
        {
            Logger.error(String.format("%s load %s failed: %s", this.getClass().getSimpleName(), fileName, ex.getMessage()));
            return null;
        }
    }

    /**
     * Returns the assets this asset requires to be loaded first. This method may be called on a thread other than the GL thread.
     *
     * @param fileName  name of the asset to load
     * @param file      the resolved file to load
     * @param parameter parameters for loading the asset
     * @return other assets that the asset depends on and need to be loaded first or null if there are no dependencies.
     */
    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, SkinLoaderParameter parameter)
    {
        return null;
    }

    public static class SkinLoaderParameter extends AssetLoaderParameters<Skin>
    {}
}
