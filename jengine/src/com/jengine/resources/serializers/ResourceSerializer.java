package com.jengine.resources.serializers;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.jengine.resources.ResDescriptor;
import com.jengine.resources.ResType;

/**
 *
 */
public final class ResourceSerializer implements Json.Serializer<ResDescriptor>
{
    @Override
    public void write(Json json, ResDescriptor object, Class knownType)
    {
        json.writeObjectStart();
        json.writeValue("type", object.type);
        json.writeValue("name", object.name);
        json.writeValue("path", object.path);
        json.writeValue("persistent", object.persistent);
        json.writeObjectEnd();
    }

    @Override
    public ResDescriptor read(Json json, JsonValue jsonData, Class type)
    {
        ResType t = ResType.valueOf(jsonData.get("type").asString());
        String name = jsonData.get("name").asString();
        String path = jsonData.get("path").asString();
        boolean persistent = jsonData.get("persistent").asBoolean();
        return new ResDescriptor(t, name, path, persistent);
    }
}
