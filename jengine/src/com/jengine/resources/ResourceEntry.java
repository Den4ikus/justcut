package com.jengine.resources;

/**
 *
 */
final class ResourceEntry
{
    public final String name;
    public final Class type;

    public ResourceEntry(String name, Class type)
    {
        this.name = name;
        this.type = type;
    }
}
