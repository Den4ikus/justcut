package com.jengine.resources;

/**
 *
 */
public enum ResType
{
    CustomResource,
    Shader,
    Texture,
    TextureAtlas,
    Skin,
    Font,
    Sound
}
