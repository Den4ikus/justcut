package com.jengine.resources;

import com.jengine.common.Event;
import com.jengine.common.EventArgs;
import com.jengine.common.IEvent;

/**
 *
 */
public final class ResourceQuery
{
    public final ResourceEntry[] resources;

    //region Events

    public final IEvent<EventArgs> eventComplete = new Event<EventArgs>();

    public void preformComplete()
    {
        ((Event)eventComplete).doEvent(this, EventArgs.Empty);
    }

    //endregion

    public ResourceQuery(ResourceEntry ... resources)
    {
        this.resources = resources;
    }
}