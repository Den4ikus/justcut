package com.jengine.resources;

import java.util.HashMap;

/**
 *
 */
public final class ResourceDatabase
{
	// начинается не с подчеркивания т.к. класс сериализуется в json
	private HashMap<String, ResDescriptor> resources = new HashMap<String, ResDescriptor>();

	public ResourceDatabase(HashMap<String, ResDescriptor> resources)
	{
		this.resources = resources;
	}

	public ResDescriptor get(String resName)
	{
		return resources.get(resName);
	}
}

/*public final class ResourceDatabase implements Json.Serializable
{
	private final class ResEntry
	{
		public final String className;
		public Object resource;

		public ResEntry(String className)
		{
			this.className = className;
		}
	}

	private final HashMap<String, ResEntry> _resources = new HashMap<String, ResEntry>();
	
	public void load(AssetManager assetManager)
	{
		for(Map.Entry<String, ResEntry> entry : _resources.entrySet())
		{
			try
			{
				Class resClass = Class.forName(entry.getValue().className);
				assetManager.load(entry.getKey(), resClass);
			}
			catch(ClassNotFoundException ex)
			{
				Logger.debug("Не удалось загрузить ресурс");
			}

		}

		assetManager.finishLoading();

		for(Map.Entry<String, ResEntry> entry : _resources.entrySet())
			entry.getValue().resource = assetManager.get(entry.getKey());
	}

	public void unload(AssetManager assetManager)
	{
		for(Map.Entry<String, ResEntry> entry : _resources.entrySet())
		{
			if(entry.getValue().resource != null)
				assetManager.unload(entry.getKey());
		}
	}
	
	public void addResource(String resName, Class resClass)
	{
		ResEntry entry = new ResEntry(resClass.getName());
		_resources.put(resName, entry);
	}
	
	public <T> T getResource(String resName, Class<T> resClass)
	{
		ResEntry entry = _resources.get(resName);
		return entry != null ? (T)entry.resource : null;
	}

	//region Json.Serializable

	@Override
	public void write(Json json)
	{
		for(Map.Entry<String, ResEntry> entry : _resources.entrySet())
			json.writeValue(entry.getKey(), entry.getValue().className);
	}

	@Override
	public void read(Json json, JsonValue jsonData)
	{
		for(JsonValue val : jsonData)
		{
			ResEntry entry = new ResEntry(val.child().asString());
			_resources.put(val.child().name(), entry);
		}
	}

	//endregion
}*/
