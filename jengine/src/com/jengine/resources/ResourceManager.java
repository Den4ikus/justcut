package com.jengine.resources;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.jengine.common.IDisposable;
import com.jengine.resources.loaders.ShaderLoader;

import java.util.HashMap;

/**
 *
 */
public final class ResourceManager implements IDisposable
{
    private final static HashMap<String, Class> ResTypeClassRelations = new HashMap<String, Class>()
        {{
            put(ResType.Shader.name(), ShaderProgram.class);
            put(ResType.Texture.name(), Texture.class);
            put(ResType.TextureAtlas.name(), TextureAtlas.class);
            put(ResType.Skin.name(), Skin.class);
            put(ResType.Font.name(), BitmapFont.class);
        }};

    private final AssetManager _assetManager = new AssetManager()
        {{
            //setLoader(Localization.class, new LocalizationLoader());
            setLoader(ShaderProgram.class, new ShaderLoader());
            //setLoader(Skin.class, new SkinLoader());
        }};;

    private ResourceDatabase _resourceDb;
    private HashMap<String, ResHandle> _handles = new HashMap<String, ResHandle>();

    public ResourceManager(ResourceDatabase resourceDb)
    {
        _resourceDb = resourceDb;
    }

    //TODO: переименовать в load/unload
    public ResHandle get(String resName)
    {
        ResHandle handle = _handles.get(resName);
        if(handle == null)
        {
            ResDescriptor resource = _resourceDb.get(resName);
            if(resource != null)
            {
                Class resClass = ResTypeClassRelations.get(resource.type.name());
                _assetManager.load(resource.path, resClass);

                handle = new ResHandle(resource);
                _handles.put(resName, handle);
            }
            //TODO добавить exception
            //else throw new ResourceNotFoundException(resName);
        }
        handle.addReference();
        return handle;
    }

    public <T> ResHandle<T> get(String resName, Class<T> resType)
    {
        ResHandle handle = _handles.get(resName);
        if(handle == null)
        {
            ResDescriptor resource = _resourceDb.get(resName);
            if(resource != null)
            {
                _assetManager.load(resource.path, resType);

                handle = new ResHandle(resource);
                _handles.put(resName, handle);
            }
            //TODO добавить exception
            //else throw new ResourceNotFoundException(resName);
        }
        handle.addReference();
        return handle;
    }

    public void free(ResHandle resHandle)
    {
        resHandle.removeReference();
    }

    public <T, P extends AssetLoaderParameters<T>> void setLoader (Class<T> resType, AssetLoader<T, P> loader)
    {
        _assetManager.setLoader(resType, loader);
    }

    public float getProgress()
    {
        return _assetManager.getProgress();
    }

    public void waitLoad()
    {
        _assetManager.finishLoading();
        refresh();
    }

    public void refresh()
    {
        for(ResHandle handle : _handles.values())
        {
            if(!handle.isLoaded() && handle.getReferenceCount() > 0)
            {
                if(_assetManager.isLoaded(handle.descriptor.path))
                    handle.set(_assetManager.get(handle.descriptor.path));
            }
        }
    }

    public boolean update()
    {
        boolean resourcesLoaded = _assetManager.update();
        if(resourcesLoaded) refresh();
        return resourcesLoaded;
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_assetManager != null)
            _assetManager.dispose();
    }
}
