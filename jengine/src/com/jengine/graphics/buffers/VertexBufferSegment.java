package com.jengine.graphics.buffers;

import com.jengine.graphics.IVertexData;
import com.jengine.graphics.vertices.IVertexDeclaration;

/**
 * Сегмент вершинного буфера
 */
public final class VertexBufferSegment implements IVertexData
{
	public void setVertexOffset(int vertexOffset)
	{
		_vertexOffset = vertexOffset;
	}
	
	public void setVertexCount(int vertexCount)
	{
		_vertexCount = vertexCount;
	}
	
    public VertexBufferSegment(VertexBuffer vertexBuffer)
    {
		this(vertexBuffer, 0, 0);
    }
	
    public VertexBufferSegment(VertexBuffer vertexBuffer, int vertexOffset, int vertexCount)
    {
        _vertexBuffer = vertexBuffer;
		_vertexDecl = vertexBuffer.getVertexDeclaration();
        _vertexOffset = vertexOffset;
        _vertexCount = vertexCount;
    }

	public void setVertices(float[] vertexData)
    {
		this.setVertices(vertexData, 0, vertexData.length);
    }
	
    public void setVertices(float[] vertexData, int srcOffset, int count)
    {
		int vertexStride = _vertexDecl.getVertexStride();
		_vertexBuffer.updateVertices(_vertexOffset * vertexStride, vertexData, srcOffset, count);
        _vertexCount = count / vertexStride;
    }
	
	//region IVertexData
	
	private VertexBuffer _vertexBuffer;
	private IVertexDeclaration _vertexDecl;
	private int _vertexOffset;
	private int _vertexCount;
	
	/**
     * Возвращает описание типа вершин
     */
	@Override
    public IVertexDeclaration getVertexDeclaration()
	{
		return _vertexDecl;
	}
	
	/**
     * Возвращает источник который хранит данные о вершинах
     */
	@Override
	public VertexBuffer getSource()
	{
		return _vertexBuffer;
	}
	
	/**
     * Возвращает смещение по которому располагаются данные о вершинах
     */
	@Override
	public int getVertexOffset()
	{
		return _vertexOffset;
	}
	
	/**
     * Возвращает количество вершин
     */
	@Override
	public int getVertexCount()
	{
		return _vertexCount;
	}
	
	//endregion
}
