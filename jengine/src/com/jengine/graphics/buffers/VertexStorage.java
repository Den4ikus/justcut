package com.jengine.graphics.buffers;

import com.jengine.common.IDisposable;
import com.jengine.common.IFactory;
import com.jengine.common.Pool;
import com.jengine.graphics.vertices.IVertexDeclaration;
import com.jengine.graphics.vertices.IVertexType;
import com.jengine.graphics.vertices.VertexSerializer;

/**
 * Обертка над вершинным буфером. Обеспечивает управление сегментами и их повторное использование
 */
public final class VertexStorage implements IDisposable
{
    private final class VertexBufferSegmentFactory implements IFactory<VertexBufferSegment>
    {
        private VertexBuffer _vertexBuffer;
        private int _currentOffset = 0;
        private int _segmentSize;

        public VertexBufferSegmentFactory(VertexBuffer vertexBuffer, int segmentsCount)
        {
            _vertexBuffer = vertexBuffer;
            _segmentSize = vertexBuffer.getVertexCount() / segmentsCount;
        }

        /**
         * Создает новый экземпляр объекта и возвращает его
         */
        @Override
        public VertexBufferSegment create()
        {
            if(_currentOffset < _vertexBuffer.getVertexCount())
            {
                VertexBufferSegment segment = new VertexBufferSegment(_vertexBuffer, _currentOffset, _segmentSize);
                _currentOffset += _segmentSize;
                return segment;
            }
            return null;
        }
    }

    private VertexBuffer _vertexBuffer;
    private VertexSerializer _serializer;
    private Pool<VertexBufferSegment> _segmentPool;

    public VertexStorage(boolean isStatic, int segmentSize, int segmentsCount, IVertexDeclaration vertexDeclaration)
    {
        _vertexBuffer = new VertexBuffer(segmentSize * segmentsCount, vertexDeclaration, isStatic);
        _serializer = new VertexSerializer(segmentSize * vertexDeclaration.getVertexStride());
        _segmentPool = new Pool<VertexBufferSegment>(new VertexBufferSegmentFactory(_vertexBuffer, segmentsCount));
    }


    public <T extends IVertexType> VertexBufferSegment allocate(T[] vertices)
    {
        VertexBufferSegment segment = _segmentPool.get();
        int floatsCount = _serializer.serialize(vertices, true);
        segment.setVertices(_serializer.getBuffer(), 0 , floatsCount);
        return segment;
    }

    public void release(VertexBufferSegment vbSegment)
    {
        _segmentPool.put(vbSegment);
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_vertexBuffer != null)
            _vertexBuffer.dispose();
    }
}
