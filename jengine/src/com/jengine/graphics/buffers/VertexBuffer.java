package com.jengine.graphics.buffers;

import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;
import com.jengine.common.IDisposable;
import com.jengine.graphics.IVertexData;
import com.jengine.graphics.vertices.IVertexDeclaration;

/**
 * Вершинный буфер
 */
public final class VertexBuffer implements IVertexData, IDisposable
{
	private VertexBufferObject _vertexBuffer;
	private IVertexDeclaration _vertexDecl;
	private int _vertexCount;

	public VertexBuffer(int vertexCount, IVertexDeclaration vertexDecl, boolean isStatic)
	{
		_vertexBuffer = new VertexBufferObject(isStatic, vertexCount, vertexDecl.getAttributes());
		_vertexDecl = vertexDecl;
		_vertexCount = vertexCount;
		//TODO что за придурки делали VertexBufferObject для вызова updateVertices сначала нужно записать в буфер какую нибудь фигню
		//записываем пустой массив (может когда-нибудь написать свой класс с блэк джеком и шлюхами)
		float[] emptyData = new float[vertexCount * vertexDecl.getVertexStride()];
		_vertexBuffer.setVertices(emptyData, 0, emptyData.length);
	}
	
	public void bind(ShaderProgram shader)
	{
		_vertexBuffer.bind(shader);
	}
	
	public void unbind(ShaderProgram shader)
	{
		_vertexBuffer.unbind(shader);
	}
	
	public void setVertices(float[] vertexData, int offset, int count)
	{
		_vertexBuffer.setVertices(vertexData, offset, count);
	}
	
	public void updateVertices(int targetOffset, float[] vertexData, int sourceOffset, int count)
	{
		_vertexBuffer.updateVertices(targetOffset, vertexData, sourceOffset, count);
	}
	
	/**
	 * Releases all resources of this object.
	 */
	@Override
	public void dispose()
	{
		if(_vertexBuffer != null)
		{
			_vertexBuffer.dispose();
			_vertexBuffer = null;
		}
	}
	
	//region IVertexData
	
	/**
     * Возвращает описание типа вершин
     */
	@Override
    public IVertexDeclaration getVertexDeclaration()
	{
		return _vertexDecl;
	}
	
	/**
     * Возвращает источник который хранит данные о вершинах
     */
	@Override
	public VertexBuffer getSource()
	{
		return this;
	}
	
	/**
     * Возвращает смещение по которому располагаются данные о вершинах
     */
	@Override
	public int getVertexOffset()
	{
		return 0;
	}
	
	/**
     * Возвращает количество вершин
     */
	@Override
	public int getVertexCount()
	{
		return _vertexCount;
	}
	
	//endregion
}
