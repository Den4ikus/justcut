//package com.jengine.graphics.buffers;
//
//import com.badlogic.gdx.graphics.glutils.IndexBufferObject;
//import com.badlogic.gdx.graphics.glutils.ShaderProgram;
//import com.badlogic.gdx.graphics.glutils.VertexBufferObject;
//import com.jengine.common.IDisposable;
//import com.jengine.graphics.vertices.IVertexDeclaration;
//
///**
// * Вершинный буфер
// */
//public final class VertexBuffer implements IVertexData, IDisposable
//{
//	private final int _id;
//	private IndexBufferObject _indexBuffer;
//
//	public int getId()
//	{
//		return _id;
//	}
//
//	public VertexBuffer(int id, int vertexCount, IVertexDeclaration vertexDecl, boolean isStatic)
//	{
//		_id = id;
//		_vertexBuffer = new VertexBufferObject(isStatic, vertexCount, vertexDecl.getAttributes());
//		//TODO что за придурки делали VertexBufferObject для вызова updateVertices сначала нужно записать в буфер какую нибудь фигню
//		//записываем пустой массив (ваще надо написать свой класс с блэк джеком и шлюхами)
//		float[] emptyData = new float[vertexCount * vertexDecl.getVertexStride()];
//		vertexBuffer.setVertices(emptyData, 0, emptyData.length);
//		_vertexDecl = vertexDecl;
//	}
//
//	public void bind(ShaderProgram shader)
//	{
//		_vertexBuffer.bind(shader);
//	}
//
//	public void unbind(ShaderProgram shader)
//	{
//		_vertexBuffer.unbind(shader);
//	}
//
//	public void setVertices(float[] vertexData, int offset, int count)
//	{
//		_vertexBuffer.setVertices(vertexData, offset, count);
//	}
//
//	public void updateVertices(int targetOffset, float[] vertexData, int sourceOffset, int count)
//	{
//		_vertexBuffer.updateVertices(targetOffset, vertexData, sourceOffset, count);
//	}
//
//	/**
//	 * Releases all resources of this object.
//	 */
//	@Override
//	public void dispose()
//	{
//		if(_vertexBuffer != null)
//		{
//			_vertexBuffer.dispose();
//			_vertexBuffer = null;
//		}
//	}
//
//	//region IVertexData
//
//	private IVertexDeclaration _vertexDecl;
//	private int _vertexCount;
//
//	/**
//     * Возвращает описание типа вершин
//     */
//	@Override
//    public IVertexDeclaration getVertexDeclaration()
//	{
//		return _vertexDecl;
//	}
//
//	/**
//     * Возвращает источник который хранит данные о вершинах
//     */
//	@Override
//	public VertexBuffer getSource()
//	{
//		return this;
//	}
//
//	/**
//     * Возвращает смещение по которому располагаются данные о вершинах
//     */
//	@Override
//	int getOffset()
//	{
//		return 0;
//	}
//
//	/**
//     * Возвращает количество вершин
//     */
//	@Override
//	int getVertexCount()
//	{
//		return _vertexCount;
//	}
//
//	//endregion
//}
