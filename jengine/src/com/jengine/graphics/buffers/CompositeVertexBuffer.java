package com.jengine.graphics.buffers;

///**
// * Представляет вершинный буфер разбитый на сегменты
// */
//public final class CompositeVertexBuffer extends VertexBuffer
//{
//    private ArrayList<VertexBufferSegment> _allocatedSegments = new ArrayList<VertexBufferSegment>();
//    private ArrayList<VertexBufferSegment> _freeSegments = new ArrayList<VertexBufferSegment>();
//
//    public CompositeVertexBuffer(int vertexCount, int segmentSize, IVertexDeclaration vertexDecl, boolean isStatic)
//    {
//        super(vertexCount, vertexDecl, isStatic);
//        if(vertexCount % segmentSize != 0) throw new IllegalArgumentException();
//        for(int i = 0; i < vertexCount; i += segmentSize)
//        {
//            VertexBufferSegment segment = new VertexBufferSegment(this,
//                    i * vertexDecl.getVertexStride(),
//                    segmentSize * vertexDecl.getVertexStride());
//            _freeSegments.add(segment);
//        }
//    }
//
//    public VertexBufferSegment getSegment()
//    {
//        if(_freeSegments.size() > 0)
//        {
//            int lastIndex = 0;//_freeSegments.size() - 1;
//            VertexBufferSegment segment = _freeSegments.get(lastIndex);
//            _freeSegments.remove(lastIndex);
//            _allocatedSegments.add(segment);
//            return segment;
//        }
//		return null;
//    }
//
//    public void putSegment(VertexBufferSegment segment)
//    {
//        _allocatedSegments.remove(segment);
//        _freeSegments.add(segment);
//    }
//}
