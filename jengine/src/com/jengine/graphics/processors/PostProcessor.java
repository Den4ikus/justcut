package com.jengine.graphics.processors;

import com.badlogic.gdx.graphics.Texture;
import com.jengine.common.IDisposable;
import com.jengine.graphics.RenderTarget;
import com.jengine.graphics.buffers.VertexBuffer;
import com.jengine.graphics.shaders.BaseShader;
import com.jengine.graphics.vertices.VertexPosition2;

/**
 * Базовый класс для реализации пост-процессинга
 */
public abstract class PostProcessor<TShader extends BaseShader> implements IDisposable
{
    // Сериализованный массив вершин VertexPosition2 описывающий квадрат из двух треугольников во весь экран
    // 2-3
    // 1-4
    private static float[] QuadVertices = new float[]{
            -1f,-1f, // 1
            -1f, 1f, // 2
             1f, 1f, // 3
            -1f,-1f, // 1
             1f, 1f, // 3
             1f,-1f  // 4
    };

    private VertexBuffer _vb;
    private TShader _shader;

    public PostProcessor(TShader shader)
    {
        _vb = new VertexBuffer(6, VertexPosition2.declaration, true);
        _vb.setVertices(QuadVertices, 0, QuadVertices.length);
        _shader = shader;
    }

    /**
     * Выполняет пост-процессинг изображения
     * @param src текстура - источник
     */
    public void render(Texture src)
    {
        this.render(src, null);
    }

    /**
     * Выполняет пост-процессинг изображения
     * @param src текстура - источник
     * @param target рендер таргет - приемник
     */
    public void render(Texture src, RenderTarget target)
    {
        doChangeParameters(_shader, src);
        _shader.begin(target, true);
        _shader.render(BaseShader.PrimitiveType.Triangles, _vb, false);
        _shader.end(true);
    }

    @Override
    public void dispose()
    {
        if(_vb != null)
        {
            _vb.dispose();
            _vb = null;
        }
    }

    /**
     * Выполняет изменение параметров рендеринга шейдера
     */
    protected abstract void doChangeParameters(TShader shader, Texture source);
}
