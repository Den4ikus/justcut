package com.jengine.graphics;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 *
 */
public class PostProcessStage implements IPostProcessStage
{
    private RenderTarget _rt;
    private ShaderProgram _shader;

    public PostProcessStage(Pixmap.Format format, int rtWidth, int rtHeight, ShaderProgram shader)
    {
        _rt = new RenderTarget(format, rtWidth, rtHeight, false);
        _shader = shader;
    }

    @Override
    public RenderTarget getRenderTarget()
    {
        return _rt;
    }

    public void render(SpriteBatch batch, RenderTarget source)
    {
        _rt.bind();

        batch.begin();
        batch.setShader(_shader);
        batch.draw(source.getTexture(), 0, 0, _rt.getWidth(), _rt.getHeight());
        batch.end();

        _rt.unbind();
    }

    protected void setParams(ShaderProgram shader)
    {

    }
}
