package com.jengine.graphics.shaders;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Шейдер для рендеринга текстурированных мешей в экранном пространстве
 */
public final class ScreenSpaceTextureShader extends BaseShader
{
    private final int _u_texture;
    private final int _u_textureRegion;
    private final int _u_invScreenSize;
    private final int _u_color;

    private Texture _texture;
    private float[] _textureRegion = new float[4];
    private float[] _invScreenSize = new float[2];
    private float[] _color = new float[4];

    public Texture getTexture()
    {
        return _texture;
    }
    public void setTexture(Texture texture)
    {
        _texture = texture;
        _textureRegion[0] = 0f;
        _textureRegion[1] = 0f;
        _textureRegion[2] = 1f;
        _textureRegion[3] = 1f;
    }

    public void setTextureRegion(TextureRegion texRegion)
    {
        _texture = texRegion.getTexture();
        _textureRegion[0] = texRegion.getU();
        _textureRegion[1] = texRegion.getV();
        _textureRegion[2] = texRegion.getU2() - texRegion.getU();
        _textureRegion[3] = texRegion.getV2() - texRegion.getV();
    }

    public void setInvScreenSize(float invWidth, float invHeigth)
    {
        _invScreenSize[0] = invWidth;
        _invScreenSize[1] = invHeigth;
    }

    public void setColor(Color color)
    {
        _color[0] = color.r;
        _color[1] = color.g;
        _color[2] = color.b;
        _color[3] = color.a;
    }

    public ScreenSpaceTextureShader(ShaderProgram shader)
    {
        super(shader);
        _u_texture = shader.getUniformLocation("u_texture");
        _u_textureRegion = shader.getUniformLocation("u_textureRegion");
        _u_invScreenSize = shader.getUniformLocation("u_invScreenSize");
        _u_color = shader.getUniformLocation("u_color");
    }

    /**
     * Передает текущие настройки рендеринга в шейдер перед процессом рендеринга
     * @param shader
     */
    @Override
    protected void doSetParameters(ShaderProgram shader)
    {
        // Размеры экрана одинаковые для одного цикла рендеринга
        shader.setUniform2fv(_u_invScreenSize, _invScreenSize, 0, 2);
    }

    /**
     * Передает текущие настройки рендеринга в шейдер в процессе рендеринга
     * @param shader
     */
    @Override
    protected void doChangeParameters(ShaderProgram shader)
    {
        _texture.bind(0);
        shader.setUniformi(_u_texture, 0);
        shader.setUniform4fv(_u_textureRegion, _textureRegion, 0, 4);
        shader.setUniform4fv(_u_color, _color, 0, 4);
    }
}
