package com.jengine.graphics.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.jengine.graphics.IVertexData;
import com.jengine.graphics.RenderTarget;
import com.jengine.graphics.buffers.VertexBuffer;

/**
 * Базовый класс для шейдеров
 */
public abstract class BaseShader
{
	public final class PrimitiveType
	{
		public static final int Points = GL20.GL_POINTS;
		public static final int Lines = GL20.GL_LINES;
		public static final int LineStrip = GL20.GL_LINE_STRIP;
		public static final int Triangles = GL20.GL_TRIANGLES;
		public static final int TriangleStrip = GL20.GL_TRIANGLE_STRIP;
		public static final int TriangleFan = GL20.GL_TRIANGLE_FAN;
	}

    private ShaderProgram _shader;
	private RenderTarget _rt;
	private VertexBuffer _vb;

    public BaseShader(ShaderProgram shader)
    {
        _shader = shader;
    }

	/**
	 * Выполняет биндинг рендер таргета, вызывается перед началом отрисовки объектов
	 */
	public final void begin(RenderTarget target, boolean bindTarget)
	{
		if(bindTarget && target != null)
		{
			target.bind();
			_rt = target;
		}

		_shader.begin();
		// Применение параметров работает только после биндинга шейдера!
		doSetParameters(_shader);
	}
	
	/**
	 * Сбрасывает биндинг рендер таргета, вызывается после окончания отрисовки объектов
	 */
	public final void end(boolean resetTarget)
	{
		if(_vb != null)
		{
			_vb.unbind(_shader);
			_vb = null;
		}

		_shader.end();
		if(resetTarget && _rt != null)
		{
			_rt.unbind();
			_rt = null;
		}
	}
	
	/**
     * Выполняет рендеринг геометрии без смены параметров шейдера
	 * @param primitiveType тип примитивов для отрисовки
     * @param vertexData вершинные данные
     */
    public final void render(int primitiveType, IVertexData vertexData)
    {
		this.render(primitiveType, vertexData, false);
    }
	
    /**
     * Выполняет рендеринг геометрии
	 * @param primitiveType тип примитивов для отрисовки
     * @param vertexData вершинные данные
	 * @param changeParameters будет ли выполнена смена параметров шейдера, по-умолчанию false
     */
    public final void render(int primitiveType, IVertexData vertexData, boolean changeParameters)
    {
		// Меняем параметры шейдера если необходимо
		if(changeParameters) doChangeParameters(_shader);
	
		// Делаем ребиндинг, если геометрия находится в другом вершинном буфере
		if(_vb != vertexData.getSource())
		{
			if(_vb != null) _vb.unbind(_shader);
			_vb = vertexData.getSource();
			_vb.bind(_shader);
		}
		
		Gdx.gl20.glDrawArrays(primitiveType, vertexData.getVertexOffset(), vertexData.getVertexCount());
    }

	/**
	 * Выполняет рендеринг геометрии
	 * @param primitiveType тип примитивов для отрисовки
	 * @param vertexData вершинные данные
	 * @param vertexOffset смещение
	 * @param vertexCount количество вершин
	 * @param changeParameters будет ли выполнена смена параметров шейдера, по-умолчанию false
	 */
	public final void render(int primitiveType, IVertexData vertexData, int vertexOffset, int vertexCount, boolean changeParameters)
	{
		// Меняем параметры шейдера если необходимо
		if(changeParameters) doChangeParameters(_shader);

		// Делаем ребиндинг, если геометрия находится в другом вершинном буфере
		if(_vb != vertexData.getSource())
		{
			if(_vb != null) _vb.unbind(_shader);
			_vb = vertexData.getSource();
			_vb.bind(_shader);
		}

		Gdx.gl20.glDrawArrays(primitiveType, vertexData.getVertexOffset() + vertexOffset, vertexCount);
	}

	/**
	 * Передает все текущие настройки рендеринга в шейдер
	 */
	public void applyParameters()
	{
		doSetParameters(_shader);
		doChangeParameters(_shader);
	}

	/**
     * Передает текущие настройки рендеринга в шейдер перед процессом рендеринга
     */
    protected abstract void doSetParameters(ShaderProgram shader);
	
    /**
     * Передает текущие настройки рендеринга в шейдер в процессе рендеринга
     */
    protected abstract void doChangeParameters(ShaderProgram shader);
}
