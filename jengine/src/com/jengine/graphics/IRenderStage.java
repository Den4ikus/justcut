package com.jengine.graphics;

import com.jengine.common.IDisposable;
import com.jengine.game.Scene;

/**
 * Интерфейс стадии рендеринга геометрии
 */
public interface IRenderStage<T extends Scene> extends IDisposable
{
    /**
     * Возвращает RenderTarget в который идет отрисовка, для окончательных стадий рендеринга возвращает null
     */
    RenderTarget getRenderTarget();

    /**
     * Изменяет размер RenderTarget в соответствии с размером экрана
     * @param width ширина экрана в пикселах
     * @param height высота экрана в пикселах
     */
    void resize(int width, int height);

    /**
     * Выполняет отрисовку сцены в RenderTarget
     * @param scene
     */
    void render(T scene);
}
