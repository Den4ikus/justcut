package com.jengine.graphics;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Интерфейс стадии пост-обработки
 */
public interface IPostProcessStage
{
    RenderTarget getRenderTarget();
    void render(SpriteBatch batch, RenderTarget source);
}
