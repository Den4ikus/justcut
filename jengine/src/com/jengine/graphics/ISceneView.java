package com.jengine.graphics;

import com.jengine.common.IDisposable;
import com.jengine.game.Scene;
import com.jengine.resources.ResourceManager;

/**
 * Интерфейс представления сцены
 * @param <T> тип сцены
 */
public interface ISceneView<T extends Scene> extends IDisposable
{
    /**
     * Вызывается для инициализации после загрузки ресурсов
     */
    void initialize();

    /**
     * Вызывается для загрузки ресурсов
     * @param resourceManager
     */
    void load(ResourceManager resourceManager);

    /**
     * Вызывается для выгрузки ресурсов
     * @param resourceManager
     */
    void unload(ResourceManager resourceManager);

    /**
     * Вызывается при изменении размеров окна
     */
    void resize(int width, int height);

    /**
     * Реализует рендеринг сцены
     * @param scene
     */
    void render(T scene);
}
