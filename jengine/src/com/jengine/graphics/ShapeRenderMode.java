package com.jengine.graphics;

/**
 * Режим рендеринга фигур
 */
public enum ShapeRenderMode
{
	/**
	 * Рендеринг контуров
	 */
	Line,
	/**
	 * Рендеринг сплошных объектов
	 */
	Solid
}
