package com.jengine.graphics;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.utils.Disposable;

/**
 * Представляет цель рендеринга для отрисовки в текстуру
 */
public final class RenderTarget implements Disposable
{
	private FrameBuffer _frame;
	private TextureRegion _frameTexture;
	
	//public final Viewport viewport = new Viewport();
	
	public int getWidth()
	{
		return _frameTexture.getRegionWidth();
	}
	
	public int getHeight()
	{
		return _frameTexture.getRegionHeight();
	}
	
	public TextureRegion getTexture()
	{
		return _frameTexture;
	}
	
	//public Viewport getViewport()
	
	public RenderTarget(Pixmap.Format format, int width, int height, boolean hasDepth)
	{
		_frame = new FrameBuffer(format, width, height, hasDepth);
		_frameTexture = new TextureRegion(_frame.getColorBufferTexture());
		//viewport.setSize(width, height);
	}
	
	public void bind()
	{
		//viewport.apply();
		_frame.begin();
	}
	
	public void unbind()
	{
		_frame.end();
	}
	
	//region Disposable
	
	@Override
	public void dispose()
	{
		if(_frame != null)
			_frame.dispose();
		
		//todo frameTexture не надо диспозить?
	}
	
	//endregion
}
