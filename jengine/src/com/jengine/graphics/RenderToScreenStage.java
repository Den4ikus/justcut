package com.jengine.graphics;

import com.jengine.game.Scene;

/**
 * Стадия рендеринга геометрии на экран
 */
public abstract class RenderToScreenStage<T extends Scene> implements IRenderStage<T>
{
    @Override
    public RenderTarget getRenderTarget()
    {
        return null;
    }

    /**
     * Изменяет размер RenderTarget в соответствии с размером экрана
     *
     * @param width  ширина экрана в пикселах
     * @param height высота экрана в пикселах
     */
    @Override
    public void resize(int width, int height)
    {}
}
