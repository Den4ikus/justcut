package com.jengine.graphics.renderers;

import com.jengine.common.IDisposable;
import com.jengine.game.Entity;
import com.jengine.graphics.RenderTarget;
import com.jengine.graphics.shaders.BaseShader;

/**
 * Базовый класс для рендеров игровых сущностей
 */
public abstract class BaseEntityRender<TEntity extends Entity, TShader extends BaseShader> implements IDisposable
{
    protected final TShader shader;

    public BaseEntityRender(TShader shader)
    {
        this.shader = shader;
    }

    public final void begin(RenderTarget target, boolean bindTarget)
    {
        this.shader.begin(target, bindTarget);
    }

    public final void end(boolean resetTarget)
    {
        this.shader.end(resetTarget);
    }

    public abstract void render(TEntity entity);
}
