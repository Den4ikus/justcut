package com.jengine.graphics.renderers;

import com.jengine.game.Entity;
import com.jengine.game.EntityType;

/**
 *
 */
public interface IEntityRender<T extends EntityType>
{
    void begin();
    void end();
    void render(Entity<T> entity);
}
