package com.jengine.graphics.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.jengine.common.IDisposable;
import com.jengine.common.IFactory;
import com.jengine.common.Pool;
import com.jengine.graphics.buffers.VertexBuffer;
import com.jengine.graphics.shaders.BaseShader;
import com.jengine.graphics.shaders.ScreenSpaceTextureShader;
import com.jengine.graphics.vertices.VertexPosition2Texture;
import com.jengine.graphics.vertices.VertexSerializer;
import com.jengine.math.MathHelper;
import com.jengine.math.Vector;

import java.util.ArrayList;

/**
 * Рендер текстур в экранном пространстве
 */
public final class ScreenSpaceTextureRenderer implements IDisposable
{
    private final static Color DefaultColor = Color.WHITE;

    private VertexBuffer _vb;
    private VertexSerializer _serializer;
    private ScreenSpaceTextureShader _shader;

    // Объект вершины для всех манипуляций с буфером
    private final VertexPosition2Texture tempVertex = new VertexPosition2Texture();

    private final ArrayList<DrawCall> _drawCalls = new ArrayList<DrawCall>();

    public ScreenSpaceTextureRenderer(ScreenSpaceTextureShader shader, int vertexCount)
    {
        _vb = new VertexBuffer(vertexCount, VertexPosition2Texture.declaration, false);
        _serializer = new VertexSerializer(vertexCount * VertexPosition2Texture.declaration.getVertexStride());
        _shader = shader;
    }

    public void draw(TextureRegion texRegion, float x, float y, float width, float height)
    {
        draw(texRegion, x, y, width, height, DefaultColor);
    }

    /**
     *
     * @param texRegion
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void draw(TextureRegion texRegion, float x, float y, float width, float height, Color color)
    {
        DrawCall dc = DrawCall.get();
        dc.texRegion = texRegion;
        dc.vertexOffset = _serializer.getBufferOffset() / _vb.getVertexDeclaration().getVertexStride();
        dc.vertexCount = 6;
        dc.color = color;
        _drawCalls.add(dc);
        //1-2
        //0-3

        //0
        tempVertex.set(x, y, 0f, 0f);
        _serializer.serialize(tempVertex, false);

        //1
        tempVertex.set(x, y + height, 0f, 1f);
        _serializer.serialize(tempVertex, false);

        //2
        tempVertex.set(x + width, y + height, 1f, 1f);
        _serializer.serialize(tempVertex, false);

        //0
        tempVertex.set(x, y, 0f, 0f);
        _serializer.serialize(tempVertex, false);

        //2
        tempVertex.set(x + width, y + height, 1f, 1f);
        _serializer.serialize(tempVertex, false);

        //3
        tempVertex.set(x + width, y, 1f, 0f);
        _serializer.serialize(tempVertex, false);
    }

    public void draw(TextureRegion texRegion, Vector2 v1, Vector2 v2, float width)
    {
        draw(texRegion, v1, v2, width, 1.0f, 1.0f, DefaultColor);
    }

    public void draw(TextureRegion texRegion, Vector2 v1, Vector2 v2, float width, float uScale, float vScale)
    {
        draw(texRegion, v1, v2, width, uScale, vScale, DefaultColor);
    }

    public void draw(TextureRegion texRegion, Vector2 v1, Vector2 v2, float width, float uScale, float vScale, Color color)
    {
        DrawCall dc = DrawCall.get();
        dc.texRegion = texRegion;
        dc.vertexOffset = _serializer.getBufferOffset() / _vb.getVertexDeclaration().getVertexStride();
        dc.vertexCount = 6;
        dc.color = color;
        _drawCalls.add(dc);

        // v1. {1-2} .v2
        //     {0-3}

        // Получаем перпендикулярный вектор длиной 1/2 getWidth
        Vector2 offset = MathHelper.normalize(MathHelper.getNormalX(v1, v2)).scl(width * 0.5f);

        //0
        tempVertex.set(v1.x + offset.x, v1.y + offset.y, 0f, 0f);
        _serializer.serialize(tempVertex, false);

        //1
        tempVertex.set(v1.x - offset.x, v1.y - offset.y, 0f, 1f * vScale);
        _serializer.serialize(tempVertex, false);

        //2
        tempVertex.set(v2.x - offset.x, v2.y - offset.y, 1f * uScale, 1f * vScale);
        _serializer.serialize(tempVertex, false);

        //0
        tempVertex.set(v1.x + offset.x, v1.y + offset.y, 0f, 0f);
        _serializer.serialize(tempVertex, false);

        //2
        tempVertex.set(v2.x - offset.x, v2.y - offset.y, 1f * uScale, 1f * vScale);
        _serializer.serialize(tempVertex, false);

        //3
        tempVertex.set(v2.x + offset.x, v2.y + offset.y, 1f * uScale, 0f);
        _serializer.serialize(tempVertex, false);

        Vector.put(offset);
    }

    public void begin()
    {
        _shader.setInvScreenSize(1f / Gdx.graphics.getWidth(), 1f / Gdx.graphics.getHeight());
        _shader.begin(null, false);
    }

    public void end()
    {
        // Записываем всё в вершинный буфер
        int bufferSize = _serializer.getBufferOffset();
        _vb.setVertices(_serializer.getBuffer(), 0, bufferSize);
        _serializer.reset();

        // Устанавливаем первую текстуру т.к. параметр может и не измениться
        // но может быть забиндена другая текстура
        /*if(_drawCalls.size() > 0)
        {
            _shader.setTexture(_drawCalls.get(0).texture);
            _shader.applyParameters();
        }*/

        for(int i = 0; i < _drawCalls.size(); i++)
        {
            DrawCall dc = _drawCalls.get(i);

            // Проверяем текстуру, если надо меняем
            /*boolean textureChanged = _shader.getTexture() != dc.texture;
            if(textureChanged)
            {
                _shader.setTexture(dc.texture);
            }*/
            if(dc.texRegion != null) _shader.setTextureRegion(dc.texRegion);
            else _shader.setTexture(dc.texture);
            _shader.setColor(dc.color);

            _shader.render(BaseShader.PrimitiveType.Triangles, _vb, dc.vertexOffset, dc.vertexCount, true);
            DrawCall.put(dc);
        }
        _drawCalls.clear();

        _shader.end(false);
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_vb != null)
            _vb.dispose();
    }
}

/**
 * Содержит информацию о запросе рендеринга. Доступ к экземплярам только через пул
 */
final class DrawCall
{
    //region Pool

    private static Pool<DrawCall> _pool = new Pool<DrawCall>(new IFactory<DrawCall>()
    {
        @Override
        public DrawCall create()
        {
            return new DrawCall();
        }
    });
    public static DrawCall get()
    {
        return  _pool.get();
    }
    public static void put(DrawCall drawCall)
    {
        _pool.put(drawCall);
    }

    //endregion

    public Texture texture;
    public TextureRegion texRegion;
    public int vertexOffset;
    public int vertexCount;
    public Color color;

    private DrawCall()
    {}
}