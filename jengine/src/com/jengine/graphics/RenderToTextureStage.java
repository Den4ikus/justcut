package com.jengine.graphics;

import com.badlogic.gdx.graphics.Pixmap;
import com.jengine.game.Scene;

/**
 * Cтадия рендеринга геометрии в текстуру
 */
public abstract class RenderToTextureStage<T extends Scene> implements IRenderStage<T>
{
    private RenderTarget _rt;
    private Pixmap.Format _rtFormat;
    private boolean _rtHasDepth;

    public RenderToTextureStage(Pixmap.Format format, int rtWidth, int rtHeight, boolean hasDepth)
    {
        _rtFormat = format;
        _rtHasDepth = hasDepth;
        _rt = new RenderTarget(format, rtWidth, rtHeight, hasDepth);
    }

    @Override
    public final RenderTarget getRenderTarget()
    {
        return _rt;
    }

    /**
     * Изменяет размер RenderTarget в соответствии с размером экрана
     *
     * @param width  ширина экрана в пикселах
     * @param height высота экрана в пикселах
     */
    @Override
    public void resize(int width, int height)
    {
        if(_rt == null) return;

        _rt.dispose();
        _rt = new RenderTarget(_rtFormat, width, height, _rtHasDepth);
    }

    @Override
    public final void render(T scene)
    {
        _rt.bind();
        onRender(scene);
        _rt.unbind();
    }

    @Override
    public void dispose()
    {
        if(_rt != null)
            _rt.dispose();
    }

    protected abstract void onRender(T scene);
}
