package com.jengine.graphics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.jengine.cameras.Camera;
import com.jengine.graphics.vertices.VertexPosition2Color;
import com.jengine.math.MathHelper;
import com.jengine.math.Vector;
import com.jengine.math.bounds.BoundingBox2D;
import com.jengine.math.bounds.BoundingCircle;
import com.jengine.math.bounds.BoundingPoly;

import javax.microedition.khronos.opengles.GL10;
import java.util.ArrayList;

/**
 *
 */
// TODO отрефакторить, выпилить Mesh перейти на VBO
public final class ShapeRenderer implements Disposable
{
	private float[] _vertexBuffer;
    private Mesh _mesh;
    private ShaderProgram _shader;
    private ShapePool _pool = new ShapePool();
    private ArrayList<Shape> _shapes = new ArrayList<Shape>();

    private final static Vector2[] boxCorners = new Vector2[4];
    private final static Vector2[] circleCorners = new Vector2[360];

    // инициализируем вспомогательные массивы
    static
    {
        boxCorners[0] = new Vector2();
        boxCorners[1] = new Vector2();
        boxCorners[2] = new Vector2();
        boxCorners[3] = new Vector2();

        final float stepAngle = MathHelper.TwoPi / 360f;
        int index = 0;
        // рассчитываем единичные вектора по кругу в плоскости XY
        for (int i = 0; i < 360; i++)
        {
            float angle = i * stepAngle;
            circleCorners[index++] = new Vector2((float)Math.cos(angle), (float)Math.sin(angle));
        }
    }
	
	private ShapeRenderMode _renderMode = ShapeRenderMode.Line;
	public ShapeRenderMode getRenderMode()
	{
		return _renderMode;
	}
	public void setRenderMode(ShapeRenderMode mode)
	{
		_renderMode = mode;
	}	

    public ShapeRenderer(ShaderProgram shader, int maxVertexCount)
    {
        _vertexBuffer = new float[maxVertexCount * VertexPosition2Color.declaration.getVertexStride()];

        // TODO Mesh и VBO говно перепилить на cвой буфер
        VertexAttribute[] attributes = new VertexAttribute[VertexPosition2Color.declaration.getAttributes().size()];
        for(int i = 0; i < attributes.length; i++)
            attributes[i] = VertexPosition2Color.declaration.getAttributes().get(i);

        _mesh = new Mesh(Mesh.VertexDataType.VertexArray, false, maxVertexCount, 0, attributes);
        _shader = shader;
    }

    public void line(Vector2 a, Vector2 b, Color color)
    {
        Shape shape = getShape(2);
        shape.vertices[0].set(a, color);
        shape.vertices[1].set(b, color);
    }

    public void box(BoundingBox2D box, Color color)
    {
		Vector2[] corners = box.getCorners(boxCorners);
		if(_renderMode == ShapeRenderMode.Line)
		{
			// рендеринг линий
			Shape shape = getShape(8);
			shape.vertices[0].set(corners[0], color);
			shape.vertices[1].set(corners[1], color);
			shape.vertices[2].set(corners[1], color);
			shape.vertices[3].set(corners[2], color);
			shape.vertices[4].set(corners[2], color);
			shape.vertices[5].set(corners[3], color);
			shape.vertices[6].set(corners[3], color);
			shape.vertices[7].set(corners[0], color);
		}
		else
		{
			// рендеринг треугольников
			Shape shape = getShape(6);
			shape.vertices[0].set(corners[0], color);
			shape.vertices[1].set(corners[1], color);
			shape.vertices[2].set(corners[2], color);
			shape.vertices[3].set(corners[0], color);
			shape.vertices[4].set(corners[2], color);
			shape.vertices[5].set(corners[3], color);
		}
    }

    public void box(Vector2 v1, Vector2 v2, Color color)
    {
        Vector2 corner0 = Vector.getVector2().set(v1);
        Vector2 corner1 = Vector.getVector2().set(v1.x, v2.y);
        Vector2 corner2 = Vector.getVector2().set(v2);
        Vector2 corner3 = Vector.getVector2().set(v2.x, v1.y);
        if(_renderMode == ShapeRenderMode.Line)
        {
            // рендеринг линий
            Shape shape = getShape(8);
            shape.vertices[0].set(corner0, color);
            shape.vertices[1].set(corner1, color);
            shape.vertices[2].set(corner1, color);
            shape.vertices[3].set(corner2, color);
            shape.vertices[4].set(corner2, color);
            shape.vertices[5].set(corner3, color);
            shape.vertices[6].set(corner3, color);
            shape.vertices[7].set(corner0, color);
        }
        else
        {
            // рендеринг треугольников
            Shape shape = getShape(6);
            shape.vertices[0].set(corner0, color);
            shape.vertices[1].set(corner1, color);
            shape.vertices[2].set(corner2, color);
            shape.vertices[3].set(corner0, color);
            shape.vertices[4].set(corner2, color);
            shape.vertices[5].set(corner3, color);
        }
    }

    public void poly(BoundingPoly poly, Color color)
    {
		Vector2[] polyVertices = poly.vertices;
		int vertexOffest = 0;
		
		if(_renderMode == ShapeRenderMode.Line)
		{
			// рендеринг линий
			int vertexCount = poly.vertices.length * 2;
			Shape shape = getShape(vertexCount);

			// первая линия замыкающая контур
			shape.vertices[vertexOffest++].set(polyVertices[polyVertices.length - 1], color);
			shape.vertices[vertexOffest++].set(polyVertices[0], color);

			for(int i = 1; i < polyVertices.length; i++)
			{
				shape.vertices[vertexOffest++].set(polyVertices[i - 1], color);
				shape.vertices[vertexOffest++].set(polyVertices[i], color);
			}
		}
		else
		{
			// рендеринг треугольников
			int vertexCount = polyVertices.length;
			int triangleCount = vertexCount - 2;
			Shape shape = getShape(triangleCount * 3);
		
			final int firstForwardIndex = 0;
			final int firstBackwardIndex = vertexCount - 1;
			int forwardIndex = firstForwardIndex;
			int backwardIndex = firstBackwardIndex;
		
			while(vertexOffest < triangleCount * 3)
			{
				// определяем в какую сторону двигаться по контуру
				if(forwardIndex - firstForwardIndex <= firstBackwardIndex - backwardIndex)
				{
					// двигаемся вперед
					shape.vertices[vertexOffest++].set(polyVertices[forwardIndex++], color);
					shape.vertices[vertexOffest++].set(polyVertices[forwardIndex], color);
					shape.vertices[vertexOffest++].set(polyVertices[backwardIndex], color);
				}
				else
				{
					// двигаемся назад
					shape.vertices[vertexOffest++].set(polyVertices[backwardIndex--], color);
					shape.vertices[vertexOffest++].set(polyVertices[forwardIndex], color);
					shape.vertices[vertexOffest++].set(polyVertices[backwardIndex], color);
				}
			}
		}
    }

    public void circle(BoundingCircle circle, Color color)
    {
        circle(circle, color, 0.5f);
    }

    public void circle(BoundingCircle circle, Color color, float accuracy)
    {
		int vertexCount = Math.round(MathHelper.TwoPi * circle.radius / accuracy);
		float step = 360f / vertexCount;
		int vertexOffest = 0;

		if(_renderMode == ShapeRenderMode.Line)
		{
			// рендеринг линий
			Shape shape = getShape(vertexCount * 2);
			// первая линия замыкающая контур
			shape.vertices[vertexOffest].set(circleCorners[Math.round(360f - step)], color);
			shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
			shape.vertices[vertexOffest].set(circleCorners[0], color);
			shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);

			for(int i = 1; i < vertexCount; i++)
			{
				float angle = i * step;
				int prevIndex = Math.round(angle - step);
				int index = Math.round(angle);
				shape.vertices[vertexOffest].set(circleCorners[prevIndex], color);
				shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
				shape.vertices[vertexOffest].set(circleCorners[index], color);
				shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
			}
		}
		else
		{
			// рендеринг треугольников
			int triangleCount = vertexCount - 2;
			Shape shape = getShape(triangleCount * 3);
		
			final int firstForwardIndex = 0;
			final int firstBackwardIndex = vertexCount - 1;
			int forwardIndex = firstForwardIndex;
			int backwardIndex = firstBackwardIndex;
		
			while(vertexOffest < triangleCount * 3)
			{
				// определяем в какую сторону двигаться по контуру
				if(forwardIndex - firstForwardIndex <= firstBackwardIndex - backwardIndex)
				{
					// двигаемся вперед
					float fwdAngle = forwardIndex++ * step;
					int fwdIndex = Math.round(fwdAngle);
					int fwdNextIndex = Math.round(fwdAngle + step);
					int bwdIndex = Math.round(backwardIndex * step);

					shape.vertices[vertexOffest].set(circleCorners[fwdIndex], color);
					shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
					shape.vertices[vertexOffest].set(circleCorners[fwdNextIndex], color);
					shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
					shape.vertices[vertexOffest].set(circleCorners[bwdIndex], color);
					shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
				}
				else
				{
					// двигаемся назад
					float bwdAngle = backwardIndex-- * step;
					int bwdIndex = Math.round(bwdAngle);
					int bwdNextIndex = Math.round(bwdAngle - step);
					int fwdIndex = Math.round(forwardIndex * step);
					
					shape.vertices[vertexOffest].set(circleCorners[bwdIndex], color);
					shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
					shape.vertices[vertexOffest].set(circleCorners[fwdIndex], color);
					shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
					shape.vertices[vertexOffest].set(circleCorners[bwdNextIndex], color);
					shape.vertices[vertexOffest++].position.scl(circle.radius).add(circle.center);
				}
			}
		}
    }

    public void render(Camera camera)
    {
		int vertexCounter = 0;
        int vertexBufferIndex = 0;
        for(int i = 0; i < _shapes.size(); i++)
        {
            // копируем вершины в буфер
            Shape shape = _shapes.get(i);
            for(int j = 0; j < shape.vertexUsed; j++)
            {
                VertexPosition2Color vertex = shape.vertices[j];
                vertex.serialize(_vertexBuffer, vertexBufferIndex);

                vertexBufferIndex += VertexPosition2Color.declaration.getVertexStride();
				vertexCounter++;
            }
			
			// возвращам шейп в пул
			_pool.put(shape);
        }
		_shapes.clear();

        _mesh.setVertices(_vertexBuffer, 0, vertexBufferIndex);

		int primitiveType = _renderMode == ShapeRenderMode.Line ? GL10.GL_LINES : GL10.GL_TRIANGLES;
        _shader.begin();
        _shader.setUniformMatrix("u_wvp", camera.viewProj);
        _mesh.render(_shader, primitiveType, 0, vertexCounter);
        _shader.end();
    }

    private Shape getShape(int verticesCount)
    {
        Shape shape = _pool.get(verticesCount);
        _shapes.add(shape);
        return shape;
    }
	
	//region Disposable
	
	@Override
    public void dispose()
    {
		if(_mesh != null)
			_mesh.dispose();
    }
	
	//endregion
}

final class ShapePool
{
    private ArrayList<Shape> _shapes = new ArrayList<Shape>();

    public Shape get(int vertexCount)
    {
        // ищем шейп с подходящим количеством вершин
        for (int i = 0; i < _shapes.size(); i++)
        {
            Shape currentShape = _shapes.get(i);
            if (currentShape.vertices.length >= vertexCount)
            {
                currentShape.vertexUsed = vertexCount;
                _shapes.remove(i);
                return currentShape;
            }
        }

        // свободных или подходящих шейпов нет, создаем новый
        Shape shape = new Shape(vertexCount);
        shape.vertexUsed = vertexCount;
        return shape;
    }

    public void put(Shape shape)
    {
        _shapes.add(shape);
    }
}

