package com.jengine.graphics;

import com.jengine.graphics.vertices.VertexPosition2Color;

/**
 *
 */
final class Shape
{
    public VertexPosition2Color[] vertices;
    public int vertexUsed;

    public Shape(int vertexCount)
    {
        vertices = new VertexPosition2Color[vertexCount];
        for(int i = 0; i < vertexCount; i++)
            vertices[i] = new VertexPosition2Color();

        vertexUsed = 0;
    }
}
