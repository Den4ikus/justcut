package com.jengine.graphics;

import com.jengine.graphics.buffers.VertexBuffer;
import com.jengine.graphics.vertices.IVertexDeclaration;

/**
 * Интерфейс описания вершинных данных меша
 */
public interface IVertexData
{
    /**
     * Возвращает описание типа вершин
     */
	IVertexDeclaration getVertexDeclaration();
	
	/**
     * Возвращает источник который хранит данные о вершинах
     */
	VertexBuffer getSource();
	
	/**
     * Возвращает смещение в источнике по которому располагаются данные о вершинах (в вершинах
     */
	int getVertexOffset();
	
	/**
     * Возвращает количество вершин
     */
	int getVertexCount();
}
