//package com.jengine.graphics;
//
//import com.jengine.common.IDisposable;
//import com.jengine.game.Scene;
//
///**
// * Интерфейс описания вершинных данных геометрии
// */
//public interface IVertexData
//{
//    /**
//     * Возвращает описание типа вершин
//     */
//    IVertexDeclaration getVertexDeclaration();
//
//	/**
//     * Возвращает источник который хранит данные о вершинах
//     */
//	VertexBuffer getSource();
//
//	/**
//     * Возвращает смещение по которому располагаются данные о вершинах
//     */
//	int getOffset();
//
//	/**
//     * Возвращает количество вершин
//     */
//	int getVertexCount();
//}
