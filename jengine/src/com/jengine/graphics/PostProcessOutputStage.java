package com.jengine.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 *
 */
public class PostProcessOutputStage implements IPostProcessStage
{
    @Override
    public RenderTarget getRenderTarget()
    {
        // окончательная стадия рендерит на экран
        throw new UnsupportedOperationException();
    }

    public void render(SpriteBatch batch, RenderTarget source)
    {
        Gdx.gl20.glViewport(0,0, Gdx.graphics.getWidth(),  Gdx.graphics.getHeight());
        batch.begin();
        // todo батч может внутри устанавливать viewport в таком случае придется писать свой
        batch.draw(source.getTexture(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();
    }
}
