package com.jengine.graphics.vertices;

import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Денис on 12.07.2016.
 * Вершина содержащая двухмерные координаты и текстурные координаты
 */
public final class VertexPosition2Texture implements IVertexType
{
    private static final VertexAttributes attributes = new VertexAttributes(
            new VertexAttribute(VertexAttributes.Usage.Position, 2, ShaderProgram.POSITION_ATTRIBUTE),
            new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE)
    );

    public static final IVertexDeclaration declaration = new IVertexDeclaration()
    {
        @Override
        public VertexAttributes getAttributes()
        {
            return attributes;
        }

        @Override
        public int getVertexStride()
        {
            return attributes.vertexSize / 4;
        }

        @Override
        public int getVertexSize()
        {
            return attributes.vertexSize;
        }
    };

    public final Vector2 position;
    public final Vector2 texcoord;

    public void set(Vector2 position, Vector2 texcoord)
    {
        this.position.set(position);
        this.texcoord.set(texcoord);
    }

    public void set(float x, float y, float u, float v)
    {
        this.position.set(x, y);
        this.texcoord.set(u, v);
    }

    public VertexPosition2Texture()
    {
        this(new Vector2(), new Vector2());
    }

    public VertexPosition2Texture(Vector2 position, Vector2 texcoord)
    {
        this.position = new Vector2(position);
        this.texcoord = new Vector2(texcoord);
    }

    //region IVertexType

    /**
     * Возвращает описание типа вершин
     */
    @Override
    public IVertexDeclaration getVertexDeclaration()
    {
        return declaration;
    }

    /**
     * Записывает данные вершины в массив
     *
     * @param array  массив
     * @param offset индекс первого элемента куда будут записаны данные
     * @return Возвращает индекс следующего свободного элемента
     */
    @Override
    public int serialize(float[] array, int offset)
    {
        array[offset] = position.x;
        array[offset + 1] = position.y;
        array[offset + 2] = texcoord.x;
        array[offset + 3] = texcoord.y;
        return offset + declaration.getVertexStride();
    }

    //endregion
}
