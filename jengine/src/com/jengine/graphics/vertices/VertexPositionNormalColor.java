package com.jengine.graphics.vertices;

/**
 *
 */
/*public class VertexPositionNormalColor implements IVertexType
{
    // в конструкторе Mesh иногда требуется массив...
    public static final VertexAttribute[] attributeArray = new VertexAttribute[]
            {
                    new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
                    new VertexAttribute(VertexAttributes.Usage.Normal, 3, ShaderProgram.NORMAL_ATTRIBUTE),
                    new VertexAttribute(VertexAttributes.Usage.ColorUnpacked, 4, ShaderProgram.COLOR_ATTRIBUTE)
            };

    public static final VertexAttributes attributes = new VertexAttributes(attributeArray);

    public static final int vertexSize = attributes.vertexSize;
    public static final int vertexStride = attributes.vertexSize / 4;

    public final Vector3 position = new Vector3();
    public final Vector3 normal = new Vector3();
    public final Color color = new Color();

    public void set(VertexPositionNormal vertex, Color color)
    {
        this.position.set(vertex.position);
        this.normal.set(vertex.normal);
        this.color.set(color);
    }

    public void set(Vector3 position, Vector3 normal, Color color)
    {
        this.position.set(position);
        this.normal.set(normal);
        this.color.set(color);
    }

    public VertexPositionNormalColor()
    {}

    public VertexPositionNormalColor(Vector3 position, Vector3 normal, Color color)
    {
        set(position, normal, color);
    }

    public void transform(Matrix4 transformation)
    {
        position.mul(transformation);
        //normal.mul(transformation);
    }

    //region IVertexType

    /**
     * Записывает данные вершины в массив
     *
     * @param array  массив
     * @param offset индекс первого элемента куда будут записаны данные
     */
    /*@Override
    public void serialize(float[] array, int offset)
    {
        array[offset] = position.x;
        array[offset + 1] = position.y;
        array[offset + 2] = position.z;
        array[offset + 3] = normal.x;
        array[offset + 4] = normal.y;
        array[offset + 5] = normal.z;
        array[offset + 6] = color.r;
        array[offset + 7] = color.g;
        array[offset + 8] = color.b;
        array[offset + 9] = color.a;
    }

    //endregion
}
*/