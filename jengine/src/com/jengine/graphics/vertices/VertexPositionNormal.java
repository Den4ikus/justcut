package com.jengine.graphics.vertices;

import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;

/**
 * Описывает вершину с данными о позиции и нормали
 */
public class VertexPositionNormal implements IVertexType
{
    private static final VertexAttributes attributes = new VertexAttributes(
            new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
            new VertexAttribute(VertexAttributes.Usage.Normal, 3, ShaderProgram.NORMAL_ATTRIBUTE)
    );

    public static final IVertexDeclaration declaration = new IVertexDeclaration()
    {
        @Override
        public VertexAttributes getAttributes()
        {
            return attributes;
        }

        @Override
        public int getVertexStride()
        {
            return attributes.vertexSize / 4;
        }

        @Override
        public int getVertexSize()
        {
            return attributes.vertexSize;
        }
    };

    public final Vector3 position = new Vector3();
    public final Vector3 normal = new Vector3();

    public void set(Vector3 position, Vector3 normal)
    {
        this.position.set(position);
        this.normal.set(normal);
    }

    public VertexPositionNormal()
    {}

    public VertexPositionNormal(Vector3 position, Vector3 normal)
    {
        set(position, normal);
    }

    //region IVertexType

    /**
     * Возвращает описание типа вершин
     */
    @Override
    public IVertexDeclaration getVertexDeclaration()
    {
        return declaration;
    }

    /**
     * Записывает данные вершины в массив
     *
     * @param array  массив
     * @param offset индекс первого элемента куда будут записаны данные
     * @return Возвращает индекс следующего свободного элемента
     */
    @Override
    public int serialize(float[] array, int offset)
    {
        array[offset] = position.x;
        array[offset + 1] = position.y;
        array[offset + 2] = position.z;
        array[offset + 3] = normal.x;
        array[offset + 4] = normal.y;
        array[offset + 5] = normal.z;
        return offset + declaration.getVertexStride();
    }

    //endregion
}
