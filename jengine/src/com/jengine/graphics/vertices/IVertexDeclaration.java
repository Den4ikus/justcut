package com.jengine.graphics.vertices;

import com.badlogic.gdx.graphics.VertexAttributes;

/**
 * Интерфейс описания типа вершин
 */
public interface IVertexDeclaration
{
	/**
	 * Возвращает атрибуты для типа вершин
	 */
	VertexAttributes getAttributes();

	/**
	 * Возвращает шаг массива с которым записываются вершины (во float-ах)
	 */
	int getVertexStride();

	/**
	 * Возвращает размер вершины в байтах
	 */
	int getVertexSize();
}
