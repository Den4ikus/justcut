package com.jengine.graphics.vertices;

/**
 * Сериализатор вершин в массив float-ов для записи в вершинный буфер
 * Нужен исключительно для предотвращения выделения памяти при каждой сериализации т.к. для того чтобы записать вершины в буфер нужно сначала получить массив float-ов
 */
public final class VertexSerializer
{
    private int _bufferOffset;
    public int getBufferOffset()
    {
        return _bufferOffset;
    }

    private float[] _buffer;
    public float[] getBuffer()
    {
        return _buffer;
    }

    /**
     *
     * @param bufferSize размер буфера во float-ах
     */
    public VertexSerializer(int bufferSize)
    {
        _buffer = new float[bufferSize];
    }

    /**
     * Сбрасывает текущее смещение внутри буфера т.е. при следующей сериализации содержимое будет перезаписано
     */
    public void reset()
    {
        _bufferOffset = 0;
    }

    /**
     * Сериализует вершину в буфер
     * @param vertex
     * @param autoReset Нужно ли сборсить указатель после выполнения сериализации
     * @param <T>
     * @return Возвращает текущий размер данных в буфере (во float-ах!)
     */
    public <T extends IVertexType> int serialize(T vertex, boolean autoReset)
    {
        int offset = vertex.serialize(_buffer, _bufferOffset);
        _bufferOffset = autoReset ? 0 : offset;
        return offset;
    }

    /**
     * Сериализует вершины меша в буфер
     * @param mesh Вершины меша
     * @param autoReset Нужно ли сборсить указатель после выполнения сериализации
     * @return Возвращает размер меша после сериализации (во float-ах!)
     */
    public <T extends IVertexType> int serialize(T[] mesh, boolean autoReset)
    {
        int offset = 0;
        for(int i = 0; i < mesh.length; i++)
            offset = mesh[i].serialize(_buffer, offset);
        _bufferOffset = autoReset ? 0 : offset;
        return offset;
    }
}
