package com.jengine.graphics.vertices;

import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

/**
 * Вершина содержащая двухмерные координаты
 */
public final class VertexPosition2 implements IVertexType
{
    private static final VertexAttributes attributes = new VertexAttributes(
            new VertexAttribute(VertexAttributes.Usage.Position, 2, ShaderProgram.POSITION_ATTRIBUTE)
    );

    public static final IVertexDeclaration declaration = new IVertexDeclaration()
    {
        @Override
        public VertexAttributes getAttributes()
        {
            return attributes;
        }

        @Override
        public int getVertexStride()
        {
            return attributes.vertexSize / 4;
        }

        @Override
        public int getVertexSize()
        {
            return attributes.vertexSize;
        }
    };

    public final Vector2 position;

    public void set(Vector2 position)
    {
        this.position.set(position);
    }

    public VertexPosition2()
    {
        this(new Vector2());
    }

    public VertexPosition2(Vector2 position)
    {
        this.position = new Vector2(position);
    }

    //region IVertexType

    /**
     * Возвращает описание типа вершин
     */
    @Override
    public IVertexDeclaration getVertexDeclaration()
    {
        return declaration;
    }

    /**
     * Записывает данные вершины в массив
     *
     * @param array  массив
     * @param offset индекс первого элемента куда будут записаны данные
     * @return Возвращает индекс следующего свободного элемента
     */
    @Override
    public int serialize(float[] array, int offset)
    {
        array[offset] = position.x;
        array[offset + 1] = position.y;
        return offset + declaration.getVertexStride();
    }

    //endregion
}

