package com.jengine.graphics.vertices;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Денис on 12.08.2015.
 * Вершина содержащая двухмерные координаты и цвет
 */
public final class VertexPosition2Color implements IVertexType
{
    private static final VertexAttributes attributes = new VertexAttributes(
            new VertexAttribute(VertexAttributes.Usage.Position, 2, ShaderProgram.POSITION_ATTRIBUTE),
            new VertexAttribute(VertexAttributes.Usage.ColorUnpacked, 4, ShaderProgram.COLOR_ATTRIBUTE)
    );

    public static final IVertexDeclaration declaration = new IVertexDeclaration()
    {
        @Override
        public VertexAttributes getAttributes()
        {
            return attributes;
        }

        @Override
        public int getVertexStride()
        {
            return attributes.vertexSize / 4;
        }

        @Override
        public int getVertexSize()
        {
            return attributes.vertexSize;
        }
    };

    public final Vector2 position;
    public final Color color;

    public void set(Vector2 position, Color color)
    {
        this.position.set(position);
        this.color.set(color);
    }

    public VertexPosition2Color()
    {
        this(new Vector2(), new Color());
    }

    public VertexPosition2Color(Vector2 position, Color color)
    {
        this.position = new Vector2(position);
        this.color = new Color(color);
    }

    //region IVertexType

    /**
     * Возвращает описание типа вершин
     */
    @Override
    public IVertexDeclaration getVertexDeclaration()
    {
        return declaration;
    }

    /**
     * Записывает данные вершины в массив
     *
     * @param array  массив
     * @param offset индекс первого элемента куда будут записаны данные
     * @return Возвращает индекс следующего свободного элемента
     */
    @Override
    public int serialize(float[] array, int offset)
    {
        array[offset] = position.x;
        array[offset + 1] = position.y;
        array[offset + 2] = color.r;
        array[offset + 3] = color.g;
        array[offset + 4] = color.b;
        array[offset + 5] = color.a;
        return offset + declaration.getVertexStride();
    }

    //endregion
}
