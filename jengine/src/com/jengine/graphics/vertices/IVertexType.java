package com.jengine.graphics.vertices;

/**
 * Интерфейс для типов вершин
 */
public interface IVertexType
{
    /**
     * Возвращает описание типа вершин
     */
	IVertexDeclaration getVertexDeclaration();
	
    /**
     * Записывает данные вершины в массив
     * @param array массив
     * @param offset индекс первого элемента куда будут записаны данные
     * @return Возвращает индекс следующего свободного элемента
     */
    int serialize(float[] array, int offset);
}
