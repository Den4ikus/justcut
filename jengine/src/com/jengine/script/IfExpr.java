package com.jengine.script;

import com.jengine.script.common.EvaluateExpression;
import com.jengine.script.common.Expression;
import com.jengine.script.common.SimpleExpr;

/**
 * Created by ����� on 17.11.2015.
 */
public final class IfExpr extends SimpleExpr
{
    private EvaluateExpression<Boolean> _condition;
    private Expression _whenTrue;
    private Expression _whenFalse;

    public IfExpr(EvaluateExpression<Boolean> condition, Expression whenTrue, Expression whenFalse)
    {
        _condition = condition;
        _whenTrue = whenTrue;
        _whenFalse = whenFalse;
    }

    @Override
    protected void doExecute(InterpreterContext context) throws ExpressionExecuteException
    {
        _condition.execute(context);
        if(_condition.evaluate()) _whenTrue.execute(context);
        else _whenFalse.execute(context);
    }
}
