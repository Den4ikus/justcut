package com.jengine.script.common;

import com.jengine.script.ExpressionExecuteException;
import com.jengine.script.InterpreterContext;

/**
 * Created by ����� on 17.11.2015.
 */
public abstract class Expression
{
    protected String _id;

    public String getId()
    {
        return _id != null ? _id : (_id = getNextId());
    }

    public abstract boolean execute(InterpreterContext context) throws ExpressionExecuteException;

    private static final String DefaultId = "Expression";
    private static int _idCounter = 0;

    private static String getNextId()
    {
        return DefaultId + _idCounter++;
    }
}
