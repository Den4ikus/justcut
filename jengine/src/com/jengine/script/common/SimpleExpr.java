package com.jengine.script.common;

import com.jengine.script.ExpressionExecuteException;
import com.jengine.script.InterpreterContext;

/**
 * Created by ����� on 17.11.2015.
 * ������������ ������� ���������, ������� �� ������ �� ������� ����������
 */
public abstract class SimpleExpr extends Expression
{
    @Override
    public final boolean execute(InterpreterContext context) throws ExpressionExecuteException
    {
        doExecute(context);
        return true;
    }

    protected abstract void doExecute(InterpreterContext context) throws ExpressionExecuteException;
}
