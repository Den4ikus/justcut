package com.jengine.script.common;

import com.jengine.script.ExpressionExecuteException;
import com.jengine.script.InterpreterContext;

/**
 * Created by ����� on 17.11.2015.
 * �������� ��������
 */
public abstract class BinaryOperation<T, TResult> extends EvaluateExpression<TResult>
{
    private EvaluateExpression<T> _leftOp;
    private EvaluateExpression<T> _rightOp;
    private TResult _result;

    protected BinaryOperation(EvaluateExpression<T> leftOp, EvaluateExpression<T> rightOp)
    {
        _leftOp = leftOp;
        _rightOp = rightOp;
    }

    @Override
    protected void doExecute(InterpreterContext context) throws ExpressionExecuteException
    {
        _leftOp.execute(context);
        _rightOp.execute(context);
        T leftOp = _leftOp.evaluate();
        T rightOp = _rightOp.evaluate();
        _result = doExecute(context, leftOp, rightOp);
    }

    protected abstract TResult doExecute(InterpreterContext context, T leftOp, T rightOp);

    @Override
    public TResult evaluate()
    {
        return _result;
    }
}
