package com.jengine.script.common;

import com.jengine.script.common.SimpleExpr;

/**
 * Created by ����� on 17.11.2015.
 */
public abstract class EvaluateExpression<T> extends SimpleExpr
{
    public abstract T evaluate();
}
