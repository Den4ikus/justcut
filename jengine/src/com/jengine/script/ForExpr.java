package com.jengine.script;

import com.jengine.script.common.EvaluateExpression;
import com.jengine.script.common.Expression;

/**
 * Created by ����� on 17.11.2015.
 */
public class ForExpr extends Expression
{
    private VariableDefExpr<Integer> _counterInitExpr;
    private EvaluateExpression<Boolean> _conditionExpr;
    private Expression _additionExpr;
    private Expression _bodyExpr;

    public ForExpr(VariableDefExpr<Integer> counterInitExpr, EvaluateExpression<Boolean> conditionExpr, Expression additionExpr, Expression bodyExpr)
    {
        _counterInitExpr = counterInitExpr;
        _conditionExpr = conditionExpr;
        _additionExpr = additionExpr;
        _bodyExpr = bodyExpr;
    }

    @Override
    public boolean execute(InterpreterContext context) throws ExpressionExecuteException
    {
        context.openScope();

        _counterInitExpr.execute(context);
        _conditionExpr.execute(context);
        while (_conditionExpr.evaluate())
        {
            if(!_bodyExpr.execute(context)) break;
            _additionExpr.execute(context);
            _conditionExpr.execute(context);
        }

        // ���� ��� ������ return ��������� ������� ��������� �� �����, �� ������� ������� ����� ������� ������������ ��������
        if(context.lookup(ReturnExpr.Marker) == ReturnExpr.MarkerValue)
            return false;

        context.closeScope();
        return true;
    }
}
