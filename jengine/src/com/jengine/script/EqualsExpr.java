package com.jengine.script;

import com.jengine.script.common.BinaryOperation;
import com.jengine.script.common.EvaluateExpression;

/**
 * Created by ����� on 17.11.2015.
 */
public final class EqualsExpr<T> extends BinaryOperation<T, Boolean>
{
    public EqualsExpr(EvaluateExpression<T> leftOp, EvaluateExpression<T> rightOp)
    {
        super(leftOp, rightOp);
    }

    @Override
    protected Boolean doExecute(InterpreterContext context, T leftOp, T rightOp)
    {
        if(leftOp != null && rightOp != null) return leftOp.equals(rightOp);
        else
        if(leftOp == null && rightOp == null) return true;

        return false;
    }
}
