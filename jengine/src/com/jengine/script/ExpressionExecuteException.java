package com.jengine.script;

/**
 * Created by ����� on 17.11.2015.
 */
public class ExpressionExecuteException extends Exception
{
    public final String ExpressionId;

    public ExpressionExecuteException(String expressionId, String description)
    {
        super(description);
        ExpressionId = expressionId;
    }
}
