package com.jengine.script;

import com.jengine.script.common.SimpleExpr;

/**
 * Created by ����� on 17.11.2015.
 * ���������� ����������
 */
public class VariableDefExpr<T> extends SimpleExpr
{
    private T _value;

    public VariableDefExpr(String name)
    {
        this(name, null);
    }

    public VariableDefExpr(String name, T value)
    {
        _id = name;
        _value = value;
    }

    @Override
    protected void doExecute(InterpreterContext context)
    {
        context.store(this, _value);
    }
}
