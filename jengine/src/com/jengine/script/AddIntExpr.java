package com.jengine.script;

import com.jengine.script.common.BinaryOperation;
import com.jengine.script.common.EvaluateExpression;

/**
 * Created by ����� on 17.11.2015.
 */
public final class AddIntExpr extends BinaryOperation<Integer, Integer>
{
    public AddIntExpr(EvaluateExpression<Integer> leftOp, EvaluateExpression<Integer> rightOp)
    {
        super(leftOp, rightOp);
    }

    @Override
    protected Integer doExecute(InterpreterContext context, Integer leftOp, Integer rightOp)
    {
        return leftOp + rightOp;
    }
}
