package com.jengine.script;

import com.jengine.script.common.EvaluateExpression;
import com.jengine.script.common.Expression;

/**
 * Created by ����� on 17.11.2015.
 */
public class WhileExpr extends Expression
{
    private EvaluateExpression<Boolean> _conditionExpr;
    private Expression _bodyExpr;

    public WhileExpr(EvaluateExpression<Boolean> conditionExpr, Expression bodyExpr)
    {
        _conditionExpr = conditionExpr;
        _bodyExpr = bodyExpr;
    }

    @Override
    public boolean execute(InterpreterContext context) throws ExpressionExecuteException
    {
        context.openScope();

        _conditionExpr.execute(context);
        while (_conditionExpr.evaluate())
        {
            if(!_bodyExpr.execute(context)) break;
            _conditionExpr.execute(context);
        }

        // ���� ��� ������ return ��������� ������� ��������� �� �����, �� ������� ������� ����� ������� ������������ ��������
        if(context.lookup(ReturnExpr.Marker) == ReturnExpr.MarkerValue)
            return false;

        context.closeScope();
        return true;
    }
}
