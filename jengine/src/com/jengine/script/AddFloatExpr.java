package com.jengine.script;

import com.jengine.script.common.BinaryOperation;
import com.jengine.script.common.EvaluateExpression;

/**
 * Created by ����� on 17.11.2015.
 */
public final class AddFloatExpr extends BinaryOperation<Float, Float>
{
    public AddFloatExpr(EvaluateExpression<Float> leftOp, EvaluateExpression<Float> rightOp)
    {
        super(leftOp, rightOp);
    }

    @Override
    protected Float doExecute(InterpreterContext context, Float leftOp, Float rightOp)
    {
        return leftOp + rightOp;
    }
}
