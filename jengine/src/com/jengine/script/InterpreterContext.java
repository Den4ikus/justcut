package com.jengine.script;

import com.jengine.script.common.Expression;

import java.util.HashMap;
import java.util.Stack;

/**
 * Created by ����� on 17.11.2015.
 */
public final class InterpreterContext
{
    private class Scope
    {
        public final String Name;
        public final HashMap<String, Object> Values;

        public Scope(String name)
        {
            Name = name;
            Values = new HashMap<String, Object>();
        }
    }

    private Stack<Scope> _scopes = new Stack<Scope>();

    public String getScopeName()
    {
        return _scopes.peek().Name;
    }

    public InterpreterContext()
    {
        _scopes.push(new Scope("global"));
    }

    public void openScope()
    {
        openScope("noname");
    }

    public void openScope(String scopeName)
    {
        _scopes.push(new Scope(scopeName));
    }

    public void closeScope()
    {
        _scopes.pop();
    }

    public void store(Expression expr, Object value)
    {
        store(expr.getId(), value);
    }

    public void store(String valueName, Object value)
    {
        HashMap<String, Object> currentScope = _scopes.peek().Values;
        currentScope.put(valueName, value);
    }

    public Object lookup(Expression expr)
    {
        return lookup(expr.getId());
    }

    public Object lookup(String valueName)
    {
        Object result = null;
        for(Scope scope : _scopes)
        {
            if((result = scope.Values.get(valueName)) != null)
                break;
        }
        return result;
    }
}
