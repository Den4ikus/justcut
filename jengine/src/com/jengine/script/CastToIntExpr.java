package com.jengine.script;

import com.jengine.script.common.EvaluateExpression;

/**
 * Created by ����� on 17.11.2015.
 */
public class CastToIntExpr extends EvaluateExpression<Integer>
{
    private EvaluateExpression _expr;
    private Integer _result;

    public CastToIntExpr(EvaluateExpression expr)
    {
        _expr = expr;
    }

    @Override
    public Integer evaluate()
    {
        return _result;
    }

    @Override
    protected void doExecute(InterpreterContext context) throws ExpressionExecuteException
    {
        Object value = _expr.evaluate();
        if(value.getClass() == Float.class)
        {
            _result = ((Float)value).intValue();
        }
        if(value.getClass() == String.class)
        {
            _result = Integer.parseInt((String) value);
        }
        else
        {
            String msg = String.format("���������� ���������� ���� %s � ���� Integer");
            throw new ExpressionExecuteException(getId(), msg);
        }
    }
}
