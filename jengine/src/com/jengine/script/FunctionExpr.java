package com.jengine.script;

import com.jengine.script.common.EvaluateExpression;
import com.jengine.script.common.Expression;

/**
 * Created by ����� on 17.11.2015.
 */
public final class FunctionExpr<TResult> extends EvaluateExpression<TResult>
{
    public static final String Result = "__result";

    private Expression[] _expressions;
    private TResult _result;

    public FunctionExpr(String name, Expression ... expressions)
    {
        _id = name;
        _expressions = expressions;
    }


    @Override
    public TResult evaluate()
    {
        return _result;
    }

    @Override
    protected void doExecute(InterpreterContext context) throws ExpressionExecuteException
    {
        String funcId = "function_" + _id;
        context.openScope(funcId);

        for(Expression expr : _expressions)
        {
            if(!expr.execute(context))
                break;
        }

        // ��������� ��������� ���������� � ��������� ��� ��������� ������� ��������� �� �������
        _result = (TResult)context.lookup(Result);
        while(context.getScopeName() != funcId) context.closeScope();

        context.closeScope();
    }
}
