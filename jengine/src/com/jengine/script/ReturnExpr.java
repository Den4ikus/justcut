package com.jengine.script;

import com.jengine.script.common.Expression;

/**
 * Created by ����� on 17.11.2015.
 */
public final class ReturnExpr<T> extends Expression
{
    public static final String Marker = "__return";
    public static final String MarkerValue = "execute";

    private T _value;

    public ReturnExpr(T value)
    {
        _value = value;
    }

    @Override
    public boolean execute(InterpreterContext context) throws ExpressionExecuteException
    {
        // �������� � �������� �������� ������� � ������ ������������ ����� return
        context.store(FunctionExpr.Result, _value);
        context.store(Marker, MarkerValue);
        return false;
    }
}
