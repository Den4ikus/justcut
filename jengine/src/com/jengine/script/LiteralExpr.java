package com.jengine.script;

import com.jengine.script.common.EvaluateExpression;

/**
 * Created by ����� on 17.11.2015.
 * ����������� ���������
 */
public final class LiteralExpr<T> extends EvaluateExpression<T>
{
    private T _value;

    public LiteralExpr(T value)
    {
        _value = value;
    }

    @Override
    public T evaluate()
    {
        return _value;
    }

    @Override
    protected void doExecute(InterpreterContext context)
    {}
}
