package com.jengine.collections.linq;

/**
 * Created by ����� on 23.10.2015.
 */
public interface ISelector<T, TResult>
{
    TResult select(T value);
}
