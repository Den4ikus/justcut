package com.jengine.collections.linq;

import java.util.Comparator;

/**
 *
 */
final class LinqComparator<T, TItem extends Comparable> implements Comparator<T>
{
    private final ISelector<T, TItem> _selector;

    public LinqComparator(ISelector<T, TItem> selector)
    {
        _selector = selector;
    }

    @Override
    public int compare(T o, T t1)
    {
        TItem first = _selector.select(o);
        TItem second = _selector.select(t1);
        return first.compareTo(second);
    }
}
