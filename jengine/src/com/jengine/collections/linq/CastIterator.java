package com.jengine.collections.linq;

import java.util.Iterator;

/**
 * Итератор с приведением типов
 */
public final class CastIterator<T, TCast> implements Iterator<TCast>
{
    private final Iterator<T> _iterator;

    public CastIterator(Iterable<T> collection)
    {
        _iterator = collection.iterator();
    }

    //region IEnumerator

    @Override
    public boolean hasNext()
    {
        return _iterator.hasNext();
    }

    @Override
    public TCast next()
    {
        return (TCast)_iterator.next();
    }

    @Override
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    // endregion
}
