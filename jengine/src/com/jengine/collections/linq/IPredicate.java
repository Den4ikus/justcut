package com.jengine.collections.linq;

/**
 * Created by Денис on 09.04.2015.
 */
public interface IPredicate<T>
{
    public final IPredicate AlwaysTrue = new IPredicate()
    {
        @Override
        public boolean evaluate(Object value)
        {
            return true;
        }
    };
    public final IPredicate AlwaysFalse = new IPredicate()
    {
        @Override
        public boolean evaluate(Object value)
        {
            return false;
        }
    };

    /*static <T> IPredicate<T> combineAnd(final IPredicate<T> ... predicates)
    {
        return new IPredicate<T>()
        {
            @Override
            public boolean evaluate(T value)
            {
                for(IPredicate predicate : predicates)
                    if(!predicate.evaluate(value)) return false;
                return true;
            }
        };
    }

    static <T> IPredicate<T> combineOr(final IPredicate<T> ... predicates)
    {
        return new IPredicate<T>()
        {
            @Override
            public boolean evaluate(T value)
            {
                for(IPredicate predicate : predicates)
                    if(predicate.evaluate(value)) return true;
                return false;
            }
        };
    }*/

    /**
     * Проверяет удовлетворяет ли значение данному предикату
     */
    boolean evaluate(T value);
}
