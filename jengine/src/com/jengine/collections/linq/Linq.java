package com.jengine.collections.linq;

import com.jengine.common.Utils;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Денис on 09.04.2015.
 */
public final class Linq
{
    /**
     * Проверяет удовлетворяет ли хотябы один элемент коллекции условию предиката
     * @param collection коллекция
     * @param predicate предикат
     * @param <T> тип элементов коллекции
     * @return Возвращает true если хотябы один элемент коллекции удовлетворяет условию предиката, false в противном случае
     */
    public static <T> boolean any(Iterable<T> collection, IPredicate<? super T> predicate)
    {
        for(T item : collection)
        {
            if(predicate.evaluate(item))
                return true;
        }
        return false;
    }

    /**
     * Проверяет удовлетворяют ли все элементы коллекции условию предиката
     * @param collection коллекция
     * @param predicate предикат
     * @param <T> тип элементов коллекции
     * @return Возвращает true если все элементы коллекции удовлетворяют условию предиката, false в противном случае
     */
    public static <T> boolean all(Iterable<T> collection, IPredicate<? super T> predicate)
    {
        for(T item : collection)
        {
            if(!predicate.evaluate(item))
                return false;
        }
        return true;
    }

    /**
     * Проверяет удовлетворяют ли все элементы коллекции условию предиката
     * @param collection коллекция
     * @param predicate предикат
     * @param <T> тип элементов коллекции
     * @return Возвращает true если все элементы коллекции удовлетворяют условию предиката, false в противном случае
     */
    public static <T> boolean all(T[] collection, IPredicate<? super T> predicate)
    {
        for(T item : collection)
        {
            if(!predicate.evaluate(item))
                return false;
        }
        return true;
    }

    /**
     * Проверяет содержит ли коллекция элементы удовлетворяющие условию предиката
     * @param collection коллекция
     * @param predicate предикат
     * @param <T> тип элементов коллекции
     * @return Возвращает true если хотябы один элемент коллекции удовлетворяет условию предиката, false в противном случае
     */
    public static <T> boolean contains(Iterable<T> collection, IPredicate<? super T> predicate)
    {
        return any(collection, predicate);
    }

    /**
     * Поиск элементов коллекции удовлетворяющих условию предиката
     * @param collection коллекция
     * @param predicate предикат
     * @param <T> тип элементов коллекции
     * @return Возвращает перечисление элементов удовлетворяющих условию предиката
     */
    public static <T> IEnumerable<T> where(Iterable<T> collection, IPredicate<? super T> predicate)
    {
        return new Enumerable<T>(new PredicateIterator<T>(collection, predicate));
    }

    /**
     * Поиск элементов коллекции удовлетворяющих условию предиката
     * @param collection коллекция
     * @param predicate предикат
     * @param <T> тип элементов коллекции
     * @return Возвращает первый найденный элемент коллекции удовлетворяющий условию предиката
     */
    public static <T> T first(Iterable<T> collection, IPredicate<? super T> predicate)
    {
        for(T item : collection)
        {
            if(predicate.evaluate(item))
                return item;
        }
        return null;
    }

    /**
     * Определение количества элементов коллекции
     * @param collection коллекция
     * @param <T> тип элементов коллекции
     * @return Возвращает количество элементов коллекции
     */
    public static <T> int count(Iterable<T> collection)
    {
        int count = 0;
        for(T item : collection)
        {
                count++;
        }
        return count;
    }

    /**
     * Определение количества элементов коллекции удовлетворяющих условию предиката
     * @param collection коллекция
     * @param predicate предикат
     * @param <T> тип элементов коллекции
     * @return Возвращает количество элементов коллекции удовлетворяющих условию предиката
     */
    public static <T> int count(Iterable<T> collection, IPredicate<? super T> predicate)
    {
        int count = 0;
        for(T item : collection)
        {
            if(predicate.evaluate(item))
                count++;
        }
        return count;
    }

    public static <T, TItem> IEnumerable<T> orderBy(Iterable<T> collection, ISelector<T, TItem> selector)
    {
        ArrayList sortedList = Linq.toList(collection);
        Collections.sort(sortedList, new LinqComparator(selector));
        return new Enumerable<T>(sortedList.iterator());
    }

    public static <T> IEnumerable<T> take(Iterable<T> collection, int count)
    {
        return new Enumerable<T>(new SubsequenceIterator<T>(collection, count));
    }

    /**
     * Возвращает список элементов в коллекции
     */
    public static <T> ArrayList<T> toList(Iterable<T> collection)
    {
        ArrayList<T> result = new ArrayList<T>();
        for(T item : collection)
        {
            result.add(item);
        }
        return result;
    }

    /**
     * Возвращает массив элементов в коллекции
     * @param collection коллекция
     * @param itemClass класс элементов в коллекции
     * @return
     */
    public static <T> T[] toArray(Iterable<T> collection, Class<T> itemClass)
    {
        ArrayList<T> list = toList(collection);
        return Utils.castArray(list.toArray(), itemClass);
    }

    /**
     * Группировка элементов коллекции по заданному селектору
     * @param collection коллекция
     * @param selector селектор
     * @param <T> тип элементов коллекции
     * @return Возвращает список групп элементов по заданному селектору
     */
    /*public static <T, TResult> ILinqCollection<ILinqCollection<T>> groupBy(Iterable<T> collection, ISelector<T, TResult> selector)
    {
        HashMap<TResult, ILinqCollection<T>> result = new HashMap<TResult, ILinqCollection<T>>();
        for(T item : collection)
        {
            TResult itemSelectValue = selector.select(item);
            if(result.containsKey(itemSelectValue))
            {
                LinqList<T> list = (LinqList<T>)result.get(itemSelectValue);
                list.add(item);
            }
            else
            {
                LinqList<T> list = new LinqList<T>();
                list.add(item);
                result.put(itemSelectValue, list);
            }
        }
        return new LinqList<ILinqCollection<T>>(result.values());
    }*/

    /**
     * Выполняет действие над каждым элементом коллекции
     * @param collection коллекция
     * @param action действие
     * @param <T> тип элементов коллекции
     */
    public static <T> void forEach(Iterable<T> collection, Action<? super T> action)
    {
        for(T item : collection)
            action.doAction(item);
    }
}
