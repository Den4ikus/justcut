package com.jengine.collections.linq;

/**
 * Created by Денис on 10.04.2015.
 */
public interface Action<T>
{
    void doAction(T arg);
}
