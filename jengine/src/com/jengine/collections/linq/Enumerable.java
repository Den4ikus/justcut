package com.jengine.collections.linq;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Реализация перечисления с LINQ операциями
 */
final class Enumerable<T> implements IEnumerable<T>
{
    private final Iterator<T> _iterator;

    public Enumerable(Iterator<T> iterator)
    {
        _iterator = iterator;
    }

    //region IEnumerable

    /**
     * Проверяет удовлетворяет ли хотябы один элемент коллекции условию предиката
     *
     * @param predicate предикат
     * @return Возвращает true если хотябы один элемент коллекции удовлетворяет условию предиката, false в противном случае
     */
    @Override
    public boolean any(IPredicate<T> predicate)
    {
        return Linq.any(this, predicate);
    }

    /**
     * Проверяет удовлетворяют ли все элементы коллекции условию предиката
     *
     * @param predicate предикат
     * @return Возвращает true если все элементы коллекции удовлетворяют условию предиката, false в противном случае
     */
    @Override
    public boolean all(IPredicate<T> predicate)
    {
        return Linq.all(this, predicate);
    }

    /**
     * Перечисление элементов коллекции удовлетворяющих условию предиката
     *
     * @param predicate предикат
     * @return Возвращает перечисление элементов удовлетворяющих условию предиката
     */
    @Override
    public IEnumerable<T> where(IPredicate<T> predicate)
    {
        return new Enumerable<T>(new PredicateIterator<T>(this, predicate));
    }

    /**
     * Упорядочивание перечисления элементов по заданному селектору
     *
     * @param selector селектор
     * @return Перечисление упорядоченное по заданному селектору
     */
    @Override
    public <TItem> IEnumerable<T> orderBy(ISelector<T, TItem> selector)
    {
        return Linq.orderBy(this, selector);
    }

    /**
     * Перечисление заданного количества элементов перечисления
     *
     * @param count количество элементов
     * @return Возвращает перечисление заданного количества элементов перечисления
     */
    @Override
    public IEnumerable<T> take(int count)
    {
        return Linq.take(this, count);
    }

    /**
     * Поиск элементов коллекции удовлетворяющих условию предиката
     *
     * @param predicate предикат
     * @return Возвращает первый найденный элемент коллекции удовлетворяющий условию предиката
     */
    @Override
    public T first(IPredicate<T> predicate)
    {
        return Linq.first(this, predicate);
    }

    /**
     * Определение количества элементов коллекции удовлетворяющих условию предиката
     *
     * @param predicate предикат
     * @return Возвращает количество элементов коллекции удовлетворяющих условию предиката
     */
    @Override
    public int count(IPredicate<T> predicate)
    {
        return Linq.count(this, predicate);
    }

    /**
     * Преобразует элементы перечисления к заданному типу
     *
     * @param castType@return Возвращает перечисление с заданным типом элементов
     */
    @Override
    public <TCast> IEnumerable<TCast> cast(Class<TCast> castType)
    {
        return new Enumerable<TCast>(new CastIterator<T, TCast>(this));
    }

    /**
     * Возвращает список элементов в перечислении
     */
    @Override
    public ArrayList<T> toList()
    {
        return Linq.toList(this);
    }

    @Override
    public Iterator<T> iterator()
    {
        return _iterator;
    }

    //endregion
}
