package com.jengine.collections.linq;

import java.util.Iterator;

/**
 * Итератор части последовательности
 */
public class SubsequenceIterator<T> implements Iterator<T>
{
    private final Iterator<T> _iterator;
    private final int _count;
    private int _counter = 0;

    public SubsequenceIterator(Iterable<T> collection, int count)
    {
        _iterator = collection.iterator();
        _count = count;
    }

    //region IEnumerator

    @Override
    public boolean hasNext()
    {
        return _counter < _count && _iterator.hasNext();
    }

    @Override
    public T next()
    {
        _counter++;
        return _iterator.next();
    }

    @Override
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    // endregion
}
