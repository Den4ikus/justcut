package com.jengine.collections.linq;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Перечислитель коллекции с предикатом
 */
public final class PredicateIterator<T> implements Iterator<T>
{
    private final Iterator<T> _iterator;
    private final IPredicate<? super T> _predicate;
    private T _nextItem;

    public PredicateIterator(Iterable<T> collection, IPredicate<? super T> predicate)
    {
        _iterator = collection.iterator();
        _predicate = predicate;
    }

    //region IEnumerator

    @Override
    public boolean hasNext()
    {
        _nextItem = null;
        while(_iterator.hasNext())
        {
            _nextItem = _iterator.next();
            if(_predicate.evaluate(_nextItem)) break;
            else _nextItem = null;
        }
        return _nextItem != null;
    }

    @Override
    public T next()
    {
        if(_nextItem == null) throw new NoSuchElementException();
        return _nextItem;
    }

    @Override
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    // endregion
}
