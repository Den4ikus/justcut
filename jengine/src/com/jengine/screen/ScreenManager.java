package com.jengine.screen;

/**
 *
 */

import com.jengine.game.BaseScreen;
import com.jengine.game.IGameTime;
import com.jengine.game.application.IApplicationEventHandler;
import com.jengine.resources.ResourceManager;

import java.util.ArrayList;

/**
 *
 */
public final class ScreenManager implements IApplicationEventHandler
{
    private ResourceManager _resourceManager;
    private ArrayList<BaseScreen> _screens = new ArrayList<BaseScreen>();
    private BaseScreen _currentScreen;
    private int _screenWidth;
    private int _screenHeight;

    public ScreenManager(ResourceManager resourceManager)
    {
        _resourceManager = resourceManager;
    }

    public void addScreen(BaseScreen screen)
    {
        // Если есть текущий экран деактивируем его
        if(_currentScreen != null)
            _currentScreen.deactivate();

        if(!screen.isInitialized())
        {
            screen.load(_resourceManager);
            _resourceManager.waitLoad();
            screen.initialize();
        }

        screen.activate();
        screen.resize(_screenWidth, _screenHeight);
        _currentScreen = screen;

        _screens.add(0, screen);
    }

    public void removeScreen()
    {
        if(_currentScreen != null)
            removeScreen(_currentScreen);
    }

    public void removeScreen(BaseScreen screen)
    {
        screen.deactivate();
        screen.unload(_resourceManager);
        screen.dispose();
        _screens.remove(screen);

        // Если удаляется активный экран нужно активировать следующий за ним
        if(screen == _currentScreen)
        {
            if(!_screens.isEmpty())
            {
                BaseScreen prevScreen = _screens.get(0);
                prevScreen.activate();
                prevScreen.resize(_screenWidth, _screenHeight);
                _currentScreen = prevScreen;
            }
            else _currentScreen = null;
        }
    }

    public void replaceScreen(BaseScreen screen, BaseScreen newScreen)
    {
        if(!newScreen.isInitialized())
        {
            newScreen.load(_resourceManager);
            _resourceManager.waitLoad();
            newScreen.initialize();
        }

        // Если экран выбран как текущий нужно деактивировать старый и активировать новый
        if(screen == _currentScreen)
        {
            screen.deactivate();
            _currentScreen = newScreen;
            newScreen.activate();
            newScreen.resize(_screenWidth, _screenHeight);
        }

        screen.unload(_resourceManager);
        screen.dispose();

        int index = _screens.indexOf(screen);
        _screens.set(index, newScreen);
    }

    //region IApplicationEventHandler

    /**
     * Вызывается при запуске приложения
     */
    @Override
    public void appInitialize()
    {

    }

    /**
     * Вызывается при активации окна приложения (получение фокуса ввода)
     */
    @Override
    public void appResume()
    {
        if(_currentScreen != null)
            _currentScreen.resume();
    }

    /**
     * Вызывается при деактивации окна приложения (потеря фокуса ввода)
     */
    @Override
    public void appSuspend()
    {
        if(_currentScreen != null)
            _currentScreen.suspend();
    }

    /**
     * Вызывается при изменении размеров окна приложения
     *
     * @param width
     * @param height
     */
    @Override
    public void appResize(int width, int height)
    {
        _screenWidth = width;
        _screenHeight = height;
        if(_currentScreen != null)
            _currentScreen.resize(width, height);
    }

    /**
     * Метод реализующий обновление игровой логики
     * @param gameTime игровое время
     */
    @Override
    public void appUpdate(IGameTime gameTime)
    {
        if(_currentScreen == null) return;

        for(int i = 0; i < _screens.size(); i++)
        {
            ScreenOverlapMode overlapMode = _screens.get(i).getOverlapMode();
            if(overlapMode == ScreenOverlapMode.Overlay || overlapMode == ScreenOverlapMode.UpdateOverlayRenderTransparent)
            {
                // Рендерим все не перекрывающиеся экраны в обратном порядке
                for(int j = i; j >= 0; j--) _screens.get(j).update(gameTime);
                break;
            }
        }
    }

    /**
     * Метод реализующий рендеринг
     * @param gameTime игровое время
     */
    @Override
    public void appRender(IGameTime gameTime)
    {
        if(_currentScreen == null) return;

        for(int i = 0; i < _screens.size(); i++)
        {
            ScreenOverlapMode overlapMode = _screens.get(i).getOverlapMode();
            if(overlapMode == ScreenOverlapMode.Overlay || overlapMode == ScreenOverlapMode.UpdateTransparentRenderOverlay)
            {
                // Рендерим все не перекрывающиеся экраны в обратном порядке
                for(int j = i; j >= 0; j--) _screens.get(j).render(gameTime);
                break;
            }
        }
    }

    /**
     * Освобождает все ресурсы используемые приложением
     */
    @Override
    public void appDispose()
    {
        for(BaseScreen screen : _screens)
            screen.dispose();
    }

    //endregion
}
