package com.jengine.screen;

/**
 * Режим наложения экрана
 */
public enum ScreenOverlapMode
{
    /**
     * Для самого экрана не вызывается update и render, для ниже-лежащего экрана аналогично Transparent
     */
    /*Invisible,*/
    /**
     * Для ниже-лежащего экрана вызывается update и render
     */
    Transparent,
    /**
     * Для ниже-лежащего экрана вызывается только update
     */
    UpdateTransparentRenderOverlay,
    /**
     * Для ниже-лежащего экрана вызывается только render
     */
    UpdateOverlayRenderTransparent,
    /**
     * Для ниже-лежащего экрана методы update и render не вызываются
     */
    Overlay
}
