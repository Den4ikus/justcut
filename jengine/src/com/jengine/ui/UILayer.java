package com.jengine.ui;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.jengine.common.IDisposable;
import com.jengine.game.IGameTime;
import com.jengine.input.Input;
import com.jengine.resources.ResourceDatabase;

/**
 *
 */
public abstract class UILayer implements IDisposable
{
	private boolean _isVisible;
	
	public final Stage stage;
	
	protected UILayer()
	{
		//TODO поэксперементировать с вьюпортами возможно получится запилить автоматически масштабируемый UI
		stage = new Stage(new ScreenViewport());
	}

	public abstract void load(ResourceDatabase resourceDb);
	public abstract void unload(ResourceDatabase resourceDb);
	
	public boolean isVisible()
	{
		return _isVisible;
	}
	
	public void show()
	{
		show(false);
	}
	
	public void show(boolean topMost)
	{
		_isVisible = true;
		Input.addProcessor(stage, topMost);
	}
	
	public void hide()
	{
		_isVisible = false;
		Input.removeProcessor(stage);
	}
	
	public void resize(int screenWidth, int screenHeight)
	{
		stage.getViewport().update(screenWidth, screenHeight);
	}
	
	public void update(IGameTime gameTime)
	{
		stage.act(gameTime.getElapsedTime());
	}

	public void render(IGameTime gameTime)
	{
		stage.draw();
	}
	
	public void dispose()
	{
		if(stage != null)
			stage.dispose();
	}
}
