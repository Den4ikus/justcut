package com.jengine.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Объединяет несколько Drawable объектов в один
 */
public final class DrawableGroup implements Drawable
{
    private Drawable[] _drawableObjects;

    public DrawableGroup(Drawable[] drawableObjects)
    {
        _drawableObjects = drawableObjects;
    }

    /**
     * Draws this drawable at the specified bounds. The drawable should be tinted with {@link Batch#getColor()}, possibly by mixing
     * its own color.
     *
     * @param batch
     * @param x
     * @param y
     * @param width
     * @param height
     */
    @Override
    public void draw(Batch batch, float x, float y, float width, float height)
    {
        for(int i = 0; i < _drawableObjects.length; i++)
            _drawableObjects[i].draw(batch, x, y, width, height);
    }

    @Override
    public float getLeftWidth()
    {
        return _drawableObjects[0].getLeftWidth();
    }

    @Override
    public void setLeftWidth(float leftWidth)
    {
        for(int i = 0; i < _drawableObjects.length; i++)
            _drawableObjects[i].setLeftWidth(leftWidth);
    }

    @Override
    public float getRightWidth()
    {
        return _drawableObjects[0].getRightWidth();
    }

    @Override
    public void setRightWidth(float rightWidth)
    {
        for(int i = 0; i < _drawableObjects.length; i++)
            _drawableObjects[i].setRightWidth(rightWidth);
    }

    @Override
    public float getTopHeight()
    {
        return _drawableObjects[0].getTopHeight();
    }

    @Override
    public void setTopHeight(float topHeight)
    {
        for(int i = 0; i < _drawableObjects.length; i++)
            _drawableObjects[i].setTopHeight(topHeight);
    }

    @Override
    public float getBottomHeight()
    {
        return _drawableObjects[0].getBottomHeight();
    }

    @Override
    public void setBottomHeight(float bottomHeight)
    {
        for(int i = 0; i < _drawableObjects.length; i++)
            _drawableObjects[i].setBottomHeight(bottomHeight);
    }

    @Override
    public float getMinWidth()
    {
        return _drawableObjects[0].getMinWidth();
    }

    @Override
    public void setMinWidth(float minWidth)
    {
        for(int i = 0; i < _drawableObjects.length; i++)
            _drawableObjects[i].setMinWidth(minWidth);
    }

    @Override
    public float getMinHeight()
    {
        return _drawableObjects[0].getMinHeight();
    }

    @Override
    public void setMinHeight(float minHeight)
    {
        for(int i = 0; i < _drawableObjects.length; i++)
            _drawableObjects[i].setMinHeight(minHeight);
    }
}
