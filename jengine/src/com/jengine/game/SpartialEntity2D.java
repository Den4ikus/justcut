package com.jengine.game;

import com.badlogic.gdx.math.Affine2;
import com.jengine.math.bounds.IBoundingShape2D;

/**
 * Представляет сущность которая имеет размеры и положение в пространстве
 */
public abstract class SpartialEntity2D<T extends EntityType> extends Entity<T>
{
    private static final Affine2 _transform = new Affine2();

	public final ITransformationProvider transformation;
	public final IBoundingShape2D bounds;
	public final boolean isStatic;
	
	//public final IEvent<EventArgs> eventCollide = new Event<EventArgs>();
   
    public SpartialEntity2D(T type, ITransformationProvider transformation, IBoundingShape2D bounds)
    {
		this(type, transformation, bounds, false);
    }
   
    public SpartialEntity2D(T type, ITransformationProvider transformation, IBoundingShape2D bounds, boolean isStatic)
    {
        super(type);
        this.transformation = transformation;
        this.bounds = bounds;
		this.isStatic = isStatic;
		
		// для статических тел сразу пересчитываем ограничивающую фигуру, больше она меняться не будет
		if(isStatic)
		{
			_transform.set(transformation.get());
			bounds.transformApply(_transform);
		}
    }

    @Override
    public void update(IGameTime gameTime)
    {
		// статические тела не должны перемещаться и для них можно не пересчитывать ограничивающую фигуру
		if(!isStatic)
		{
			//todo нужно оценить производительность, возможно придется делать ленивую трансформацию
			_transform.set(transformation.get());
			bounds.transformRollback();
			bounds.transformApply(_transform);
		}
        onUpdate(gameTime);
    }

    protected abstract void onUpdate(IGameTime gameTime);


    /*void doCollide(SpartialEntity2D entity)
	{
		((Event)this.eventCollide).doEvent(this, new CollideEventArgs(entity));
	}*/
}
