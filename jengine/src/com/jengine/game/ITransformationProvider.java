package com.jengine.game;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Интерфейс предоставляющий трансформацию для объектов имеющих положение в пространстве
 */
public interface ITransformationProvider
{
	void set(ITransformationProvider tranfromation);
	Matrix4 get();
	Matrix4 getInv();
	Quaternion getRotation();
	
    Vector3 getPosition();
	float getPositionX();
	float getPositionY();
	float getPositionZ();
    void setPosition(Vector3 pos);
	void setPosition(float x, float y, float z);
	
	Vector3 getAngles();
	float getYaw();
	float getPitch();
	float getRoll();
	void setAngles(Vector3 angles);
	void setAngles(float yaw, float pitch, float roll);

	Vector3 getScale();
	float getScaleX();
	float getScaleY();
	float getScaleZ();
	void setScale(Vector3 scale);
	void setScale(float scaleX, float scaleY, float scaleZ);

	Vector3 getForward();
	void setForward(Vector3 forward);
	void setForward(float forwardX, float forwardY, float forwardZ);

	Vector3 getUp();
	void setUp(Vector3 up);
	void setUp(float upX, float upY, float upZ);

	/**
	 * Преобразует мировые координаты в локальные. ВНИМАНИЕ!!! Возвращается объект из пула
	 */
	Vector2 toLocal(Vector2 v);

	/**
	 * Преобразует мировые координаты в локальные. ВНИМАНИЕ!!! Возвращается объект из пула
	 */
    Vector3 toLocal(Vector3 v);

	/**
	 * Преобразует мировые координаты в локальные. ВНИМАНИЕ!!! Возвращается объект из пула
	 */
	Vector3 toLocal(float x, float y, float z);

    Vector3 toWorld(Vector3 v);
	Vector3 toWorld(float x, float y, float z);
}
