package com.jengine.game;

import com.jengine.input.IInputHandler;
import com.jengine.input.Input;
/**
 * Базовый класс для контроллеров сущностей, которым требуется обработка пользовательского ввода
 */
public abstract class EntityInputController<TScene extends Scene, TEntity extends Entity> extends EntityController<TScene, TEntity> implements IInputHandler
{
	protected EntityInputController(TScene scene, TEntity entity)
	{
		super(scene, entity);
		Input.addHandler(this);
	}
	
	@Override
	protected void onDelete()
	{
		Input.removeHandler(this);
	}
}