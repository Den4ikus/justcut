package com.jengine.game;

import com.badlogic.gdx.math.Vector2;
import com.jengine.cameras.Camera;
import com.jengine.collections.linq.IEnumerable;
import com.jengine.collections.linq.IPredicate;
import com.jengine.collections.linq.Linq;
import com.jengine.game.scene.serialization.SceneData;
import com.jengine.math.Ray2D;
import com.jengine.math.bounds.IBoundingShape2D;
import com.jengine.viewports.Viewport;

import java.util.HashMap;


public abstract class SpartialScene2D<T extends SceneData> extends Scene<T>
{
	public final IBoundingShape2D bounds;
	
	public SpartialScene2D(String sceneDataName,
                           Class<T> sceneDataType,
                           HashMap<String, IEntityFactory> factoryMap,
						   Camera camera,
						   Viewport viewport,
						   IBoundingShape2D bounds)
	{
		super(sceneDataName, sceneDataType, factoryMap, camera, viewport);
		this.bounds = bounds;
	}
	
	public SpartialEntity2D getFirstAtPoint(Vector2 point)
	{
		return getFirstAtPoint(point, IPredicate.AlwaysTrue);
	}
	
	public SpartialEntity2D getFirstAtPoint(Vector2 point, IPredicate<SpartialEntity2D> filter)
	{
		for(int i = 0; i < entities.size(); i++)
		{
			Entity e = entities.get(i);
			if(e instanceof SpartialEntity2D)
			{		
				SpartialEntity2D se = (SpartialEntity2D)e;
				if(filter.evaluate(se) && se.bounds.contains(point))
				{
					return se;
				}					
			}
		}
		return null;
	}
	
	public IEnumerable<SpartialEntity2D> getAllAtPoint(final Vector2 point)
	{
		return getAllAtPoint(point, IPredicate.AlwaysTrue);
	}
	
	public IEnumerable<SpartialEntity2D> getAllAtPoint(final Vector2 point, final IPredicate<SpartialEntity2D> filter)
	{
        final IPredicate atPointPredicate = new IPredicate<Entity>()
        {
            @Override
            public boolean evaluate(Entity value)
            {
                if(value instanceof SpartialEntity2D)
                {
                    SpartialEntity2D se = (SpartialEntity2D)value;
                    return filter.evaluate(se) && se.bounds.contains(point);
                }
                return false;
            }
        };

        return Linq.where(entities, atPointPredicate);
	}
	
	public IEnumerable<SpartialEntity2D> getAllFromArea(final IBoundingShape2D area)
	{
		return getAllFromArea(area, IPredicate.AlwaysTrue);
	}
	
	public IEnumerable<SpartialEntity2D> getAllFromArea(final IBoundingShape2D area, final IPredicate<SpartialEntity2D> filter)
	{
		final IPredicate atAreaPredicate = new IPredicate<Entity>()
        {
            @Override
            public boolean evaluate(Entity value)
            {
                if(value instanceof SpartialEntity2D)
                {
                    // используется соглашение - точка привязки всегда находится внутри оболочки тела т.к. это значительно упрощает и ускоряет некоторые расчеты
                    // добавляем все объекты у которых точка привязки находится внутри области т.к. это 100% гарантия пересечения
                    SpartialEntity2D se = (SpartialEntity2D)value;
                    float x = se.transformation.getPositionX();
                    float y = se.transformation.getPositionY();
                    return filter.evaluate(se) && (area.contains(x, y) || area.intersectWith(se.bounds));
                }
                return false;
            }
        };

        return Linq.where(entities, atAreaPredicate);
	}

	/*public boolean existsAtArea(IBoundingShape2D area, IPredicate<SpartialEntity2D> filter)
	{
		// используется соглашение - точка привязки всегда находится внутри оболочки тела т.к. это значительно упрощает и ускоряет расчеты
		// добавляем все объекты у которых точка привязки находится внутри области т.к. это 100% гарантия пересечения
		for(int i = 0; i < entities.size(); i++)
		{
			Entity e = entities.get(i);
			if(e instanceof SpartialEntity2D)
			{
				SpartialEntity2D se = (SpartialEntity2D) e;
				float x = se.transformation.getPositionX();
				float y = se.transformation.getPositionY();
				if(filter.evaluate(se) && (area.contains(x, y) || area.intersectWith(se.bounds))) return true;
			}
		}
		return false;
	}*/

	public IEnumerable<SpartialEntity2D> rayCast(final Ray2D ray)
	{
		return rayCast(ray, IPredicate.AlwaysTrue);
	}
	
	public IEnumerable<SpartialEntity2D> rayCast(final Ray2D ray, final IPredicate<SpartialEntity2D> filter)
	{
        final IPredicate intersectsWithRay = new IPredicate()
        {
            @Override
            public boolean evaluate(Object value)
            {
                if(value instanceof SpartialEntity2D)
                {
                    SpartialEntity2D se = (SpartialEntity2D)value;
                    return filter.evaluate(se) && se.bounds.intersectWith(ray);
                }
                return false;
            }
        };

        return Linq.where(entities, intersectsWithRay);
	}

	//todo: переделать поиск с использованием регулярной сетки
	/*public void pop(SpartialEntity2D entity)
	{
		
	}
	
	public void push(SpartialEntity2D entity)
	{
		
	}*/
}