package com.jengine.game;

import com.jengine.common.EventArgs;

/**
 * 
 */
public class SceneUpdateEventArgs extends EventArgs
{
    public final IGameTime gameTime;

    public SceneUpdateEventArgs(IGameTime gameTime)
    {
        this.gameTime = gameTime;
    }
}
