package com.jengine.game;

import java.util.HashMap;

/**
 * Created by Денис on 10.07.2015.
 * Класс содержащий локализуемые строки игры
 */
public final class Localization
{
    private HashMap<String, String> strings = new HashMap<String, String>();

    public String getLocalized(String localizableString)
    {
        String result = strings.get(localizableString);
        return result != null ? result : localizableString;
    }
}
