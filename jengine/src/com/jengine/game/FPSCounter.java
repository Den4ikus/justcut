package com.jengine.game;

/**
 * Счетчик FPS
 */
public final class FPSCounter
{
    private float timeCounter = 0f;
    private int fpsCounter = 0;
    private float period = 0.25f;
    private float fpsValue = 0f;

    public float getFPS()
    {
        return fpsValue;
    }

    public float getPeriod()
    {
        return period;
    }
    public void setPeriod(float period)
    {
        this.period = period;
        fpsCounter = 0;
        timeCounter = 0f;
    }

    public void frame(float elapsedTime)
    {
        timeCounter += elapsedTime;
        if (timeCounter < period) fpsCounter++;
        else
        {
            fpsValue = (fpsValue + (fpsCounter / period)) * 0.5f;
            timeCounter = 0f;
            fpsCounter = 0;
        }
    }
}
