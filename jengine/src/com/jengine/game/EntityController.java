package com.jengine.game;

import com.jengine.common.EventArgs;
import com.jengine.common.IEventHandler;

/**
 * Базовый класс для контроллеров сущностей
 */
public abstract class EntityController<TScene extends Scene, TEntity extends Entity>
{
    private final IEventHandler<UpdateEventArgs> _updateHandler = new IEventHandler<UpdateEventArgs>()
    {
        @Override
        public void doHandle(Object sender, UpdateEventArgs args)
        {
            onUpdate(args.gameTime);
        }
    };

	protected TScene scene;
	protected TEntity entity;
	
	protected EntityController(final TScene scene, TEntity entity)
	{
		this.scene = scene;
		this.entity = entity;

        scene.eventUpdate.attach(_updateHandler);
		entity.eventDelete.attach(new IEventHandler<EventArgs>()
		{
			@Override
			public void doHandle(Object sender, EventArgs args)
			{
                scene.eventUpdate.detach(_updateHandler);
                ((Entity)sender).eventDelete.detach(this); // todo куда указывает this?
				onDelete();
 			}
		});
	}

    /**
     * Вызывается при обновлении сцены
     */
    protected abstract void onUpdate(IGameTime gameTime);

    /**
     * Вызывается при удалении контролируемой сущности
     */
    protected abstract void onDelete();
}