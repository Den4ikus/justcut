package com.jengine.game;

import com.badlogic.gdx.Gdx;
import com.jengine.graphics.ISceneView;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenOverlapMode;

/**
 * Базовый класс для игровых экранов, отображающих сцену
 */
public abstract class SceneScreen<TGame extends Game, TScene extends Scene> extends BaseScreen<TGame>
{
    protected TScene scene;
    protected ISceneView<? super TScene>[] renderers;

    protected SceneScreen(TGame game, ScreenOverlapMode overlapMode, TScene scene, ISceneView<? super TScene>... renderers)
    {
        super(game, overlapMode);
        this.scene = scene;
        this.renderers = renderers;
    }

    @Override
    public void initialize()
    {
        scene.initialize();
        for(ISceneView renderer : renderers)
            renderer.initialize();

        // Вызываем метод родительского класса который проставляет isInitialized
        super.initialize();
    }

    @Override
    public void load(ResourceManager resourceManager)
    {
        scene.load(resourceManager);
        for(ISceneView renderer : renderers)
            renderer.load(resourceManager);
    }

    @Override
    public void unload(ResourceManager resourceManager)
    {
        scene.unload(resourceManager);
        for(ISceneView renderer : renderers)
            renderer.unload(resourceManager);
    }

    /**
     * Вызывается при изменении разрешения окна приложения
     *
     * @param width
     * @param height
     */
    public void resize(int width, int height)
    {
        scene.resize(width, height);
        for(ISceneView renderer : renderers)
            renderer.resize(width, height);
    }

    /**
     * Метод в котором реализуется обновление игровой логики
     * @param gameTime класс представляющий игровое время
     */
    public final void update(IGameTime gameTime)
    {
        scene.update(gameTime);
    }

    /**
     * Метод в котором реализуется рендеринг
     * @param gameTime класс представляющий игровое время
     */
    public final void render(IGameTime gameTime)
    {
        for(int i = 0; i < renderers.length; i++)
		{
			ISceneView<? super TScene> render = renderers[i];
            long begin = System.currentTimeMillis();
			render.render(scene);
            long end = System.currentTimeMillis();
            Gdx.app.debug(null,String.format("%s:%d ms", render.getClass().getSimpleName(), end - begin));
        }
    }
	
	@Override
	public void dispose()
	{
		if(scene != null)
			scene.dispose();

        for(int i = 0; i < renderers.length; i++)
            renderers[i].dispose();
	}
}
