package com.jengine.game.application;

import com.jengine.game.IGameTime;

/**
 *
 */
final class GameTimeManager implements IGameTime
{
    private final float MillisecondTicks = 1000000f;
    private final float SecondTicks = 1000000000f;
    private long _updateTime = System.nanoTime();
    private float _elapsedTime = 0f;
    private float _elapsedMilliseconds = 0f;

    public void update()
    {
        long currentTime = System.nanoTime();
        long timeDiff = currentTime - _updateTime;
        _elapsedMilliseconds = timeDiff / MillisecondTicks;
        _elapsedTime = timeDiff / SecondTicks;
        _updateTime = currentTime;
    }

    //region IGameTime

    @Override
    public float getElapsedTime()
    {
        return _elapsedTime;
    }

    /**
     * @return Возвращает прошедшее время в миллисекундах
     */
    @Override
    public float getElapsedMilliseconds()
    {
        return _elapsedMilliseconds;
    }

    //endregion
}
