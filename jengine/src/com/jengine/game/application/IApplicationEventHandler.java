package com.jengine.game.application;

import com.jengine.game.IGameTime;

/**
 * Интерфейс событий приложения
 */
public interface IApplicationEventHandler
{
    /**
     * Вызывается при запуске приложения
     */
    void appInitialize();

    /**
     * Вызывается при активации окна приложения (получение фокуса ввода)
     */
    void appResume();

    /**
     * Вызывается при деактивации окна приложения (потеря фокуса ввода)
     */
    void appSuspend();

    /**
     * Вызывается при изменении размеров окна приложения
     * @param width
     * @param height
     */
    void appResize(int width, int height);

    /**
     * Метод реализующий обновление игровой логики
     * @param gameTime игровое время
     */
    void appUpdate(IGameTime gameTime);

    /**
     * Метод реализующий рендеринг
     * @param gameTime игровое время
     */
    void appRender(IGameTime gameTime);

    // Оба интерфейса ApplicationListener и IApplicationEventHandler
    // реализуют IDisposable чтобы исключить конфликты добавляем префикс app
    /**
     * Освобождает все ресурсы используемые приложением
     */
    void appDispose();
}
