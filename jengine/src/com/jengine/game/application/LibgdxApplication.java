package com.jengine.game.application;

import com.badlogic.gdx.ApplicationListener;

/**
 * Враппер над стандартными событиями приложения libgdx, преобразует их в формат IApplicationEventHandler
 */
public abstract class LibgdxApplication implements ApplicationListener, IApplicationEventHandler
{
    private GameTimeManager _gameTimeManager = new GameTimeManager();

    //region ApplicationListener

    @Override
    public final void create()
    {
        appInitialize();
    }

    @Override
    public final void pause()
    {
        appSuspend();
    }

    @Override
    public final void resume()
    {
        appResume();
    }

    @Override
    public final void resize(int width, int height)
    {
        appResize(width, height);
    }

    @Override
    public final void render()
    {
        _gameTimeManager.update();
        appUpdate(_gameTimeManager);
        appRender(_gameTimeManager);
    }

    @Override
    public final void dispose()
    {
        appDispose();
    }

    //endregion
}