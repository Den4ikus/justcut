package com.jengine.game;

import com.jengine.common.EventArgs;

/**
 *
 */
public class UpdateEventArgs extends EventArgs
{
    public final IGameTime gameTime;

    public UpdateEventArgs(IGameTime gameTime)
    {
        this.gameTime = gameTime;
    }
}
