package com.jengine.game;

import com.badlogic.gdx.Gdx;
import com.jengine.common.logs.LogLevel;
import com.jengine.common.logs.Logger;
import com.jengine.game.application.LibgdxApplication;
import com.jengine.resources.ResDescriptor;
import com.jengine.resources.ResType;
import com.jengine.resources.ResourceDatabase;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenManager;

import java.util.HashMap;

/**
 * Базовый класс игры
 */
public abstract class Game extends LibgdxApplication
{
    protected ResourceManager resourceManager;
    public ResourceManager getResourceManager()
    {
        return resourceManager;
    }

    protected ScreenManager screenManager;
    public ScreenManager getScreenManager()
    {
        return screenManager;
    }

    //region IApplicationEventHandler

    /**
     * Вызывается при запуске приложения
     */
    @Override
    public void appInitialize()
    {
        //TODO выпилить логи куда нибудь)
        Logger.initialize("", LogLevel.DEBUG);
        Gdx.app.debug("app","initialize");

        //TODO: типа грузим ресурсы из конфига
        //десериализуем БД с ресурсами
        HashMap<String, ResDescriptor> resources = new HashMap<String, ResDescriptor>()
            {{
                put("ShapeContourShader", new ResDescriptor(ResType.Shader, "ShapeContourShader", "shaders/line_shader.glsl", true));
                put("RGBMeshShader", new ResDescriptor(ResType.Shader, "RGBMeshShader", "shaders/mesh_shader.glsl", true));
                put("SobelFilterShader", new ResDescriptor(ResType.Shader, "SobelFilterShader", "shaders/sobel_filter.glsl", true));
                put("RobertsFilterShader", new ResDescriptor(ResType.Shader, "RobertsFilterShader", "shaders/roberts_filter.glsl", true));
                put("ScreenSpaceTextureShader", new ResDescriptor(ResType.Shader, "ScreenSpaceTextureShader", "shaders/screen_space_texture_shader.glsl", true));
                put("FPSFont", new ResDescriptor(ResType.Font, "FPSFont", "default.fnt", true));
                put("UISkin", new ResDescriptor(ResType.Skin, "UISkin", "ui.json", true));
                put("MainMenuBackground", new ResDescriptor(ResType.Texture, "MainMenuBackground", "menu_background.png", true));
                put("ColoredMainMenuBackground", new ResDescriptor(ResType.Texture, "ColoredMainMenuBackground", "colored_background.png", true));
                put("TextMainMenuBackground", new ResDescriptor(ResType.Texture, "TextMainMenuBackground", "text_background.png", true));
                put("Level1Preview", new ResDescriptor(ResType.Texture, "Level1Preview", "levels/level1_preview.png", true));
                put("Level2Preview", new ResDescriptor(ResType.Texture, "Level2Preview", "levels/level2_preview.png", true));
                put("GoldStar", new ResDescriptor(ResType.Texture, "GoldStar", "textures/gold_star.png", true));
                put("SilverStar", new ResDescriptor(ResType.Texture, "SilverStar", "textures/silver_star.png", true));
                put("BronzeStar", new ResDescriptor(ResType.Texture, "BronzeStar", "textures/bronze_star.png", true));
                put("TestLevel", new ResDescriptor(ResType.CustomResource, "TestLevel", "levels/test.scene", false));
            }};

        ResDescriptor grayscaleAtlas = new ResDescriptor(ResType.TextureAtlas, "GrayscaleTexAtlas", "textures/grayscale.atlas", true);
        resources.put(grayscaleAtlas.name, grayscaleAtlas);

        resourceManager = new ResourceManager(new ResourceDatabase(resources));
        screenManager = new ScreenManager(resourceManager);
    }

    /**
     * Вызывается при активации окна приложения
     */
    @Override
    public void appResume()
    {
        Gdx.app.debug("app","resume");
        screenManager.appResume();
    }

    /**
     * Вызывается при деактивации окна приложения
     */
    @Override
    public void appSuspend()
    {
        Gdx.app.debug("app","pause");
        screenManager.appSuspend();
    }

    /**
     * Вызывается при изменении разрешения окна игры
     * @param width
     * @param height
     */
    @Override
    public void appResize(int width, int height)
    {
        Gdx.app.debug("app","resize");
        screenManager.appResize(width, height);
    }

    /**
     * Метод реализующий обновление игровой логики
     * @param gameTime игровое время
     */
    @Override
    public void appUpdate(IGameTime gameTime)
    {
        resourceManager.update();
        screenManager.appUpdate(gameTime);
    }

    @Override
    public void appRender(IGameTime gameTime)
    {
        screenManager.appRender(gameTime);
    }

    @Override
    public void appDispose()
    {
        Gdx.app.debug("app","dispose");

        if(resourceManager != null)
            resourceManager.dispose();

        if(screenManager != null)
            screenManager.appDispose();
    }

    //endregion
}
