package com.jengine.game.scene;

import com.jengine.game.IEntityFactory;
import com.jengine.game.EntityType;
import com.jengine.game.Scene;

import java.util.HashMap;

/**
 *
 */
public final class EntityFactoryDatabase<TScene extends Scene>
{
    private final HashMap<String, IEntityFactory> _factoryMap;

    public EntityFactoryDatabase(HashMap<String, IEntityFactory> factoryMap)
    {
        _factoryMap = factoryMap;
    }

    public <T extends EntityType> IEntityFactory<TScene,T> get(T entityType)
    {
        //TODO: или getName?
        String entityTypeName = entityType.getClass().getSimpleName();
        return (IEntityFactory<TScene,T>)_factoryMap.get(entityTypeName);
    }
}
