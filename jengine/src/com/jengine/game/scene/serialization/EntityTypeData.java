package com.jengine.game.scene.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.jengine.game.EntityType;

import java.util.HashMap;

/**
 * Класс содержащий сериализуемые данные типов сущностей сцены
 */
public final class EntityTypeData implements Json.Serializable
{
    private HashMap<String, EntityType> _types;

    public EntityType getType(String typeName)
    {
        return _types.get(typeName);
    }

    /**
     * Конструктор без параметров для десериализации
     */
    protected EntityTypeData()
    {
    }

    public EntityTypeData(HashMap<String, EntityType> types)
    {
        _types = types;
    }

    //region Json.Serializable

    @Override
    public void write(Json json)
    {
        json.writeValue("EntityTypes", _types);
    }

    @Override
    public void read(Json json, JsonValue jsonData)
    {
        _types = json.readValue(HashMap.class, jsonData.get("EntityTypes"));
    }

    //endregion
}
