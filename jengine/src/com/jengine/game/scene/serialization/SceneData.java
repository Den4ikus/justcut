package com.jengine.game.scene.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.jengine.game.EntityType;

import java.util.HashMap;

/**
 * Класс содержащий сериализуемые данные сцены
 */
public class SceneData implements Json.Serializable
{
    private HashMap<String, EntityType> _types;
    //TODO переделать на список если нужно будет создавать сущности через json
    //private HashMap<String, Transformation> _instances;

    public EntityType getType(String typeName)
    {
        return _types.get(typeName);
    }

    /*public HashMap<String, Transformation> getInstances()
    {
        return _instances;
    }*/

    /**
     * Конструктор без параметров для десериализации
     */
    protected SceneData()
    {}

    public SceneData(HashMap<String, EntityType> typeMap)
    {
        _types = typeMap;
        //_instances = instanceMap;
    }

    //region Json.serializable

    @Override
    public void write(Json json)
    {
        json.writeValue("Types", _types);
    }

    @Override
    public void read(Json json, JsonValue jsonData)
    {
        _types = json.readValue(HashMap.class, jsonData.get("Types"));
    }

    //endregion
}
