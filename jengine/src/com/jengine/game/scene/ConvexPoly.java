package com.jengine.game.scene;

import com.badlogic.gdx.math.Vector2;

// вершины должны описывать контур по часовой стрелке
public final class ConvexPoly
{
	public final Vector2[] vertices;
	public final int vertexCount;

	public ConvexPoly(Vector2[] vertices)
	{
		this.vertices = vertices;
		this.vertexCount = vertices.length;
	}

	public boolean contains(Vector2 point)
	{
		// выкинута проверка на ограничивающий прямоугольник
		// т.к. метод будет вызываться для полигонов которые в большинстве случаев содержат точку
		/*
		float minX = vertices[0].x;
		float maxX = vertices[0].x;
		float minY = vertices[0].y;
		float maxY = vertices[0].y;
		for(int i = 1; i < vertices.length; i++)
		{
			Vector2 t = vertices[i];
			if(t.x < minX) minX = t.x;
			if(t.x > maxX) maxX = t.x;
			if(t.y < minY) minY = t.y;
			if(t.y > maxY) maxY = t.y;
		}

		if (point.y < minX || point.X > maxX ||
			point.Y < minY || point.Y > maxY ) return false;
		*/

		// алгоритм
		// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
		boolean inside = false;
		for(int i = 0, j = vertices.length - 1; i < vertices.length ; j = i++ )
		{
			if ((vertices[i].y > point.y) != (vertices[j].y > point.y) &&
				 point.x < (vertices[j].x - vertices[i].x) * (point.y - vertices[i].y ) / (vertices[j].y - vertices[i].y) + vertices[i].x)
			{
				inside = !inside;
			}
		}
		return inside;
	}

	public Vector2[] triangulate()
	{
		int triangleCount = vertexCount - 1;
		Vector2[] triangulation = new Vector2[triangleCount * 3];
		int vertexIndex = 0;

		final int firstForwardIndex = 0;
		final int firstBackwardIndex = vertexCount - 1;
		int forwardIndex = firstForwardIndex;
		int backwardIndex = firstBackwardIndex;

		while(vertexIndex < vertexCount)
		{
			// определяем в какую сторону двигаться по контуру
			if(forwardIndex - firstForwardIndex <= firstBackwardIndex - backwardIndex)
			{
				// двигаемся вперед
				triangulation[vertexIndex++] = vertices[forwardIndex++];
				triangulation[vertexIndex++] = vertices[forwardIndex];
				triangulation[vertexIndex++] = vertices[backwardIndex];
			}
			else
			{
				// двигаемся назад
				triangulation[vertexIndex++] = vertices[backwardIndex--];
				triangulation[vertexIndex++] = vertices[forwardIndex];
				triangulation[vertexIndex++] = vertices[backwardIndex];
			}
		}

		// каждые 3 вершины массива описывают треугольник, вершины идут по часовой стрелке
		return triangulation;
	}
}
