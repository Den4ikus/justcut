package com.jengine.game.scene;

import com.jengine.game.EntityType;

import java.util.HashMap;

/**
 *
 */
public final class EntityTypeDatabase
{
    private final HashMap<String, EntityType> _typeMap;

    public EntityTypeDatabase(HashMap<String, EntityType> typeMap)
    {
        _typeMap = typeMap;
    }

    public EntityType get(String entityTypeName)
    {
        return _typeMap.get(entityTypeName);
    }
}
