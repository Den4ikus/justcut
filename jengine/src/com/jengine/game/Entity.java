package com.jengine.game;

import com.jengine.common.Event;
import com.jengine.common.EventArgs;
import com.jengine.common.IDisposable;
import com.jengine.common.IEvent;

/**
 * Базовый класс для сущностей сцены
 */
public abstract class Entity<T extends EntityType> implements IDisposable
{
	private static int InstanceIdCounter = 0;
	
	public final int instanceId;
	public final T type;
    public boolean visible = true;
	public Object tag = null;
	
	//region Events

    /**
     * Событие возникающее при удалении сущности
     */
	public final IEvent<EventArgs> eventDelete = new Event<EventArgs>();
	
	protected void onDelete()
	{
		((Event)eventDelete).doEvent(this, EventArgs.Empty);
	}
	
	//endregion

	public Entity()
	{
        this(null);
	}

    public Entity(T type)
    {
		this.instanceId = InstanceIdCounter++;
        this.type = type;

        // Увеличиваем счетчик инстансов для типа
        if(type != null)
            type.onCreateInstance();
    }

    /**
     * Освобождает все ресурсы и генерирует событие Delete
     */
    public void dispose()
    {
        // Уменьшаем счетчик инстансов для типа
        if(type != null)
            type.onDeleteInstance();

        onDelete();
    }

    public abstract void update(IGameTime gameTime);
}