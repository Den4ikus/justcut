package com.jengine.game;

/**
 * Представляет игровое время
 */
public interface IGameTime
{
    /**
     * @return Возвращает прошедшее время в секундах
     */
    float getElapsedTime();

    /**
     * @return Возвращает прошедшее время в миллисекундах
     */
    float getElapsedMilliseconds();
}
