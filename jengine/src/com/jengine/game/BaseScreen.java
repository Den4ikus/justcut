package com.jengine.game;

import com.jengine.common.IDisposable;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenOverlapMode;

/**
 * Базовый класс для игровых экранов
 */
public abstract class BaseScreen<TGame extends Game> implements IDisposable
{
    private boolean _isInitialized = false;
    private ScreenOverlapMode _overlapMode;

    protected TGame game;
    protected int width;
    protected int height;

    //region Properties

    /**
     * Возвращает был ли проинициализирован экран (инициализированный экран готов к немедленному началу рендеринга)
     */
    public final boolean isInitialized()
    {
        return _isInitialized;
    }

    /**
     * Возвращает режим наложения экрана
     */
    public final ScreenOverlapMode getOverlapMode()
    {
        return _overlapMode;
    }

    /**
     * Задает режим наложения экрана
     * @param overlapMode
     */
    public final void setOverlapMode(ScreenOverlapMode overlapMode)
    {
        _overlapMode = overlapMode;
    }

    //endregion

    public BaseScreen(TGame game, ScreenOverlapMode overlapMode)
    {
        this.game = game;
        _overlapMode = overlapMode;
    }

    //region Methods

    /**
     * Вызывается для инициализации после загрузки ресурсов
     */
    public void initialize()
    {
        _isInitialized = true;
    }

    /**
     * Вызывается для загрузки ресурсов экрана
     *
     * @param resourceManager
     */
    public void load(ResourceManager resourceManager)
    {}

    /**
     * Вызывается для выгрузки ресурсов экрана
     *
     * @param resourceManager
     */
    public void unload(ResourceManager resourceManager)
    {}

    /**
     * Вызывается при активации экрана (активным считается текущий экран)
     */
    public void activate()
    {
    }

    /**
     * Вызывается при деактивации экрана (активным считается текущий экран)
     */
    public void deactivate()
    {}

    /**
     * Вызывается при активации окна приложения (получение фокуса ввода)
     */
    public void resume()
    {}

    /**
     * Вызывается при деактивации окна приложения (потеря фокуса ввода)
     */
    public void suspend()
    {}

    /**
     * Вызывается при изменении размеров окна приложения
     * @param width
     * @param height
     */
    public void resize(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    /**
     * Метод реализующий обновление игровой логики
     * @param gameTime класс представляющий игровое время
     */
    public void update(IGameTime gameTime)
    {}

    /**
     * Метод реализующий рендеринг
     * @param gameTime класс представляющий игровое время
     */
    public void render(IGameTime gameTime)
    {}

    //endregion
}
