package com.jengine.game;

import com.badlogic.gdx.Gdx;
import com.jengine.cameras.Camera;
import com.jengine.common.Event;
import com.jengine.common.IDisposable;
import com.jengine.common.IEvent;
import com.jengine.game.scene.EntityFactoryDatabase;
import com.jengine.game.scene.serialization.SceneData;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.jengine.viewports.Viewport;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Базовый класс для игровых сцен
 */
public abstract class Scene<T extends SceneData> implements IDisposable
{
	private final EntityFactoryDatabase _entityFactoryDb;

    protected final String sceneDataName;
    protected final Class<T> sceneDataType;
    protected T data;

    public final ArrayList<Entity> entities = new ArrayList<Entity>();

    //region Events

    public final IEvent<UpdateEventArgs> eventUpdate = new Event<UpdateEventArgs>();

    private void performUpdate(IGameTime gameTime)
    {
        ((Event)eventUpdate).doEvent(this, new UpdateEventArgs(gameTime));
    }

    //endregion

    private ResHandle<T> sceneData;

    public Camera camera;
	public Viewport viewport;

	protected Scene(String sceneDataName,
                    Class<T> sceneDataType,
                    HashMap<String, IEntityFactory> factoryMap,
					Camera camera,
					Viewport viewport)
	{
        this.sceneDataName = sceneDataName;
        this.sceneDataType = sceneDataType;
		_entityFactoryDb = new EntityFactoryDatabase(factoryMap);

		this.camera = camera;
		this.viewport = viewport;
		//TODO entities не должно быть public?
		entities.add(camera);
	}

	/**
	 * Вызывается для инициализации после загрузки ресурсов
	 */
	public void initialize()
    {
        data = sceneData.get();
    }

    /**
     * Вызывается для загрузки ресурсов
     */
    public void load(ResourceManager resourceManager)
    {
        sceneData = resourceManager.get(sceneDataName, sceneDataType);
    }

    /**
     * Вызывается для выгрузки ресурсов
     */
    public void unload(ResourceManager resourceManager)
    {
        resourceManager.free(sceneData);
    }

	public void resize(int width, int height)
	{
		viewport.setScreenSize(width, height);
	}
	
	public void update(IGameTime gameTime)
    {
        performUpdate(gameTime);
		for(int i = entities.size() - 1; i >= 0 ; i--)
			entities.get(i).update(gameTime);
    }

    public final void addInstance(Entity entity)
    {
        entities.add(entity);
    }

	public final <T extends EntityType> Entity<T> createInstance(T entityType)
	{
		IEntityFactory factory = _entityFactoryDb.get(entityType);
		if(factory != null)
		{
			Entity<T> entity = factory.create(this, entityType);
			if(entity != null)
			{
				Gdx.app.debug("Scene", String.format("createInstance[%s]: Succeed (object created)", entityType.getName()));
				entities.add(entity);
				return entity;
			}
			else Gdx.app.error("Scene", String.format("createInstance[%s]: Failed (factory return null)", entityType.getName()));
		}
		else Gdx.app.error("Scene", String.format("createInstance[%s]: Failed (factory not found)", entityType.getName()));
		return null;
	}

	public final <T extends Entity, TEntityType extends EntityType> T createInstance(TEntityType type, Class<T> entityClass)
	{
		IEntityFactory factory = _entityFactoryDb.get(type);
		if(factory != null)
		{
			T entity = (T)factory.create(this, type);
			if(entity != null)
			{
				Gdx.app.debug("Scene", String.format("createInstance[%s:%s]: Succeed (object created)", entityClass.getSimpleName(), type.getName()));
				entities.add(entity);
				return entity;
			}
			else Gdx.app.error("Scene", String.format("createInstance[%s:%s]: Failed (factory return null)", entityClass.getSimpleName(), type.getName()));
		}
		else Gdx.app.error("Scene", String.format("createInstance[%s:%s]: Failed (factory not found)", entityClass.getSimpleName(), type.getName()));
		return null;
	}

	public final Entity createInstance(String entityTypeName)
	{
		EntityType type = data.getType(entityTypeName);
		if(type != null)
		{
			return createInstance(type);
		}

		Gdx.app.error("Scene", String.format("createInstance[%s]: Failed (type not found)", entityTypeName));
		return null;
	}

	public final <T extends Entity> T createInstance(String entityTypeName, Class<T> entityClass)
	{
		EntityType type = data.getType(entityTypeName);
		if(type != null)
		{
			return (T)createInstance(type);
		}

		Gdx.app.error("Scene", String.format("createInstance[%s:%s]: Failed (type not found)", entityClass.getSimpleName(), entityTypeName));
		return null;
	}

	public void deleteInstance(Entity entity)
	{
		entities.remove(entity);
        entity.dispose();

		Gdx.app.debug("Scene", String.format("deleteInstance[%s]: Succeed (object deleted)", entity.getClass().getSimpleName()));
	}
	
	@Override
	public void dispose()
	{
		for(int i = entities.size() - 1; i >= 0; i--)
		{
			deleteInstance(entities.get(i));
		}
	}
}