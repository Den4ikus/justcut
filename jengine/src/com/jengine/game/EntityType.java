package com.jengine.game;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.jengine.common.Event;
import com.jengine.common.EventArgs;
import com.jengine.common.IEvent;

/**
 * Базовый класс для типов игровых сущностей
 */
public class EntityType implements Json.Serializable
{
	private int _instanceCounter = 0;

    /**
     * Возвращает количество инстансов сущностей данного типа
     */
	public int getInstanceCount()
	{
		return _instanceCounter;
	}

    //region Events

    /**
     * Событие возникающее при создании инстанса фигуры с данным типом
     */
    public final IEvent<EventArgs> eventCreateInstance = new Event<EventArgs>();

    /**
     * Событие возникающее при удалении инстанса фигуры с данным типом
     */
    public final IEvent<EventArgs> eventDeleteInstance = new Event<EventArgs>();

    /*internal*/ void onCreateInstance()
    {
        _instanceCounter++;
        ((Event)eventCreateInstance).doEvent(this, EventArgs.Empty);
    }

    /*internal*/ void onDeleteInstance()
    {
        _instanceCounter--;
        ((Event)eventDeleteInstance).doEvent(this, EventArgs.Empty);
    }

    //endregion

	protected String name;

    public String getName()
    {
        return name;
    }

    /**
     * Конструктор без параметров для десериализации
     */
    protected EntityType()
    {}

    public EntityType(String typeName)
    {
        this.name = typeName;
    }

    //region Json.Serializable

    public void write (Json json)
    {
        json.writeValue("name", name);
    }

    public void read (Json json, JsonValue jsonMap)
    {
        name = jsonMap.get("name").asString();
    }

    //endregion
}
