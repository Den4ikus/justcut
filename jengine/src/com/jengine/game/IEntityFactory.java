package com.jengine.game;

/**
 * Интерфейс фабрики сущностей
 * @param <TScene> Тип сцены для которой предназначена фабрика
 * @param <TEntityType> Тип сущности которые создает фабрика
 */
public interface IEntityFactory<TScene extends Scene, TEntityType extends EntityType>
{
    /**
     * Создает инстанс сущности заданного типа на указанной сцене
     * @param scene
     * @param type
     * @return
     */
	Entity<TEntityType> create(TScene scene, TEntityType type);
}
