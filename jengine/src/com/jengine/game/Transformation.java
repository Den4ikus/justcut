package com.jengine.game;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.jengine.math.MathHelper;
import com.jengine.math.Vector;

/**
 * Трансформация управляемая игровой логикой
 */
public class Transformation implements ITransformationProvider, Json.Serializable
{
	// объект для преобразований world-local local-world
	private static final Vector3 temp = new Vector3();
	
    protected final Vector3 position = new Vector3(Vector.Zero);
	protected final Vector3 angles = new Vector3(Vector.Zero);
	protected final Vector3 scale = new Vector3(Vector.One);
	protected final Vector3 forward = new Vector3(Vector.Forward);
	protected final Vector3 up = new Vector3(Vector.Up);
	protected final Vector3 right = new Vector3(Vector.Right);

	private final Quaternion _rotation = new Quaternion().idt();
    private final Matrix4 _cachedMatrix = new Matrix4().idt();
	private final Matrix4 _cachedInvMatrix = new Matrix4().idt();

	private boolean _needUpdateForward = true;
	private boolean _needUpdateUp = true;
	private boolean _needUpdateAngles = false;
	private boolean _needUpdateRotation = true;
    private boolean _needUpdateMatrix = true;
	private boolean _needUpdateInvMatrix = true;

    /**
     * Конструктор без параметров для десериализации
     */
    public Transformation()
    {}

    public Transformation(Vector3 position, Vector3 rotation)
    {
        this.position.set(position);
        this.angles.set(rotation);
    }

	private void updateAngles()
	{
		Vector3 temp = new Vector3(Vector.AxisX);
		angles.x = MathHelper.toDegrees((float)Math.asin(-temp.dot(forward))); // yaw
		temp.set(Vector.AxisY);
		angles.y = MathHelper.toDegrees((float)Math.asin(temp.dot(forward))); // pitch
		temp.set(Vector.AxisX);
		angles.z = MathHelper.toDegrees((float)Math.asin(temp.dot(up))); // roll
		_needUpdateAngles = false;
		_needUpdateRotation = true;
	}

    //region ITransformationProvider

	public void set(ITransformationProvider transformation)
	{
		this.position.set(transformation.getPosition());
		this.angles.set(transformation.getAngles());
		this.scale.set(transformation.getScale());
		this.forward.set(transformation.getForward());
		this.up.set(transformation.getUp());
		
		_needUpdateRotation = true;
		_needUpdateMatrix = true;
		_needUpdateInvMatrix = true;
	}
	
	@Override
    public Matrix4 get()
    {
        if(_needUpdateMatrix)
        {
			_cachedMatrix.set(position, getRotation(), scale);
            _needUpdateMatrix = false;
			_needUpdateInvMatrix = true;
        }
        return _cachedMatrix;
    }
	
	@Override
	public Matrix4 getInv()
	{
		if(_needUpdateInvMatrix)
		{
			_cachedInvMatrix.set(get());
			Matrix4.inv(_cachedInvMatrix.val);
			_needUpdateInvMatrix = false;
		}
		return _cachedInvMatrix;
	}


	@Override
	public Quaternion getRotation() 
	{ 
		if(_needUpdateRotation)
		{
			if(_needUpdateAngles) updateAngles();
			_rotation.setEulerAngles(angles.x, angles.y, angles.z);
			_needUpdateRotation = false;
		}
		return _rotation;
	}
	
    @Override
    public Vector3 getPosition() { return position; }
	@Override
	public float getPositionX() { return position.x; }
	@Override
	public float getPositionY() { return position.y; }
	@Override
	public float getPositionZ() { return position.z; }

    @Override
    public void setPosition(Vector3 pos)
    {
        position.set(pos);
        _needUpdateMatrix = true;
		_needUpdateInvMatrix = true;
    }

	@Override
	public void setPosition(float x, float y, float z)
	{
		position.x = x;
		position.y = y;
		position.z = z;
		_needUpdateMatrix = true;
		_needUpdateInvMatrix = true;
	}

	@Override
	public Vector3 getAngles()
	{
		if(_needUpdateAngles) updateAngles();
		return angles;
	}

	/**
	 * Получает угол поворота по оси Y (по часовой стрелке) вектор Up
	 */
	@Override
	public float getYaw()
	{
		if(_needUpdateAngles) updateAngles();
		return angles.x;
	}

	@Override
	public float getPitch()
	{
		if(_needUpdateAngles) updateAngles();
		return angles.y;
	}

	@Override
	public float getRoll()
	{
		if(_needUpdateAngles) updateAngles();
		return angles.z;
	}

	@Override
	public void setAngles(Vector3 angles)
	{
		this.angles.set(angles);
		_needUpdateAngles = false;
		_needUpdateForward = true;
		_needUpdateUp = true;
		_needUpdateRotation = true;
		_needUpdateMatrix = true;
		_needUpdateInvMatrix = true;
	}

	@Override
	public void setAngles(float yaw, float pitch, float roll)
	{
		angles.x = yaw;
		angles.y = pitch;
		angles.z = roll;
		_needUpdateAngles = false;
		_needUpdateForward = true;
		_needUpdateUp = true;
		_needUpdateRotation = true;
		_needUpdateMatrix = true;
		_needUpdateInvMatrix = true;
	}

	@Override
	public Vector3 getScale()
	{
		return scale;
	}

	@Override
	public float getScaleX()
	{
		return scale.x;
	}

	@Override
	public float getScaleY()
	{
		return scale.y;
	}

	@Override
	public float getScaleZ()
	{
		return scale.z;
	}
	
	@Override
	public void setScale(Vector3 scale)
	{
		this.scale.set(scale);
		_needUpdateMatrix = true;
		_needUpdateInvMatrix = true;
	}
	
	@Override
	public void setScale(float scaleX, float scaleY, float scaleZ)
	{
		scale.x = scaleX;
		scale.y = scaleY;
		scale.z = scaleZ;
		_needUpdateMatrix = true;
		_needUpdateInvMatrix = true;
	}

	@Override
	public Vector3 getForward()
	{
		if(_needUpdateForward)
		{
			forward.set(Vector.Forward);
			forward.mul(getRotation());
			_needUpdateForward = false;
		}
		return forward;
	}

	@Override
	public void setForward(Vector3 forward)
	{
		setForward(forward.x, forward.y, forward.z);
	}

	@Override
	public void setForward(float forwardX, float forwardY, float forwardZ)
	{
		forward.set(forwardX, forwardY, forwardZ);
		// пересчитываем вектор направленный вправо
		right.set(forward);
		right.crs(up);

		// пересчитываем вектор направленный вверх
		up.set(forward);
		up.crs(right);

		_needUpdateForward = false;
		_needUpdateRotation = true;
		_needUpdateAngles = true;
	}

	@Override
	public Vector3 getUp()
	{
		if(_needUpdateUp)
		{
			up.set(Vector.Up);
			up.mul(getRotation());
			_needUpdateUp = false;
		}
		return up;
	}

	@Override
	public void setUp(Vector3 up)
	{
		setUp(up.x, up.y, up.z);
	}

	@Override
	public void setUp(float upX, float upY, float upZ)
	{
		up.set(upX, upY, upZ);
		// пересчитываем вектор направленный вправо
		right.set(forward);
		right.crs(up);

		// пересчитываем вектор направленный вперед
		forward.set(up);
		forward.crs(right);

		_needUpdateUp = false;
		_needUpdateRotation = true;
		_needUpdateAngles = true;
	}

	/**
	 * Преобразует мировые координаты в локальные. ВНИМАНИЕ!!! Возвращается объект из пула
	 *
	 * @param v
	 */
	@Override
	public Vector2 toLocal(Vector2 v)
	{
		temp.set(v, 0f);
		temp.mul(getInv());
		return Vector.getVector2().set(temp.x, temp.y);
	}

	//TODO переделать остальные методы на использование пула, пиздец косяки прут
	@Override
    public Vector3 toLocal(Vector3 v) 
	{
		temp.set(v);
		temp.mul(getInv());
		return temp;
	}	
	
	@Override
	public Vector3 toLocal(float x, float y, float z) 
	{ 
		temp.set(x, y, z);
		temp.mul(getInv());
		return temp; 
	}
	
	@Override
    public Vector3 toWorld(Vector3 v) 
	{
		temp.set(v);
		temp.mul(get());
		return temp;
	}
	
	@Override
	public Vector3 toWorld(float x, float y, float z) 
	{
		temp.set(x, y, z);
		temp.mul(get());
		return temp;
	}

    //endregion

    //region Json.Serializable

    @Override
    public void write(Json json)
    {
        json.writeValue("position", position);
        json.writeValue("rotation", angles);
        json.writeValue("scale", scale);
    }

    @Override
    public void read(Json json, JsonValue jsonData)
    {
        position.set(json.readValue(Vector3.class, jsonData.get("position")));
        angles.set(json.readValue(Vector3.class, jsonData.get("rotation")));
        scale.set(json.readValue(Vector3.class, jsonData.get("scale")));
    }

    //endregion
}
