package com.jengine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;

import java.util.HashMap;

/**
 *
 */
public final class ColorHelper
{
    private static final Vector3 RGBToGrayscale = new Vector3(0.299f, 0.587f, 0.114f);
	
	public static Color invert(Color color)
	{
		color.set(1f - color.r,
				  1f - color.g,
				  1f - color.b,
				  color.a);
		return color;
	}
	
    private ColorHelper()
    {}

    public static Color byName(String colorName)
    {
        Color cr = new Color();
        HashMap <String, Color> hashColor = new HashMap<String, Color>();//для хранения цветов по ключам
        hashColor.put("clear", cr.CLEAR );
        hashColor.put("white", cr.WHITE );
        hashColor.put("black", cr.BLACK );
        hashColor.put("red", cr.RED );
        hashColor.put("green", cr.GREEN );
        hashColor.put("blue", cr.BLUE );
        hashColor.put("light_gray", cr.LIGHT_GRAY );
        hashColor.put("gray", cr.GRAY );
        hashColor.put("dark_gray", cr.DARK_GRAY );
        hashColor.put("pink", cr.PINK );
        hashColor.put("orange", cr.ORANGE );
        hashColor.put("yellow", cr.YELLOW );
        hashColor.put("magenta", cr.MAGENTA );
        hashColor.put("cyan", cr.CYAN );
        hashColor.put("olive", cr.OLIVE );
        hashColor.put("purple", cr.PURPLE );
        hashColor.put("maroon", cr.MAROON );
        hashColor.put("teal", cr.TEAL );
        hashColor.put("navy", cr.NAVY );

        return hashColor.get(colorName);//возврещаем цвет из класса Color по введенному имени цвета(ключу)
    }

    public static float brightness(Color color)
    {
        return color.r * RGBToGrayscale.x + color.g * RGBToGrayscale.y + color.b * RGBToGrayscale.z;
    }
}
