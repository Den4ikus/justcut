package com.jengine;

import com.jengine.common.logs.LogLevel;

/**
 * Created by Денис on 11.07.2015.
 * Конфигурация движка игры
 */
public interface IJEngineCfg
{
    static final IJEngineCfg Default = new IJEngineCfg()
    {
        @Override
        public LogLevel getLogLevel()
        {
            return LogLevel.NONE;
        }

        @Override
        public String getLogFilename()
        {
            return null;
        }

        @Override
        public String getResourceLocalizationDirectory()
        {
            return null;
        }
    };

    /**
     * Возвращает уровень логгирования для движка
     */
    LogLevel getLogLevel();

    /**
     * Возвращает имя файла лога
     */
    String getLogFilename();

    /**
     * Возвращает директорию с файлами локализации
     */
    String getResourceLocalizationDirectory();
}
