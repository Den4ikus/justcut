package com.jengine.input;

import com.badlogic.gdx.math.Vector2;
import com.jengine.common.EventArgs;

public final class InputScaleEventArgs extends EventArgs
{
	public final Vector2 first = new Vector2();
	public final Vector2 second = new Vector2();
	
	public InputScaleEventArgs()
	{}
}