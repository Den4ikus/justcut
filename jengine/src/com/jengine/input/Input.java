package com.jengine.input;

import android.view.KeyEvent;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.jengine.common.Event;
import com.jengine.common.EventArgs;
import com.jengine.common.IEvent;

import java.util.ArrayList;

/**
 * Предоставляет точку доступа к событиям ввода
 */
public final class Input implements InputProcessor
{
	private final class TouchInfo
	{
		public final Vector2 lastTouchUp = new Vector2();
		public final Vector2 lastTouchDown = new Vector2();
		public final Vector2 pos = new Vector2();
		public boolean touched = false;
	}
	
	// InputProcessor используется для стандарного UI libgdx => обрабатывается в первую очередь
	// IInputHandler предоставляет более удобный и расширенный интерфейс обработки ввода для контроллеров
	private static final ArrayList<InputProcessor> _processors = new ArrayList<InputProcessor>();
	private static final ArrayList<IInputHandler> _handlers = new ArrayList<IInputHandler>();
	private static final Input _instance;
	
	static
	{
		_instance = new Input();
		Gdx.input.setInputProcessor(_instance);
	}
	
	//region Events

	public static final IEvent<InputKeyEventArgs> eventKeyUp = new Event<InputKeyEventArgs>();
	public static final IEvent<EventArgs> eventKeyUpBack = new Event<EventArgs>();
	public static final IEvent<EventArgs> eventKeyUpMenu = new Event<EventArgs>();
	public static final IEvent<InputTouchEventArgs> eventTouchDown = new Event<InputTouchEventArgs>();
	public static final IEvent<InputTouchEventArgs> eventTouchUp = new Event<InputTouchEventArgs>();
	public static final IEvent<InputTouchEventArgs> eventTouch = new Event<InputTouchEventArgs>();
	public static final IEvent<InputTouchMoveEventArgs> eventMove = new Event<InputTouchMoveEventArgs>();
	public static final IEvent<InputScaleEventArgs> eventScale = new Event<InputScaleEventArgs>();

	private void doEventKeyUp(InputKeyEventArgs args)
	{
		((Event)eventKeyUp).doEvent(this, args);
	}

	private void doEventKeyUpBack()
	{
		((Event) eventKeyUpBack).doEvent(this, EventArgs.Empty);
	}

	private void doEventKeyUpMenu()
	{
		((Event) eventKeyUpMenu).doEvent(this, EventArgs.Empty);
	}

	private void doEventTouchDown(InputTouchEventArgs args)
	{
		((Event)eventTouchDown).doEvent(this, args);
	}
	
	private void doEventTouchUp(InputTouchEventArgs args)
	{
		((Event)eventTouchUp).doEvent(this, args);
	}
	
	private void doEventTouch(InputTouchEventArgs args)
	{
		((Event)eventTouch).doEvent(this, args);
	}
		
	private void doEventMove(InputTouchMoveEventArgs args)
	{
		((Event)eventMove).doEvent(this, args);
	}
		
	private void doEventScale(InputScaleEventArgs args)
	{
		((Event)eventScale).doEvent(this, args);
	}
	
	//endregion
	
	private final TouchInfo[] touchInfo = new TouchInfo[]
	{
		new TouchInfo(),
		new TouchInfo(),
		new TouchInfo(),
		new TouchInfo(),
		new TouchInfo(),
		new TouchInfo()
	};

	private final InputTouchEventArgs _touchEventArgs = new InputTouchEventArgs();
	private final InputTouchMoveEventArgs _moveEventArgs = new InputTouchMoveEventArgs();
	private final InputScaleEventArgs _scaleEventArgs = new InputScaleEventArgs();
	
	private Input()
	{
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);
	}
	
	//TODO переименовать на addHandler и addProcessor соответствтенно
	public static void addProcessor(InputProcessor processor)
	{
		_processors.add(processor);
	}
	
	public static void addProcessor(InputProcessor processor, boolean isTopLevel)
	{
		if(isTopLevel) _processors.add(0, processor);
		else addProcessor(processor);
	}
	
	public static void addHandler(IInputHandler handler)
	{
		_handlers.add(handler);
	}
	
	public static void addHandler(IInputHandler handler, boolean isTopLevel)
	{
		if(isTopLevel) _handlers.add(0, handler);
		else addHandler(handler);
	}
	
	public static void removeProcessor(InputProcessor processor)
	{
		_processors.remove(processor);
	}
	
	public static void removeHandler(IInputHandler handler)
	{
		_handlers.remove(handler);
	}
	
	//region InputProcessor
	
    @Override
    public boolean touchDown(int x, int y, int pointer, int button) 
	{
		TouchInfo info = touchInfo[pointer];
		info.lastTouchDown.set(x, y);
		info.touched = true;
		
		_touchEventArgs.position.set(x, y);
		_touchEventArgs.pointer = pointer;
		_touchEventArgs.button = button;
		doEventTouchDown(_touchEventArgs);
		
		for(int i = 0; i < _processors.size(); i++)
		{
			if(_processors.get(i).touchDown(x, y, pointer, button))
				return true;
		}
		for(int i = 0; i < _handlers.size(); i++)
		{
			if(_handlers.get(i).touchDown(_touchEventArgs))
				return true;
		}

		return true;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) 
	{
		TouchInfo info = touchInfo[pointer];
		info.lastTouchUp.set(x, y);
		info.touched = false;
		
		_touchEventArgs.position.set(x, y);
		_touchEventArgs.pointer = pointer;
		_touchEventArgs.button = button;
		doEventTouchUp(_touchEventArgs);
		
		for(int i = 0; i < _processors.size(); i++)
		{
			if(_processors.get(i).touchUp(x, y, pointer, button))
				return true;
		}

		for(int i = 0; i < _handlers.size(); i++)
		{
			if(_handlers.get(i).touchUp(_touchEventArgs))
				return true;
		}

		return true;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) 
	{
		TouchInfo info = touchInfo[pointer];
		info.pos.set(x, y);
		
		//TODO оптимизировать порядок выполнения
		// жест "перемещение"
		if(touchInfo[0].touched && !touchInfo[1].touched)
		{
			_moveEventArgs.start.set(info.lastTouchDown);
			_moveEventArgs.current.set(info.pos);
			doEventMove(_moveEventArgs);
		}
		// жест "масштабирование"
		else if(touchInfo[0].touched && touchInfo[1].touched)
		{
			_scaleEventArgs.first.set(touchInfo[0].pos);
			_scaleEventArgs.second.set(touchInfo[1].pos);
			doEventScale(_scaleEventArgs);
		}
		
		for(int i = 0; i < _processors.size(); i++)
		{
			if(_processors.get(i).touchDragged(x, y, pointer))
				return true;
		}
		
		if(touchInfo[0].touched && !touchInfo[1].touched)
		{
			for(int i = 0; i < _handlers.size(); i++)
			{
				if(_handlers.get(i).touchMove(_moveEventArgs))
					return true;
			}
		}

		return true;
    }

	/**
	 * Called when the mouse was moved without any buttons being pressed. Will not be called on iOS.
	 *
	 * @param screenX
	 * @param screenY
	 * @return whether the input was processed
	 */
	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		for(int i = 0; i < _processors.size(); i++)
		{
			if(_processors.get(i).mouseMoved(screenX, screenY))
				break;
		}
		return true;
	}

    @Override
    public boolean keyDown(int keycode) 
	{ 
		for(int i = 0; i < _processors.size(); i++)
		{
			if(_processors.get(i).keyDown(keycode))
				break;
		}
		return true;
	}
	
    @Override
    public boolean keyUp(int keycode) 
	{
		doEventKeyUp(new InputKeyEventArgs(keycode));

		if(keycode == KeyEvent.KEYCODE_BACK) doEventKeyUpBack();
		else if(keycode == KeyEvent.KEYCODE_MENU) doEventKeyUpMenu();

		for(int i = 0; i < _processors.size(); i++)
		{
			if(_processors.get(i).keyUp(keycode))
				break;
		}
		return true;
	}
	
    @Override
    public boolean keyTyped(char character) 
	{ 
		for(int i = 0; i < _processors.size(); i++)
		{
			if(_processors.get(i).keyTyped(character))
				break;
		}
		return true;
	}
	
	@Override
    public boolean scrolled(int amount) 
	{ 
		for(int i = 0; i < _processors.size(); i++)
		{
			if(_processors.get(i).scrolled(amount))
				break;
		}
		for(int i = 0; i < _handlers.size(); i++)
		{
			if(_handlers.get(i).scroll(amount))
				break;
		}
		return true;
	}
	
	//endregion
}
