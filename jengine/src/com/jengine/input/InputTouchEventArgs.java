package com.jengine.input;

import com.badlogic.gdx.math.Vector2;
import com.jengine.common.EventArgs;

public final class InputTouchEventArgs extends EventArgs
{
	//TODO переделать button и pointer на enum?
	public final Vector2 position = new Vector2();
	public int pointer;
	public int button;
	
	public InputTouchEventArgs()
	{}
}