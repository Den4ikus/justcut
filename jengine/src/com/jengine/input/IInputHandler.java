package com.jengine.input;

public interface IInputHandler
{
	boolean touchDown(InputTouchEventArgs args);
	boolean touchUp(InputTouchEventArgs args);
	boolean touchMove(InputTouchMoveEventArgs args);
	boolean scroll(int scrollValue);
}