package com.jengine.input;

import com.badlogic.gdx.math.Vector2;
import com.jengine.common.EventArgs;

public final class InputTouchMoveEventArgs extends EventArgs
{
	public final Vector2 start = new Vector2();
	public final Vector2 current = new Vector2();
	
	public InputTouchMoveEventArgs()
	{}
}