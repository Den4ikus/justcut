package com.jengine.input;

import com.jengine.common.EventArgs;

/**
 *
 */
public final class InputKeyEventArgs extends EventArgs
{
    public final int keyCode;

    public InputKeyEventArgs(int keyCode)
    {
        this.keyCode = keyCode;
    }
}
