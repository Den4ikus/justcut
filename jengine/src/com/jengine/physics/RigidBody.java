package com.jengine.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.jengine.common.Event;
import com.jengine.common.EventArgs;
import com.jengine.common.IEvent;
import com.jengine.common.VectorHelper;
import com.jengine.game.ITransformationProvider;
import com.jengine.game.Transformation;
import com.jengine.math.MathHelper;

/**
 * Представляет обертку над физическим объектом Box2d.
 * Предоставляет трансформацию для объектов.
 */
public final class RigidBody extends Transformation
{
    private Body _box2dBody;
	private boolean _needUpdatePosition = false;
	private boolean _needUpdateRotation = false;

    //region Events

	public final IEvent<EventArgs> eventBeginCollide = new Event<EventArgs>();
    public final IEvent<EventArgs> eventEndCollide = new Event<EventArgs>();
    public final IEvent<EventArgs> eventPreCollide = new Event<EventArgs>();
    public final IEvent<EventArgs> eventPostCollide = new Event<EventArgs>();

    void doBeginCollide(RigidBody entity)
    {
        ((Event)this.eventBeginCollide).doEvent(this, EventArgs.Empty);
    }

    void doEndCollide(RigidBody entity)
    {
        ((Event)this.eventEndCollide).doEvent(this, EventArgs.Empty);
    }

    void doPreCollide(RigidBody entity)
    {
        ((Event)this.eventPreCollide).doEvent(this, EventArgs.Empty);
    }

    void doPostCollide(RigidBody entity)
    {
        ((Event)this.eventPostCollide).doEvent(this, EventArgs.Empty);
    }

    //endregion
	
    Body getBox2dBody() { return _box2dBody; }

    public float getMass()
    {
        return _box2dBody.getMass();
    }

    public RigidBody(Body box2dBody)
    {
        _box2dBody = box2dBody;
    }
	
	//todo: реализовать всякие методы физического воздействия

    public void update()
    {
        // обновляем позицию и вращение у тела если они менялись
        if(_needUpdatePosition || _needUpdateRotation)
        {
            Vector2 position = _needUpdatePosition ? VectorHelper.ToVector2(this.position) : _box2dBody.getPosition();
            float angle = _needUpdateRotation ? MathHelper.toRadians(angles.z) : _box2dBody.getAngle();
            _box2dBody.setTransform(position, angle);

            _needUpdatePosition = false;
            _needUpdateRotation = false;
        }
        else if(_box2dBody.isAwake())
        {
            // обновляем позицию и вращение если тело активно
            Vector2 pos = _box2dBody.getPosition();
            float angle = _box2dBody.getAngle();
            super.setPosition(pos.x, pos.y, 0f);
            super.setAngles(0f, 0f, MathHelper.toDegrees(angle));
        }
    }

    //region ITransformation


    @Override
    public void set(ITransformationProvider transformation)
    {
        super.set(transformation);
        _needUpdatePosition = true;
        _needUpdateRotation = true;
    }

    @Override
	public void setPosition(float x, float y, float z)
	{
		super.setPosition(x, y, z);
		_needUpdatePosition = true;
	}
	
	@Override
    public void setPosition(Vector3 pos)
	{
		super.setPosition(pos);
		_needUpdatePosition = true;
	}
	
	@Override
	public void setAngles(Vector3 angles)
	{
		super.setAngles(angles);
        _needUpdateRotation = true;
	}
	
	@Override
	public void setAngles(float angleX, float angleY, float angleZ)
	{
		super.setAngles(angleX, angleY, angleZ);
        _needUpdateRotation = true;
	}

    //endregion
}
