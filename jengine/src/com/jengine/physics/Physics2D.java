package com.jengine.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.jengine.common.IDisposable;
import com.jengine.math.MathHelper;

import java.util.ArrayList;

/**
 * Created by Денис on 17.03.2015.
 * Класс для работы с двумерной физикой сцены
 */
public final class Physics2D implements IDisposable
{
    public class Box2dContactListener implements ContactListener
    {
        @Override
        public void beginContact(Contact contact)
        {
            RigidBody entityA = getEntityA(contact);
            RigidBody entityB = getEntityB(contact);
            entityA.doBeginCollide(entityB);
            entityB.doBeginCollide(entityA);
        }

        @Override
        public void endContact(Contact contact)
        {
            RigidBody entityA = getEntityA(contact);
            RigidBody entityB = getEntityB(contact);
            entityA.doEndCollide(entityB);
            entityB.doEndCollide(entityA);
        }

        @Override
        public void preSolve(Contact contact, Manifold oldManifold)
        {
            RigidBody entityA = getEntityA(contact);
            RigidBody entityB = getEntityB(contact);
            entityA.doPreCollide(entityB);
            entityB.doPreCollide(entityA);
        }

        @Override
        public void postSolve(Contact contact, ContactImpulse impulse)
        {
            RigidBody entityA = getEntityA(contact);
            RigidBody entityB = getEntityB(contact);
            entityA.doPostCollide(entityB);
            entityB.doPostCollide(entityA);
        }

        private RigidBody getEntityA(Contact contact)
        {
            return (RigidBody)contact.getFixtureA().getBody().getUserData();
        }

        private RigidBody getEntityB(Contact contact)
        {
            return (RigidBody)contact.getFixtureB().getBody().getUserData();
        }
    }

    private final float TIME_STEP = 1f/30f;
    private final int VELOCITY_ITERATIONS = 8;
    private final int POSITION_ITERATIONS = 3;

	    
    private ArrayList<RigidBody> bodies = new ArrayList<RigidBody>();
    private World _box2dWorld;
	private float elapsedTime = 0f;

    public World getBox2dWorld()
    {
        return _box2dWorld;
    }

    public Physics2D(Vector2 gravity)
    {
        _box2dWorld = new World(gravity, true);
        _box2dWorld.setContactListener(new Box2dContactListener());
    }
	
	public void update(float elapsedTime)
    {
        this.elapsedTime += elapsedTime;
        while(this.elapsedTime >= TIME_STEP)
        {
            this.elapsedTime -= TIME_STEP;
            _box2dWorld.step(TIME_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
        }

        for (int i = 0; i < bodies.size(); i++)
        {
            RigidBody body = bodies.get(i);
            body.update();
        }
    }

    public RigidBody createEdge(BodyDef.BodyType bodyType, Vector2 position, Vector2 v1, Vector2 v2, float density, float friction, float restitution)
    {
        EdgeShape edgeShape = new EdgeShape();
        edgeShape.set(v1, v2);
        Body box2dBody = createBox2dBody(edgeShape, bodyType, position, density, friction, restitution);
        edgeShape.dispose();

        return createRigidBody(box2dBody);
    }

    public RigidBody createChain(BodyDef.BodyType bodyType, Vector2 position, Vector2[] vertices, float density, float friction, float restitution)
    {
        ChainShape chainShape = new ChainShape();
        chainShape.createChain(vertices);
        Body box2dBody = createBox2dBody(chainShape, bodyType, position, density, friction, restitution);
        chainShape.dispose();

        return createRigidBody(box2dBody);
    }

    public RigidBody createBox(BodyDef.BodyType bodyType, Vector2 position, Vector2 size, float density, float friction, float restitution)
    {
        PolygonShape boxShape = new PolygonShape();
        boxShape.setAsBox(size.x, size.y);
        Body box2dBody = createBox2dBody(boxShape, bodyType, position, density, friction, restitution);
        boxShape.dispose();

        return createRigidBody(box2dBody);
    }

    public RigidBody createCircle(BodyDef.BodyType bodyType, Vector2 position, float radius, float density, float friction, float restitution)
    {
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(radius);
        Body box2dBody = createBox2dBody(circleShape, bodyType, position, density, friction, restitution);
        circleShape.dispose();

        return createRigidBody(box2dBody);
    }

	public RigidBody createConvexPoly(BodyDef.BodyType bodyType, Vector2 position, Vector2[] poly, float density, float friction, float restitution)
    {
        // Валидация на минимальный размер граней
        // т.к. если Box2D обнаружит что после оптимизации полигона получилось меньше 3 вершин, сразу же грохнет приложение
        if(!validatePoly(poly)) return null;

        // Если вершин 8 или меньше Box2d нормально прожует полигон, если больше нужно разбивать полигон на отдельные ограничивающие фигуры
        // Box2d автоматически стоит выпуклый полигон по набору точек поэтому порядок не имеет значения
        final int firstForwardIndex = 0;
        final int firstBackwardIndex = poly.length - 1;
        int forwardIndex = firstForwardIndex;
        int backwardIndex = firstBackwardIndex;

        ArrayList<PolygonShape> polyShapes = new ArrayList<PolygonShape>();
        Vector2[] vertices = new Vector2[8];
        int vertexIndex = 0;
        while(forwardIndex <= backwardIndex)
        {
            // Определяем в какую сторону двигаться по контуру
            if(forwardIndex - firstForwardIndex <= firstBackwardIndex - backwardIndex)
            {
                // Двигаемся вперед
                vertices[vertexIndex++] = poly[forwardIndex++];
            }
            else
            {
                // Двигаемся назад
                vertices[vertexIndex++] = poly[backwardIndex--];
            }

            // Если группа из 8 вершин определена, строим ограничивающую фигуру полигона
            if(vertexIndex > 7)
            {
                PolygonShape shape = new PolygonShape();
                shape.set(vertices);
                polyShapes.add(shape);

                // Устанавливаем две крайние точки как начальные для новой части полигона
                vertices[0] = vertices[6];
                vertices[1] = vertices[7];
                vertexIndex = 2;
            }
        }

        // Если полигон не делится на восьмиугольники последняя часть полигона получается с количеством точек меньше 8
        // разбиение полигона идет стык в стык и первая пара точек части полигона совпадает с краними точками предыдущей части полигона
        if(vertexIndex > 2)
        {
            // Копируем точки т.к. строить ограничивающую фигуру можно только по массиву целиком
            Vector2[] temp = new Vector2[vertexIndex];
            System.arraycopy(vertices, 0, temp, 0, temp.length);

            PolygonShape shape = new PolygonShape();
            shape.set(temp);
            polyShapes.add(shape);
        }

        Body box2dBody = polyShapes.size() == 1
                ? createBox2dBody(polyShapes.get(0), bodyType, position, density, friction, restitution)
                : createBox2dBody(polyShapes, bodyType, position, density, friction, restitution);

        for(int i = 0; i < polyShapes.size(); i++)
            polyShapes.get(i).dispose();

        return createRigidBody(box2dBody);
    }
	
    public void deleteBody(RigidBody body)
    {
        bodies.remove(body);
        _box2dWorld.destroyBody(body.getBox2dBody());
    }

    /**
     * Проверяет подходит ли полигон для Box2D
     * @param poly вершины описывающие полигон по часовой стрелке
     * @return
     */
    private boolean validatePoly(Vector2[] poly)
    {
        // Box2D small length used as a collision and constraint tolerance.
        final float b2_linearSlop = 0.005f;

        // Проверяем что Box2D понравится полигон
        // Сохраняем вершины расстояние между которыми подходит для Box2D
        Vector2[] tempVertices = new Vector2[poly.length];
        int tempIndex = 0;

        for(int i = 0; i < poly.length; i++)
        {
            Vector2 v = poly[i];

            boolean isValidVertex = true;
            for(int j = 0; j < tempIndex; j++)
            {
                if(MathHelper.len2(v, tempVertices[j]) < b2_linearSlop)
                {
                    isValidVertex = false;
                    break;
                }
            }

            if(isValidVertex)
            {
                tempVertices[tempIndex++] = v;
            }
        }

        return tempIndex >= 3;
    }

    private Body createBox2dBody(Shape bodyShape, BodyDef.BodyType bodyType, Vector2 position, float density, float friction, float restitution)
    {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = bodyType;
        bodyDef.position.set(position);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = bodyShape;
        fixtureDef.density = density;
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;

        Body body = _box2dWorld.createBody(bodyDef);
        body.createFixture(fixtureDef);
        return body;
    }
	
	private Body createBox2dBody(ArrayList<PolygonShape> bodyShapes, BodyDef.BodyType bodyType, Vector2 position, float density, float friction, float restitution)
    {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = bodyType;
        bodyDef.position.set(position);

		Body body = _box2dWorld.createBody(bodyDef);
		
		for(int i = 0; i < bodyShapes.size(); i++)
		{
			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = bodyShapes.get(i);
			fixtureDef.density = density;
			fixtureDef.friction = friction;
			fixtureDef.restitution = restitution;
			body.createFixture(fixtureDef);
		}
		
        return body;
    }

    private RigidBody createRigidBody(Body body)
    {
        RigidBody rb = new RigidBody(body);
        body.setUserData(rb);
        bodies.add(rb);
        return rb;
    }

    //region Disposable

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_box2dWorld != null)
            _box2dWorld.dispose();
    }

    //endregion
}
