package com.jengine.cameras;

/**
 * Камера использующая перспективную проекцию
 */
public final class PerspectiveCamera extends Camera
{
    public PerspectiveCamera(float viewportWidth, float viewportHeight)
    {
        super(viewportWidth, viewportHeight, 0f, 1f);
    }

    public PerspectiveCamera(float viewportWidth, float viewportHeight, float near, float far)
    {
        super(viewportWidth, viewportHeight, near, far);
    }

    @Override
    protected void updateProj()
    {
        float aspectRatio = viewportWidth / viewportHeight;
        proj.setToProjection(near, far, 90f, aspectRatio);
        needUpdateProj = false;
    }
}
