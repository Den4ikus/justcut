package com.jengine.cameras;

/**
 * Камера использующая ортографическую проекцию
 */
public final class OrthographicCamera extends Camera
{
	public OrthographicCamera(float viewportWidth, float viewportHeight)
	{
		this(viewportWidth, viewportHeight, 0f, 1f);
	}
	
    public OrthographicCamera(float viewportWidth, float viewportHeight, float near, float far)
    {
		super(viewportWidth, viewportHeight, near, far);
    }
	
	@Override
    protected void updateProj()
	{
		float halfWidth = viewportWidth * 0.5f;
		float halfHeight = viewportHeight * 0.5f;
		proj.setToOrtho(-halfWidth, halfWidth, -halfHeight, halfHeight, near, far);
		needUpdateProj = false;
	}
}
