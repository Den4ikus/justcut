package com.jengine.cameras;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.jengine.game.Entity;
import com.jengine.game.IGameTime;
import com.jengine.game.Transformation;
import com.jengine.viewports.Viewport;

/**
 * Базовый класс для камер
 */
public abstract class Camera extends Entity
{
    //todo добавить frustum
    //protected Frustum frustum;
	private static final Vector3 tempVec3 = new Vector3();
	private static final Vector2 tempVec2 = new Vector2();
		
    public final Transformation transformation = new Transformation();
	public final Matrix4 view = new Matrix4().idt();
    public final Matrix4 proj = new Matrix4().idt();
    public final Matrix4 viewProj = new Matrix4().idt();
	public final Matrix4 invViewProj = new Matrix4().idt();
	
	protected float viewportWidth;
	protected float viewportHeight;
    protected float near;
	protected float far;
	protected boolean needUpdateProj = true;
	
	public float getViewportWidth()
	{
		return viewportWidth;
	}
	
	public void setViewportWidth(float width)
	{
		viewportWidth = width;
		needUpdateProj = true;
	}
	
	public float getViewportHeight()
	{
		return viewportHeight;
	}
	
	public void setViewportHeight(float height)
	{
		viewportHeight = height;
		needUpdateProj = true;
	}
	
	public void setViewport(float width, float height)
	{
		viewportWidth = width;
		viewportHeight = height;
		needUpdateProj = true;
	}
	
	public float getNear()
	{
		return near;
	}
	
	public void setNear(float near)
	{
		this.near = near;
		needUpdateProj = true;
	}
	
	public float getFar()
	{
		return far;
	}
	
	public void setFar(float far)
	{
		this.far = far;
		needUpdateProj = true;
	}
	
	public Camera(float viewportWidth, float viewportHeight, float near, float far)
	{
		this.viewportWidth = viewportWidth;
		this.viewportHeight = viewportHeight;
		this.near = near;
		this.far = far;
	}

	/**
	 * Проецирует точку из нормализованных экранных координат в мировое пространство
	 * @param screenPoint Точка в нормализованных экранных координатах
	 * @param z Глубина точки 0 - near, 1 - far
	 * @return Точка в мировых координатах
	 */
	public Vector3 toWorld(Vector2 screenPoint, float z)
	{
		tempVec3.set(screenPoint, z);
		tempVec3.prj(invViewProj);
		return tempVec3;
	}

	/**
	 * Проецирует точку из оконных координат в мировое пространство
	 * @param screenPoint Точка в оконных координатах
	 * @param z Глубина точки 0 - near, 1 - far
	 * @param viewport Viewport используемый для пересчета оконных координат в нормализованные экранные
	 * @return Точка в мировых координатах
	 */
	//todo для смещенного вьюпорта будет работать неверно
	public Vector3 toWorld(Vector2 screenPoint, float z, Viewport viewport)
	{
		// переводим оконные координаты в нормализованные
		float normX = (screenPoint.x / viewport.getScreenWidth()) * 2f - 1f;
		float normY = -(screenPoint.y / viewport.getScreenHeight()) * 2f + 1f;
		tempVec2.set(normX, normY);
		return toWorld(tempVec2, z);
	}

	/**
	 * Проецирует точку из мировых координат в нормализованные экранные координаты
	 * @param worldPoint Точка в мировых координатах
	 * @return Точка в нормализованных экранных координатах
	 */
	public Vector2 toScreen(Vector3 worldPoint)
    {
		tempVec3.set(worldPoint);
		tempVec3.mul(viewProj);
		tempVec2.set(tempVec3.x, tempVec3.y);
        return tempVec2;
    }

	/**
	 * Проецирует точку из мировых координат в оконные координаты
	 * @param worldPoint Точка в мировых координатах
	 * @param viewport Viewport используемый для пересчета нормализованных экранных координат в оконные
	 * @return Точка в оконных координатах
	 */
	//todo для смещенного вьюпорта будет работать неверно
	public Vector2 toScreen(Vector3 worldPoint, Viewport viewport)
	{
		Vector2 norm = toScreen(worldPoint);
		// переводим нормализованные координаты в оконные
		float wndX = viewport.getScreenWidth() * ((norm.x + 1f) * 0.5f);
		float wndY = viewport.getScreenHeight() * ((1f - norm.y) * 0.5f);
		tempVec2.set(wndX, wndY);
		return tempVec2;
	}

	@Override
	public void update(IGameTime gameTime)
	{
		// обновляем матрицу вида
		Vector3 target = new Vector3(transformation.getPosition()).add(transformation.getForward());
		view.setToLookAt(transformation.getPosition(), target, transformation.getUp());
		// матрица проекции меняется не так часто, поэтому имеет смысл оптимизация
		if(needUpdateProj)
		{
			updateProj();
			needUpdateProj = false;
		}
		
		// пересчитываем комбинированную и обратную матрицу
		viewProj.set(proj);
		Matrix4.mul(viewProj.val, view.val);
		invViewProj.set(viewProj);
		// invViewProj.inv() кидает эксепшен что типа матрицу невозможно инвертировать,
		// причем только для перспективной проекции, похоже на баг libgdx
		Matrix4.inv(invViewProj.val);
	}
	
    protected abstract void updateProj();
}
