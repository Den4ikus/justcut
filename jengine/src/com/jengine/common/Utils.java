package com.jengine.common;

import java.lang.reflect.Array;

/**
 *
 */
public final class Utils
{
    /**
     * Преобразует массив object-ов в массив объектов заданного типа
     * Используется в Linq.toArray т.к. метод toArray для коллекций возвращает массив объектов
     * @param array
     * @param castType
     * @param <T>
     * @return
     */
    public static <T> T[] castArray(Object[] array, Class<T> castType)
    {
        T[] result = (T[]) Array.newInstance(castType, array.length);
        for(int i = 0; i < array.length; i++)
        {
            result[i] = (T)array[i];
        }
        return result;
    }
}
