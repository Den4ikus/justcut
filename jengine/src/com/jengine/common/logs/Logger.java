package com.jengine.common.logs;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Денис on 07.06.2015.
 */

/**
 * Логгер Console/Logcat
 */
public final class Logger
{
    private static final SimpleDateFormat _dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    private static LogLevel logLevelFilter = LogLevel.DEBUG;
    private static FileHandle logFile = null;

    /**
     * Инициализирует логгер
     * @param logLevel минимальный уровень логгирования, сообщения с уровнем логгирования меньше указанного будут отброшены
     */
    public static final void initialize(String logFilename, LogLevel logLevel)
    {
        Gdx.app.setLogLevel(Application.LOG_NONE);
        logLevelFilter = logLevel;
        //logFile = Gdx.files.local(logFilename);
        info("LOGGER INITIALIZED");
    }

    /**
     * Логгирование в целях отладки
     */
    public static final void debug(String message)
    {
        log(message, LogLevel.DEBUG);
    }

    /**
     * Логгирование информации, параметры объектов, статистические данные и т.п.
     */
    public static final void info(String message)
    {
        log(message, LogLevel.INFO);
    }

    public static final void info(String className, String methodName, String parameters, String result, String description)
    {
        log(className, methodName, parameters, result, description, LogLevel.INFO);
    }

    /**
     * Логгирование ошибок не нарушающих нормальную работу программы
     */
    public static final void warning(String message)
    {
        log(message, LogLevel.WARNING);
    }

    public static final void warning(String className, String methodName, String parameters, String result, String description)
    {
        log(className, methodName, parameters, result, description, LogLevel.WARNING);
    }

    /**
     * Логгирование ошибок нарушающих нормальную работу программы
     */
    public static final void error(String message)
    {
        log(message, LogLevel.ERROR);
    }

    /**
     * Логгирование критических ошибок, при которых дальнейшая работа программы невозможна
     */
    public static final void critical(String message)
    {
        log(message, LogLevel.CRITICAL);
    }

    /**
     * Запись сообщения в лог
     * @param message сообщение
     * @param logLevel уровень логгирования
     */
    public static void log(String message, LogLevel logLevel)
    {
        //if(logFile == null) throw new IllegalStateException("Перед использованием нужно вызвать метод Initialize");
        if(logLevelFilter.compareTo(logLevel) > 0) return;

        try
        {
            String formattedSender = String.format("[%s", logLevel);
            String formattedMessage = String.format("%s]: %s\n", _dateFormat.format(new Date()), message);
            //logFile.writeString(formattedMessage, true);
            Gdx.app.log(formattedSender, formattedMessage);
        }
        catch(GdxRuntimeException ex)
        {
            // Если вылетело исключение значит логи больше вести не получится, но пользователю об этом знать не обязательно
            // т.к. могло закончиться место на диске или файл был открыт другой программой и т.п.
        }
    }

    public static void log(String className, String methodName, String parameters, String result, String description, LogLevel logLevel)
    {
        if(logLevelFilter.compareTo(logLevel) > 0) return;

        try
        {
            String formattedSender = String.format("[%s", logLevel);
            String formattedMessage = String.format("%s] %s:%s[%s]: %s (%s)", _dateFormat.format(new Date()),
                    className, methodName, parameters, result, description);
            //logFile.writeString(formattedMessage, true);
            Gdx.app.log(formattedSender, formattedMessage);
        }
        catch(GdxRuntimeException ex)
        {
            // Если вылетело исключение значит логи больше вести не получится, но пользователю об этом знать не обязательно
            // т.к. могло закончиться место на диске или файл был открыт другой программой и т.п.
        }
    }
}
