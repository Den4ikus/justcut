package com.jengine.common.logs;

/**
 * Created by Денис on 07.06.2015.
 */

/**
 * Уровень логгирования
 */
public enum LogLevel
{
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    CRITICAL,
    NONE
}
