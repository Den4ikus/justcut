package com.jengine.common;

/**
 * Created by Денис on 25.03.2015.
 * Базовый класс для объектов содержащих информацию о событии
 */
public class EventArgs
{
    public static final EventArgs Empty = new EventArgs();

    protected EventArgs()
    {}
}
