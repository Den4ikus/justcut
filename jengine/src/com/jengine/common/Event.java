package com.jengine.common;

import java.util.ArrayList;

/**
 *
 */
public final class Event<TArgs extends EventArgs> implements IEvent<TArgs>
{
    private ArrayList<IEventHandler<TArgs>> _handlers = new ArrayList<IEventHandler<TArgs>>();

    public void doEvent(Object sender, TArgs args)
    {
        for(int i = _handlers.size() - 1; i >= 0; i--)
            _handlers.get(i).doHandle(sender, args);
    }

    //region IEvent<TArgs>

    @Override
    public void attach(IEventHandler<TArgs> handler) { _handlers.add(handler); }
    @Override
    public void detach(IEventHandler<TArgs> handler) { _handlers.remove(handler); }

    //endregion
}
