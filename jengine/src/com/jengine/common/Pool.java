package com.jengine.common;

import java.util.Stack;

/**
 * Пул объектов
 */
public class Pool<T>
{
    private Stack<T> _objects = new Stack<T>();
    private IFactory<T> _objectFactory;
    private int _maxPoolSize;

    /**
     * Возвращает количество свободных объектов в пуле
     */
    public int getCount()
    {
        return _objects.size();
    }

    /**
     * @param objectFactory фабрика для создания объектов пула при необходимости
     */
    public Pool(IFactory<T> objectFactory)
    {
        this(objectFactory, -1);
    }

    /**
     * @param objectFactory фабрика для создания объектов пула при необходимости
     */
    public Pool(IFactory<T> objectFactory, int maxPoolSize)
    {
        _objectFactory = objectFactory;
        _maxPoolSize = maxPoolSize;
    }

    /**
     * Возвращает свободный объект из пула, если свободных объектов нет фабрикой создается новый
     */
    public T get()
    {
        if(!_objects.empty())
        {
            return _objects.pop();
        }
        return _objectFactory.create();
    }

    /**
     * Возвращает объект в пул
     * @param object
     */
    public void put(T object)
    {
        if(_maxPoolSize > 0 && _objects.size() >= _maxPoolSize) return;
        _objects.push(object);
    }
}
