package com.jengine.common;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Денис on 20.03.2015.
 */
public class VectorHelper
{
    private static final Vector2 tempVec2 = new Vector2();
    private static final Vector3 tempVec3 = new Vector3();

    public static final Vector3 Z_AXIS = new Vector3(0f, 0f, 1f);

    public static final Vector2 up = new Vector2(0f, 1f);
    public static final Vector2 down = new Vector2(0f, -1f);

    public static Vector2 ToVector2(Vector3 v)
    {
        tempVec2.set(v.x, v.y);
        return tempVec2;
    }

    public static Vector3 ToVector3(Vector2 v)
    {
        tempVec3.set(v, 0f);
        return tempVec3;
    }
}
