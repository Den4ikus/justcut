package com.jengine.common;

/**
 * Created by ����� on 25.03.2015.
 * ��������� ������� ����� ������ � ���� �����������, ��� ����� ����� ������ ���� ������� ������� ������� �������
 */
public interface IEvent<TArgs extends EventArgs>
{
    void attach(IEventHandler<TArgs> handler);
    void detach(IEventHandler<TArgs> handler);
}