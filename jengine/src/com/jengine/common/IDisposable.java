package com.jengine.common;

import com.badlogic.gdx.utils.Disposable;

/**
 * Интерфейс Disposable в соответствии с C# coding standarts
 */
public interface IDisposable extends Disposable
{}
