package com.jengine.common;

/**
 * Интерфейс фабрики объектов
 */
public interface IFactory<T>
{
    /**
     * Создает новый экземпляр объекта и возвращает его
     */
    T create();
}
