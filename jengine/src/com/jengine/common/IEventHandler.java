package com.jengine.common;

/**
 * Created by Денис on 25.03.2015.
 * Обработчик события
 */
public interface IEventHandler<TArgs extends EventArgs>
{
    void doHandle(Object sender, TArgs args);
}
