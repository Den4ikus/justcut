package com.jengine;

/**
 * Created by Денис on 11.07.2015.
 */
public final class JEngine
{


    private IJEngineCfg config;
    public IJEngineCfg getConfig()
    {
        return config;
    }

    /**
     * Выполняет инициализацию движка
     * @param config конфигурация для инициализации
     */
    public void initialize(IJEngineCfg config)
    {
        this.config = config;
    }

    public void update()
    {

    }


    //region Singletone

    private JEngine()
    {}

    private static JEngine _instance = null;

    /**
     * Паттерн Singletone
     * @return Возвращает единственный экземпляр класса
     */
    public static JEngine getInstance()
    {
        return _instance == null ? _instance = new JEngine() : _instance;
    }

    //endregion
}
