package com.jengine.math;

import com.badlogic.gdx.math.Plane;

/**
 *
 */
public final class Planes
{
    public static final Plane XY = new Plane(Vector.Forward, 0f);
    public static final Plane YZ = new Plane(Vector.Left, 0f);
    public static final Plane XZ = new Plane(Vector.Up, 0f);
}
