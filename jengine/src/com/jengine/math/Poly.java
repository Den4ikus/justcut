package com.jengine.math;

import com.badlogic.gdx.math.Vector2;

/**
 * Предоставляет методы для работы с многоугольниками
 */
public final class Poly
{
	// Точка пересечения многоугольника, содержит индекс вершины после которой обнаружено пересечение и координаты точки пересечения
	private final static class CrossPoint
	{
		public int index;
		public Vector2 point;

		public CrossPoint(int index, Vector2 point)
		{
			this.index = index;
			this.point = point;
		}
	}

	private static Vector2 temp = new Vector2();
	
    private final static Vector2[] circleCorners = new Vector2[360];

    static
    {
        final float stepAngle = MathHelper.TwoPi / 360f;
        // рассчитываем единичные вектора по кругу в плоскости XY
        for (int i = 0; i < 360; i++)
        {
            float angle = i * stepAngle;
            circleCorners[i] = new Vector2((float)Math.cos(angle), (float)Math.sin(angle));
        }
    }

	/**
	 * Проверяет содержится ли точка в многоугольнике. Порядок вершин - по часовой стрелке.
	 * @param poly Вершины многоугольника
	 */
	public static boolean contains(Vector2[] poly, float x, float y)
	{
		int endIndex = poly.length - 1;
		for(int i = 0; i < endIndex; i++)
		{
			temp.set(x, y);
			if(MathHelper.crossProd(poly[i], poly[i + 1], temp) < 0f) return false;
		}

		// проверяем последнюю грань
		return MathHelper.crossProd(poly[endIndex], poly[0], temp) >= 0f;
	}

	/**
	 * Проверяет содержится ли точка в многоугольнике. Порядок вершин - по часовой стрелке.
	 * @param poly Вершины многоугольника
	 * @param point Проверяемая точка
	 */
	public static boolean contains(Vector2[] poly, Vector2 point)
	{
		int endIndex = poly.length - 1;
		for(int i = 0; i < endIndex; i++)
		{
            float res = MathHelper.crossProd(poly[i], poly[i + 1], point);
			if(MathHelper.crossProd(poly[i], poly[i + 1], point) > 0f) return false;
		}

		// проверяем последнюю грань
		return MathHelper.crossProd(poly[endIndex], poly[0], point) <= 0f;
	}
	
	public static boolean isConvex(Vector2[] poly)
	{
		int vertexCount = poly.length;
		
		if(MathHelper.crossProd(poly[0], poly[vertexCount - 1], poly[1]) > 0)
			return false;
		
		for(int i = 1; i < vertexCount - 1; i++)
		{
			if(MathHelper.crossProd(poly[i], poly[i - 1], poly[i + 1]) > 0)
				return false;
		}
		
		if(MathHelper.crossProd(poly[vertexCount - 1], poly[vertexCount - 2], poly[0]) > 0)
			return false;
		
		return true;
	}

	// Разделяет многоугольник секущей линией
	// Возвращает два массива точек описывающих получившиеся многоугольники или null если многоугольник невозможно разделить
	public static Vector2[][] split(Vector2[] poly, Line line)
	{
		CrossPoint[] crossPoints = getCrossPoints(poly, line);
		// Если точек пересечения меньше двух, разделить многоугольник невозможно
		if(crossPoints[0] == null || crossPoints[1] == null) return null;

		int v1Offset = crossPoints[0].index;
		int v2Offset = crossPoints[1].index;
		int truncatedVertexCount = v2Offset - v1Offset;
		// количество вершин в первой половине равно количеству отсеченных + 2 новых
		int firstPartVertexCount = truncatedVertexCount + 2;
		int secondPartVertexCount = poly.length - truncatedVertexCount + 2;

		Vector2[] firstPart = new Vector2[firstPartVertexCount];
		Vector2[] secondPart = new Vector2[secondPartVertexCount];
		Vector2[][] result = { firstPart, secondPart };

		// первая половина состоит из 1 части
		// от v1Offset до v2Offset + точки пересечения
		firstPart[0] = new Vector2(crossPoints[1].point);
		firstPart[1] = new Vector2(crossPoints[0].point);
		int fpIndex = 2;
		for(int i = v1Offset + 1; i <= v2Offset; i++)
			firstPart[fpIndex++] = new Vector2(poly[i]);

		// вторая половина состоит из 2 частей
		// 1) из элементов от v2Offset до конца
		// 2) из элементов от начала до v1Offset + точки пересечения
		secondPart[0] = new Vector2(crossPoints[0].point);
		secondPart[1] = new Vector2(crossPoints[1].point);
		int spIndex = 2;
		for(int i = v2Offset + 1; i < poly.length; i++)
			secondPart[spIndex++] = new Vector2(poly[i]);
		for(int i = 0; i <= v1Offset; i++)
			secondPart[spIndex++] = new Vector2(poly[i]);

		// Возвращаем объекты в пул
		Vector.put(crossPoints[0].point);
		Vector.put(crossPoints[1].point);

		return result;
	}

	private static CrossPoint[] getCrossPoints(Vector2[] poly, Line line)
	{
		// Ищем координаты точек пересечения граней фигуры, для выпуклого многоугольника их всегда не более двух
		// Возвращаем всегда массив из двух элементов, возможно не очень хорошо, но упрощает алгоритм
		CrossPoint[] crossPoints = new CrossPoint[2];

		// Проверяем что обе точки находятся вне полигона т.к. только в этом случае может быть 2 точки пересечения
		// TODO: проверить производительность без проверки местонахождения точек
		if(contains(poly, line.v1) || contains(poly, line.v2)) return crossPoints;

		int index = 0;
		for(int i = 0; i < poly.length; i++)
		{
			// Находим точки формирующие грань
			Vector2 v1 = poly[i];
			Vector2 v2 = poly[(i + 1) < poly.length ? i + 1 : 0];

			// Находим точку пересечения грани и секущей линии, если она существует - сохраняем ее
			Vector2 crossPoint = line.crossPoint(v1, v2);
			if(crossPoint != null)
			{
				crossPoints[index++] = new CrossPoint(i, crossPoint);
				if(index >= 2) break;
			}
		}
		return crossPoints;
	}

	//TODO выпилить
	/*public static Vector2[] createFromBox(BoundingBox2D box)
	{
		Vector2[] poly = new Vector2[4];
		return box.getCorners(poly);
	}
	
	public static Vector2[] createFromCircle(BoundingCircle circle, float accuracy)
	{
		int vertexCount = Math.round(MathHelper.TwoPi * circle.radius / accuracy);
		float step = 360f / vertexCount;

		Vector2[] poly = new Vector2[vertexCount];
		for(int i = 0; i < vertexCount; i++)
		{
			float angle = i * step;
			int index = Math.round(angle);
			poly[i] = new Vector2(circleCorners[index]).scl(circle.radius);
		}
		return poly;
	}*/
	


	
	/*public static boolean contains(Vector2[] poly, Vector2 point)
	{
		// �������� �������� �� �������������� ������������� 
		// �.�. ����� ����� ���������� ��� ��������� ������� � ����������� ������� �������� �����

		float minX = poly[0].x;
		float maxX = poly[0].x;
		float minY = poly[0].y;
		float maxY = poly[0].y;
		for(int i = 1; i < poly.length; i++)
		{
			Vector2 t = poly[i];
			if(t.x < minX) minX = t.x;
			if(t.x > maxX) maxX = t.x;
			if(t.y < minY) minY = t.y;
			if(t.y > maxY) maxY = t.y;
		}
		
		if (point.y < minX || point.X > maxX ||
			point.Y < minY || point.Y > maxY ) return false;


		// ��������
		// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
		boolean inside = false;
		for(int i = 0, j = poly.length - 1; i < poly.length ; j = i++ )
		{
			if ((poly[i].y > point.y) != (poly[j].y > point.y) &&
				 point.x < (poly[j].x - poly[i].x) * (point.y - poly[i].y ) / (poly[j].y - poly[i].y) + poly[i].x)
			{
				inside = !inside;
			}
		}
		return inside;
	}*/
}
