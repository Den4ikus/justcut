package com.jengine.math.bounds;

import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Vector2;
import com.jengine.math.*;

/**
 * Представляет ограничивающий многоугольник
 */
public final class BoundingPoly implements IBoundingShape2D
{
    private final Vector2[] _cachedVertices;

    public final Vector2[] vertices;

	/*public BoundingPoly(Vector2 a, Vector2 b, Vector2 c)
	{
		this(new Vector2[] { a, b, c });
	}*/
	
	public BoundingPoly(BoundingBox2D box)
	{
		this(new Vector2[] {
                new Vector2(box.v1.x, box.v1.y),
                new Vector2(box.v1.x, box.v1.y + box.getHeight()),
                new Vector2(box.v1.x + box.getWidth(), box.v1.y + box.getHeight()),
                new Vector2(box.v1.x + box.getWidth(), box.v1.y)
        });
	}
	
    public BoundingPoly(Vector2[] vertices)
    {
        // копируем вершины и создаем кэш
        this.vertices = new Vector2[vertices.length];
        _cachedVertices = new Vector2[vertices.length];
        for(int i = 0; i < vertices.length; i++)
        {
            this.vertices[i] = new Vector2(vertices[i]);
            _cachedVertices[i] = new Vector2(vertices[i]);
        }
    }

    //region IBoundingShape2D

    /**
     * Проверяет содержит ли фигура заданную фигуру
     * @param shape Проверяемая фигура
     */
    @Override
    public boolean contains(IBoundingShape2D shape)
    {
        if(shape instanceof BoundingBox2D) return contains((BoundingBox2D)shape);
        else if(shape instanceof BoundingPoly) return contains((BoundingPoly)shape);
        else if(shape instanceof BoundingCircle) return contains((BoundingCircle)shape);
        else throw new IllegalArgumentException("shape");
    }

    /**
     * Проверяет содержит ли фигура заданную точку
     */
    @Override
    public boolean contains(float x, float y)
    {
        return Poly.contains(this.vertices, x, y);
    }

    /**
     * Проверяет содержит ли фигура заданную точку
     * @param point Проверяемая точка
     */
    @Override
    public boolean contains(Vector2 point)
    {
        return Poly.contains(this.vertices, point);
    }

    /**
     * Проверяет содержит ли фигура заданный прямоугольник
     * @param box Проверяемый прямоугольник
     */
    @Override
    public boolean contains(BoundingBox2D box)
    {
        Vector2 temp = new Vector2();

        float width = box.getWidth();
        float height = box.getHeight();
		// проверяем углы прямоугольника
		temp.set(box.v1.x, box.v1.x);
		if(!contains(temp)) return false;
		temp.set(box.v1.x + width, box.v1.y);
		if(!contains(temp)) return false;
		temp.set(box.v1.x, box.v1.y + height);
		if(!contains(temp)) return false;
		temp.set(box.v1.x + width, box.v1.y + height);
		return contains(temp);
    }

    /**
     * Проверяет содержит ли фигура заданный круг
     * @param circle Проверяемый круг
     */
    @Override
    public boolean contains(BoundingCircle circle)
    {
		if(!contains(circle.center)) return false;
		
		float radius2 = circle.radius * circle.radius;
		int endIndex = vertices.length - 1;
		for(int i = 0; i < endIndex; i++)
		{
			Vector2 projPos = MathHelper.projPos(vertices[i], vertices[i + 1], circle.center);
			if(MathHelper.len2(circle.center, projPos) > radius2)
            {
                Vector.put(projPos);
                return false;
            }
            Vector.put(projPos);
		}
		// проверяем последний отрезок
		Vector2 projPos = MathHelper.projPos(vertices[endIndex], vertices[0], circle.center);
        boolean result = MathHelper.len2(circle.center, projPos) <= radius2;
        Vector.put(projPos);
		return result;
    }

    /**
     * Проверяет содержит ли фигура заданный многоугольник
     * @param poly Проверяемый многоугольник
     */
    @Override
    public boolean contains(BoundingPoly poly)
    {
		Vector2[] polyVertices = poly.vertices;
		for(int i = 0; i < polyVertices.length; i++)
		{
			if(!contains(polyVertices[i])) return false;
		}
        return true;
    }

	/**
	 * Проверяет пересекается ли фигура с заданным лучом
	 * @param ray Луч
	 */
	@Override
	public boolean intersectWith(Ray2D ray)
	{
		Vector2 origin = ray.origin;
		Vector2 dir = ray.direction;
		Vector2 vertex = vertices[0];
		float firstCrossProd = MathHelper.crossProd(dir, vertex.x - origin.x, vertex.y - origin.y);
		if(firstCrossProd > 0f)
		{
			for(int i = 0; i < vertices.length; i++)
			{
				vertex = vertices[i];
				if(MathHelper.crossProd(dir, vertex.x - origin.x, vertex.y - origin.y) <= 0f) return true;
			}
			return false;
		}
		else if(firstCrossProd < 0f)
		{
			for(int i = 0; i < vertices.length; i++)
			{
				vertex = vertices[i];
				if(MathHelper.crossProd(dir, vertex.x - origin.x, vertex.y - origin.y) >= 0f) return true;
			}
			return false;
		}
		else return true;
	}
	
    /**
     * Проверяет пересекается ли фигура с заданной фигурой
     * @param shape Проверяемая фигура
     */
    @Override
    public boolean intersectWith(IBoundingShape2D shape)
    {
        if(shape instanceof BoundingBox2D) return intersectWith((BoundingBox2D)shape);
        else if(shape instanceof BoundingPoly) return intersectWith((BoundingPoly)shape);
        else if(shape instanceof BoundingCircle) return intersectWith((BoundingCircle)shape);
        else throw new IllegalArgumentException("shape");
    }

    /**
     * Проверяет пересекается ли фигура с заданным прямоугольником
     * @param box Проверяемый прямоугольник
     */
    @Override
    public boolean intersectWith(BoundingBox2D box)
    {
        return IntersectionHelper.intersect(box, this);
    }

    /**
     * Проверяет пересекается ли фигура с заданным кругом
     * @param circle Проверяемый круг
     */
    @Override
    public boolean intersectWith(BoundingCircle circle)
    {
        return IntersectionHelper.intersect(circle, this);
    }

    /**
     * Проверяет пересекается ли фигура с заданным многоугольником
     * @param poly Проверяемый многоугольник
     */
    @Override
    public boolean intersectWith(BoundingPoly poly)
    {
		// todo необходимо сравнить производительность с теоремой о разделяющей оси (SAT)
		// в теории этот способ быстрее определеяет пересекающиеся многоугольники а SAT наоборот быстрее для непересекающихся
		
		// проверяем содержатся ли точки проверяемого многоугольника в данном многоугольнике
		Vector2[] polyVertices = poly.vertices;
        for(int i = 0; i < polyVertices.length; i++)
			if(contains(polyVertices[i])) return true;
		
		// проверяем содержатся ли точки данного многоугольника в проверяемом многоугольнике
		for(int i = 0; i < vertices.length; i++)
			if(poly.contains(vertices[i])) return true;
		
		return false;
    }

    /**
     * Применяет заданную трансформацию к фигуре
     * @param transformation Трансформация
     */
    @Override
    public void transformApply(Affine2 transformation)
    {
		for(int i = 0; i < vertices.length; i++)
		{
			_cachedVertices[i].set(vertices[i]);
			transformation.applyTo(vertices[i]);
		}
    }

    /**
     * Отменяет примененную трансформацию к фигуре.
     * Такое решение нужно чтобы не создавать каждый раз новый объект фигуры при трансформации
     * т.к. например в случае с полигонами нужно создавать кучу трансформированных векторов каждый кадр
     * и java на андроиде просто умирает от таких нагрузок :-)
     */
    @Override
    public void transformRollback()
    {
		// восстанавливаем положение вершин до трансформации
		for(int i = 0; i < vertices.length; i++)
			vertices[i].set(_cachedVertices[i]);
    }

    //endregion
}
