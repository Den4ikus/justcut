package com.jengine.math.bounds;

import com.badlogic.gdx.math.Vector2;

/**
 * Класс с функциями проверки пересечений
 * т.к. фигуры включают в себя функции проверки пересечения со всеми остальными 
 * для исключения дублирования, взаимнообратные функции собраны в этом классе
 */
final class IntersectionHelper
{
    public static boolean intersect(BoundingBox2D box, BoundingCircle circle)
    {
        float dx = Math.abs(circle.center.x - box.v1.x);
        float dy = Math.abs(circle.center.y - box.v1.y);
        float halfWidth = box.getWidth() * 0.5f;
        float halfHeight = box.getHeight() * 0.5f;

        if(dx > (halfWidth + circle.radius)) return false;
        else if(dx <= halfWidth) return true;

        if(dy > (halfHeight + circle.radius)) return false;
        else if(dy <= halfHeight) return true;

        float cornerDist2 = (dx - halfWidth) * (dx - halfWidth) + (dy - halfHeight) * (dy - halfHeight);
        return cornerDist2 <= (circle.radius * circle.radius);
    }
	
	public static boolean intersect(BoundingBox2D box, BoundingPoly poly)
	{
		Vector2[] polyVertices = poly.vertices;
		for(int i = 0; i < polyVertices.length; i++)
			if(box.contains(polyVertices[i])) return true;
		return false;
	}
	
	public static boolean intersect(BoundingCircle circle, BoundingPoly poly)
    {
		Vector2[] polyVertices = poly.vertices;
		for(int i = 0; i < polyVertices.length; i++)
			if(circle.contains(polyVertices[i])) return true;
		return false;
    }
}
