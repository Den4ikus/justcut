package com.jengine.math.bounds;

import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Vector2;
import com.jengine.math.Ray2D;

/**
 * Двумерная ограничивающая фигура
 */
public interface IBoundingShape2D
{
    /**
     * Проверяет содержит ли фигура заданную фигуру
     * @param shape Проверяемая фигура
     */
    boolean contains(IBoundingShape2D shape);

    /**
     * Проверяет содержит ли фигура заданную точку
     */
    boolean contains(float x, float y);

    /**
     * Проверяет содержит ли фигура заданную точку
     * @param point Проверяемая точка
     */
    boolean contains(Vector2 point);

    /**
     * Проверяет содержит ли фигура заданный прямоугольник
     * @param box Проверяемый прямоугольник
     */
    boolean contains(BoundingBox2D box);

    /**
     * Проверяет содержит ли фигура заданный круг
     * @param circle Проверяемый круг
     */
    boolean contains(BoundingCircle circle);

    /**
     * Проверяет содержит ли фигура заданный многоугольник
     * @param poly Проверяемый многоугольник
     */
    boolean contains(BoundingPoly poly);

	/**
	 * Проверяет пересекается ли фигура с заданным лучом
	 * @param ray Луч
	 */
	boolean intersectWith(Ray2D ray);
	
    /**
     * Проверяет пересекается ли фигура с заданной фигурой
     * @param shape Проверяемая фигура
     */
    boolean intersectWith(IBoundingShape2D shape);

    /**
     * Проверяет пересекается ли фигура с заданным прямоугольником
     * @param box Проверяемый прямоугольник
     */
    boolean intersectWith(BoundingBox2D box);

    /**
     * Проверяет пересекается ли фигура с заданным кругом
     * @param circle Проверяемый круг
     */
    boolean intersectWith(BoundingCircle circle);

    /**
     * Проверяет пересекается ли фигура с заданным многоугольником
     * @param poly Проверяемый многоугольник
     */
    boolean intersectWith(BoundingPoly poly);

    /**
     * Применяет заданную трансформацию к фигуре
     * @param transformation Трансформация
     */
    void transformApply(Affine2 transformation);

    /**
     * Отменяет примененную трансформацию к фигуре.
     * Такое решение нужно чтобы не создавать каждый раз новый объект фигуры при трансформации
     * т.к. например в случае с полигонами нужно создавать кучу трансформированных векторов каждый кадр
     * и java на андроиде просто умирает от таких нагрузок :-)
     */
    void transformRollback();
}