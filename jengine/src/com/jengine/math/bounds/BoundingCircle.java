package com.jengine.math.bounds;

import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Vector2;
import com.jengine.math.MathHelper;
import com.jengine.math.Ray2D;
import com.jengine.math.Vector;

/**
 * Представляет ограничивающий круг
 */
public final class BoundingCircle implements IBoundingShape2D
{
    private final Vector2 _cachedCenter = new Vector2();

    public final Vector2 center;
    public final float radius;

	public BoundingCircle(Vector2 center, float radius)
	{
		this.center = new Vector2(center);
		this.radius = radius;
	}
	
    public BoundingCircle(float x, float y, float radius)
    {
        this.center = new Vector2(x, y);
        this.radius = radius;
    }
	
    //region IBoundingShape2D

    /**
     * Проверяет содержит ли фигура заданную фигуру
     * @param shape Проверяемая фигура
     */
    @Override
    public boolean contains(IBoundingShape2D shape)
    {
        if(shape instanceof BoundingBox2D) return contains((BoundingBox2D)shape);
        else if(shape instanceof BoundingPoly) return contains((BoundingPoly)shape);
        else if(shape instanceof BoundingCircle) return contains((BoundingCircle)shape);
        else throw new IllegalArgumentException("shape");
    }

    /**
     * Проверяет содержит ли фигура заданную точку
     */
    @Override
    public boolean contains(float x, float y)
    {
        return MathHelper.len2(this.center.x, this.center.y, x, y) <= (this.radius * this.radius);
    }

    /**
     * Проверяет содержит ли фигура заданную точку
     * @param point Проверяемая точка
     */
    @Override
    public boolean contains(Vector2 point)
    {
        return contains(point.x, point.y);
    }

    /**
     * Проверяет содержит ли фигура заданный прямоугольник
     * @param box Проверяемый прямоугольник
     */
    @Override
    public boolean contains(BoundingBox2D box)
    {
		Vector2 temp = new Vector2();
        float radius2 = this.radius * this.radius;
		
		// проверяем все вершины бокса
		temp.set(box.v1.x, box.v1.y);
		if(MathHelper.len2(temp, center) > radius2) return false;
		temp.set(box.v1.x + box.getWidth(), box.v1.y);
		if(MathHelper.len2(temp, center) > radius2) return false;
		temp.set(box.v1.x, box.v1.y + box.getHeight());
		if(MathHelper.len2(temp, center) > radius2) return false;
		temp.set(box.v1.x + box.getWidth(), box.v1.y + box.getHeight());
		return MathHelper.len2(temp, center) <= radius2;
    }

    /**
     * Проверяет содержит ли фигура заданный круг
     * @param circle Проверяемый круг
     */
    @Override
    public boolean contains(BoundingCircle circle)
    {
        float dr = this.radius - circle.radius;
        return MathHelper.len2(this.center, circle.center) <= (dr * dr);
    }

    /**
     * Проверяет содержит ли фигура заданный многоугольник
     * @param poly Проверяемый многоугольник
     */
    @Override
    public boolean contains(BoundingPoly poly)
    {
        Vector2[] vertices = poly.vertices;
        for(int i = 0; i < vertices.length; i++)
        {
            if(!contains(vertices[i])) return false;
        }
        return true;
    }

	/**
	 * Проверяет пересекается ли фигура с заданным лучом
	 * @param ray Луч
	 */
	@Override
	public boolean intersectWith(Ray2D ray)
	{
		Vector2 origin = ray.origin;
		Vector2 dir = ray.direction;
		Vector2 pos = MathHelper.projPos(origin.x, origin.y, origin.x + dir.x, origin.y + dir.y, center.x, center.y);
		if(MathHelper.len2(center, pos) <= this.radius * this.radius)
		{
            Vector.put(pos);
			float proj = MathHelper.proj(origin.x, origin.y, origin.x + dir.x, origin.y + dir.y, center.x, center.y);
			return proj > 0f || contains(origin);
		}
        Vector.put(pos);
		return false; 
	}
	
    /**
     * Проверяет пересекается ли фигура с заданной фигурой
     * @param shape Проверяемая фигура
     */
    @Override
    public boolean intersectWith(IBoundingShape2D shape)
    {
        if(shape instanceof BoundingBox2D) return intersectWith((BoundingBox2D)shape);
        else if(shape instanceof BoundingPoly) return intersectWith((BoundingPoly)shape);
        else if(shape instanceof BoundingCircle) return intersectWith((BoundingCircle)shape);
        else throw new IllegalArgumentException("shape");
    }

    /**
     * Проверяет пересекается ли фигура с заданным прямоугольником
     * @param box Проверяемый прямоугольник
     */
    @Override
    public boolean intersectWith(BoundingBox2D box)
    {
        return IntersectionHelper.intersect(box, this);
    }

    /**
     * Проверяет пересекается ли фигура с заданным кругом
     * @param circle Проверяемый круг
     */
    @Override
    public boolean intersectWith(BoundingCircle circle)
    {
        float rSum = this.radius + circle.radius;
        return MathHelper.len2(this.center, circle.center) <= (rSum * rSum);
    }

    /**
     * Проверяет пересекается ли фигура с заданным многоугольником
     * @param poly Проверяемый многоугольник
     */
    @Override
    public boolean intersectWith(BoundingPoly poly)
    {
        return IntersectionHelper.intersect(this, poly);
    }

    /**
     * Применяет заданную трансформацию к фигуре
     * @param transformation Трансформация
     */
    @Override
    public void transformApply(Affine2 transformation)
    {
        // сохраняем позицию до трансформации
        _cachedCenter.set(center);
        transformation.applyTo(center);
    }

    /**
     * Отменяет примененную трансформацию к фигуре.
     * Такое решение нужно чтобы не создавать каждый раз новый объект фигуры при трансформации
     * т.к. например в случае с полигонами нужно создавать кучу трансформированных векторов каждый кадр
     * и java на андроиде просто умирает от таких нагрузок :-)
     */
    @Override
    public void transformRollback()
    {
        // восстанавливаем позицию до трансформации
        center.set(_cachedCenter);
    }

    //endregion
}
