package com.jengine.math.bounds;

import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Vector2;
import com.jengine.math.MathHelper;
import com.jengine.math.Ray2D;
import com.jengine.math.Vector;

/**
 * Представляет выровненный по осям ограничивающий прямоугольник
 */
public final class BoundingBox2D implements IBoundingShape2D
{
    private final Vector2 _cachedV1 = new Vector2();
    private final Vector2 _cachedV2 = new Vector2();

    public final Vector2 v1 = new Vector2();
    public final Vector2 v2 = new Vector2();

    public float getWidth()
    {
        return v2.x - v1.x;
    }

    public final float getHeight()
    {
        return v2.y - v1.y;
    }

    public Vector2 getCenter()
    {
        return new Vector2((v1.x + v2.x) * 0.5f, (v1.y + v2.y) * 0.5f);
    }

    public Vector2[] getCorners(Vector2[] target)
    {
        target[0].set(v1);
        target[1].set(v1.x, v1.y + getHeight());
        target[2].set(v1.x + getWidth(), v1.y + getHeight());
        target[3].set(v1.x + getWidth(), v1.y);
        return target;
    }

    public BoundingBox2D(Vector2 v1, Vector2 v2)
    {
        // Находим левый нижний и правый верхний угол
        Vector2 min = MathHelper.min(v1, v2);
        Vector2 max = MathHelper.max(v1, v2);

        this.v1.set(min);
        this.v2.set(max);

        Vector.put(min);
        Vector.put(max);
    }

	public BoundingBox2D(Vector2 pos, float width, float height)
	{
		this.v1.set(pos);
        this.v2.set(pos.x + width, pos.y + height);
	}
	
    public BoundingBox2D(float x, float y, float width, float height)
    {
        this.v1.set(x, y);
        this.v2.set(x + width, y + height);
    }

    public void fromPoints(Vector2 v1, Vector2 v2)
    {
        // Находим левый нижний и правый верхний угол
        Vector2 min = MathHelper.min(v1, v2);
        Vector2 max = MathHelper.max(v1, v2);

        this.v1.set(min);
        this.v2.set(max);

        Vector.put(min);
        Vector.put(max);
    }

    //region IBoundingShape2D

    /**
     * Проверяет содержит ли фигура заданную фигуру
     * @param shape Проверяемая фигура
     */
    @Override
    public boolean contains(IBoundingShape2D shape)
    {
        if(shape instanceof BoundingBox2D) return contains((BoundingBox2D)shape);
        else if(shape instanceof BoundingPoly) return contains((BoundingPoly)shape);
        else if(shape instanceof BoundingCircle) return contains((BoundingCircle)shape);
        else throw new IllegalArgumentException("shape");
    }

    /**
     * Проверяет содержит ли фигура заданную точку
     */
    @Override
    public boolean contains(float x, float y)
    {
        return  (x >= this.v1.x) &&
                (y >= this.v1.y) &&
                (x <= (this.v1.x + this.getWidth())) &&
                (y <= (this.v1.y + this.getHeight()));
    }

    /**
     * Проверяет содержит ли фигура заданную точку
     * @param point Проверяемая точка
     */
    @Override
    public boolean contains(Vector2 point)
    {
        return contains(point.x, point.y);
    }

    /**
     * Проверяет содержит ли фигура заданный прямоугольник
     * @param box Проверяемый прямоугольник
     */
    @Override
    public boolean contains(BoundingBox2D box)
    {
        return  (box.v1.x >= this.v1.x) &&
                (box.v1.y >= this.v1.y) &&
                (box.getWidth() <= this.getWidth()) &&
                (box.getHeight() <= this.getHeight());
    }

    /**
     * Проверяет содержит ли фигура заданный круг
     *
     * @param circle Проверяемый круг
     */
    @Override
    public boolean contains(BoundingCircle circle)
    {
        float dx = circle.center.x - this.v1.x;
        float dy = circle.center.y - this.v1.y;
        return  dx >= circle.radius &&
                dy >= circle.radius &&
                this.getWidth() >= (dx - circle.radius) &&
                this.getHeight() >= (dy - circle.radius);
    }

    /**
     * Проверяет содержит ли фигура заданный многоугольник
     * @param poly Проверяемый многоугольник
     */
    @Override
    public boolean contains(BoundingPoly poly)
    {
        Vector2[] vertices = poly.vertices;
        for(int i = 0; i < vertices.length; i++)
        {
            if(!contains(vertices[i])) return false;
        }
        return true;
    }

	/**
	 * Проверяет пересекается ли фигура с заданным лучом
	 * @param ray Луч
	 */
	@Override
	public boolean intersectWith(Ray2D ray)
	{
		Vector2 origin = ray.origin;
		Vector2 dir = ray.direction;

        float width = getWidth();
        float height = getHeight();
		float firstCrossProd = MathHelper.crossProd(dir, v1.x - origin.x, v1.y - origin.y);
		if(firstCrossProd > 0f)
		{
			if(MathHelper.crossProd(dir, v1.x + width - origin.x, v1.y - origin.y) <= 0f) return true;
			if(MathHelper.crossProd(dir, v1.x - origin.x, v1.y + height - origin.y) <= 0f) return true;
			if(MathHelper.crossProd(dir, v1.x + width - origin.x, v1.y + height - origin.y) <= 0f) return true;
			return false;
		}
		else if(firstCrossProd < 0f)
		{
			if(MathHelper.crossProd(dir, v1.x + width - origin.x, v1.y - origin.y) >= 0f) return true;
			if(MathHelper.crossProd(dir, v1.x - origin.x, v1.y + height - origin.y) >= 0f) return true;
			if(MathHelper.crossProd(dir, v1.x + width - origin.x, v1.y + height - origin.y) >= 0f) return true;
			return false;
		}
		else return true;
	}
	
    /**
     * Проверяет пересекается ли фигура с заданной фигурой
     * @param shape Проверяемая фигура
     */
    @Override
    public boolean intersectWith(IBoundingShape2D shape)
    {
        if(shape instanceof BoundingBox2D) return intersectWith((BoundingBox2D)shape);
        else if(shape instanceof BoundingPoly) return intersectWith((BoundingPoly)shape);
        else if(shape instanceof BoundingCircle) return intersectWith((BoundingCircle)shape);
        else throw new IllegalArgumentException("shape");
    }

    /**
     * Проверяет пересекается ли фигура с заданным прямоугольником
     * @param box Проверяемый прямоугольник
     */
    @Override
    public boolean intersectWith(BoundingBox2D box)
    {
        return  (Math.abs(this.v1.x - box.v1.x) * 2f <= (this.getWidth() + box.getWidth())) &&
                (Math.abs(this.v1.y - box.v1.y) * 2f <= (this.getHeight() + box.getHeight()));
    }

    /**
     * Проверяет пересекается ли фигура с заданным кругом
     * @param circle Проверяемый круг
     */
    @Override
    public boolean intersectWith(BoundingCircle circle)
    {
        return IntersectionHelper.intersect(this, circle);
    }

    /**
     * Проверяет пересекается ли фигура с заданным многоугольником
     * @param poly Проверяемый многоугольник
     */
    @Override
    public boolean intersectWith(BoundingPoly poly)
    {
        return IntersectionHelper.intersect(this, poly);
    }

    /**
     * Применяет заданную трансформацию к фигуре
     * @param transformation Трансформация
     */
    @Override
    public void transformApply(Affine2 transformation)
    {
        // т.к. бокс выровнен по осям, применяем только перемещение
        _cachedV1.set(v1);
        _cachedV2.set(v2);
        transformation.getTranslation(v1);
        transformation.getTranslation(v2);
        v1.add(_cachedV1);
        v2.add(_cachedV2);
    }

    /**
     * Отменяет примененную трансформацию к фигуре.
     * Такое решение нужно чтобы не создавать каждый раз новый объект фигуры при трансформации
     * т.к. например в случае с полигонами нужно создавать кучу трансформированных векторов каждый кадр
     * и java на андроиде просто умирает от таких нагрузок :-)
     */
    @Override
    public void transformRollback()
    {
        // восстанавливаем позицию до трансформации
        v1.set(_cachedV1);
        v2.set(_cachedV2);
    }

    //endregion

    /*public static BoundingBox2D fromVertices(Vector2 ... vertices)
    {
        float minX = vertices[0].x;
        float maxX = vertices[0].x;
        float minY = vertices[0].y;
        float maxY = vertices[0].y;

        for(int i = 1; i < vertices.length; i++)
        {
            if(vertices[i].x < minX) minX = vertices[i].x;
            else if(vertices[i].x > maxX) maxX = vertices[i].x;
            if(vertices[i].y < minY) minY = vertices[i].y;
            else if(vertices[i].y > maxY) maxY = vertices[i].y;
        }

        return new BoundingBox2D(minX, minY, maxX - minX, maxY - minY);
    }*/
}
