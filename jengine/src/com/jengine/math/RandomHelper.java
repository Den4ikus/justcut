package com.jengine.math;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 *
 */
public final class RandomHelper
{
    private static Random _random = new Random();
    private static char[] _stringCharacters = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();;

    public static float floatValue()
    {
        return _random.nextFloat();
    }

    public static float floatValue(float min, float max)
    {
        return min + (_random.nextFloat() * (max - min));
    }

    public static int intValue()
    {
        return _random.nextInt();
    }

    public static int intValue(int min, int max)
    {
        return min + _random.nextInt(max - min + 1);
    }

    public static void vector2(Vector2 v)
    {
        v.set(_random.nextFloat(), _random.nextFloat());
    }

    public static void vector2(Vector2 v, float min, float max)
    {
        v.set(floatValue(min, max), floatValue(min, max));
    }

    public static void vector2(Vector2[] v, float min, float max)
    {
        for(int i = 0; i < v.length; i++)
            v[i].set(floatValue(min, max), floatValue(min, max));
    }

    public static Color color()
    {
        return new Color(_random.nextInt() | 0x000000FF);
    }

    public static Color color(Color color)
    {
        return color.set(_random.nextInt() | 0x000000FF);
    }

    public static String string(int length)
    {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = _stringCharacters[_random.nextInt(_stringCharacters.length)];
        }
        return new String(text);
    }
}
