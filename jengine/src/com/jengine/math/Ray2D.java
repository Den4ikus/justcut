package com.jengine.math;

import com.badlogic.gdx.math.Vector2;

/**
 * Представляет двухмерный луч
 */
public final class Ray2D
{
	public final Vector2 origin = new Vector2();
	public final Vector2 direction = new Vector2();
	
	public void set(Vector2 origin, Vector2 target)
	{
		this.origin.set(origin);
		this.direction.set(target).sub(origin);
		MathHelper.normalize(this.direction);
	}
	
	public Ray2D()
	{}
	
	public Ray2D(Vector2 origin, Vector2 direction)
	{
		this.origin.set(origin);
		this.direction.set(direction);
	}
}
