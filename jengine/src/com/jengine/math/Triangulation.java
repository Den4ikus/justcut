/*public final class Triangle
{
	public final Vector2[] vertices;
	public Triangle triangles;
	
	public Triangle(Vector2 v1, Vector2 v2, Vector2 v3)
	{
		this.vertices = new Vector2[3] { v1, v2, v3 };
	}
	
}

public final class Triangulation
{
	// вершины должны описывать контур по часовой стрелке
	public static Vector2[] triangulate(Vector2[] vertices)
	{
		int vertexCount = vertices.count;
		int triangleCount = vertexCount - 1;
		Vector2[] triangulation = new Vector2[triangleCount * 3];
		int vertexIndex = 0;
		
		final int firstForwardIndex = 0;
		final int firstBackwardIndex = vertexCount - 1;
		int forwardIndex = firstForwardIndex;
		int backwardIndex = firstBackwardIndex;
		
		while(vertexIndex < vertexCount)
		{
			// определяем в какую сторону двигаться по контуру
			if(forwardIndex - firstForwardIndex <= firstBackwardIndex - backwardIndex)
			{
				// двигаемся вперед
				triangulation[vertexIndex++] = vertices[forwardIndex++];
				triangulation[vertexIndex++] = vertices[forwardIndex];
				triangulation[vertexIndex++] = vertices[backwardIndex];
			}
			else
			{
				// двигаемся назад
				triangulation[vertexIndex++] = vertices[backwardIndex--];
				triangulation[vertexIndex++] = vertices[forwardIndex];
				triangulation[vertexIndex++] = vertices[backwardIndex];
			}
		}
		
		// каждые 3 вершины массива описывают треугольник, вершины идут по часовой стрелке
		return triangulation;
	}
}

/* todo: триангуляция в стандартном виде не понадобилась т.к. изначально известно, что все точки находятся на переферии
public final class Triangulation
{
	private ArrayList<Triangle> _triangles = new ArrayList<Triangle>();
	
	public Triangulation(Vector2[] vertices, float superStructOffset)
	{
		// создаем суперструктуру
		Vector2 firstPoint = vertices[0];
		Vector2 ss1 = new Vector2(firstPoint);
		Vector2 ss4 = new Vector2(firstPoint);
		for(int i = 0; i < vertices.Count; i++)
		{
			float currX = vertices[i].x;
			float currY = vertices[i].y;
			if(currX < ss1.x) ss1.x = currX - superStructOffset;
			if(currY < ss1.y) ss1.y = currY - superStructOffset;
			if(currX > ss4.x) ss4.x = currX + superStructOffset;
			if(currY > ss4.y) ss4.y = currY + superStructOffset;
		}
		Vector2 ss2 = new Vector2(ss1.x, ss4.y);
		Vector2 ss3 = new Vector2(ss4.x, ss1.y);
		
		// получилась такая структура 
		// ss1--ss3
		//  |	 |
		// ss2--ss4
		
		// создаем треугольники для первой точки
		
		Triangle first = 
		_triangles.add(new Triangle(new Vector2[] { ss1, ss3, firstPoint},
			))
	}
	
	private static Triangle[] splitTriangle(Triangle t, Vector2 point)
	{
		Vector2 v1 = t.vertices[0];
		Vector2 v2 = t.vertices[1];
		Vector2 v3 = t.vertices[2];
		
		// проверяем лежит ли точка на ребрах треугольника
		// т.к. проверка на нахождение точки внутри треугольника включает случай нахождения точки на ребре
		// достаточно проверить лежит ли точка на прямой
		if(JMath.Line.contains(v1, v2, point))
		{
			// разбиваем треугольник по ребру v1-v2 и смежный треугольник, противолежащий v3
			
		}
		else if(JMath.Line.contains(v2, v3, point))
		{
			// разбиваем треугольник по ребру v2-v3 и смежный треугольник, противолежащий v1
		}
		else if(JMath.Line.contains(v3, v1, point))
		{
			// разбиваем треугольник по ребру v3-v1 и смежный треугольник, противолежащий v2
		}
		else
		{
			// точка находится внутри треугольника, разбиваем его на 3 части
			Triangle n1 = new Triangle(v1, v2, point);
			Triangle n2 = new Triangle(v2, v3, point);
			Triangle n3 = new Triangle(v1, point, v3);
			n1.triangles = new Triangle[] { n2, n3, t.triangles[2] };
			n2.triangles = new Triangle[] { n3, n1, t.triangles[0] };
			n3.triangles = new Triangle[] { n2, t.triangles[1], n1 };
		}
	}
}*/