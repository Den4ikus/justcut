package com.jengine.math;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.jengine.common.IFactory;
import com.jengine.common.Pool;

/**
 * Класс для работы с векторами
 */
public final class Vector
{
	//region VectorPool

	private static final Pool<Vector2> Vector2Pool = new Pool<Vector2>(new IFactory<Vector2>()
	{
		@Override
		public Vector2 create()
		{
			return new Vector2();
		}
	}, 1024);
	private static final Pool<Vector3> Vector3Pool = new Pool<Vector3>(new IFactory<Vector3>()
	{
		@Override
		public Vector3 create()
		{
			return new Vector3();
		}
	}, 1024);

	/**
	 * Получает объект из пула
	 * Для возвращения объекта в пул используется метод put
	 */
	public static Vector2 getVector2()
	{
		return Vector2Pool.get();
	}

	/**
	 * Получает объект из пула
	 * Для возвращения объекта в пул используется метод put
	 */
	public static Vector3 getVector3()
	{
		return Vector3Pool.get();
	}

	/**
	 * Возвращает объект в пул
	 * Для получения объекта из пула используется метод get
	 */
	public static void put(Vector2 v)
	{
		Vector2Pool.put(v);
	}

	/**
	 * Возвращает объект в пул
	 * Для получения объекта из пула используется метод get
	 */
	public static void put(Vector3 v)
	{
		Vector3Pool.put(v);
	}

	//endregion

	public static Vector3 Zero = new Vector3(0f, 0f, 0f);
	public static Vector3 One = new Vector3(1f, 1f, 1f);

	public static Vector3 Forward = new Vector3(0f, 0f, -1f);
	public static Vector3 Backward = new Vector3(0f, 0f, 1f);
	public static Vector3 Up = new Vector3(0f, 1f, 0f);
	public static Vector3 Down = new Vector3(0f, -1f, 0f);
	public static Vector3 Left = new Vector3(-1f, 0f, 0f);
	public static Vector3 Right = new Vector3(1f, 0f, 0f);

	public static Vector3 AxisX = new Vector3(1f, 0f, 0f);
	public static Vector3 AxisY = new Vector3(0f, 1f, 0f);
	public static Vector3 AxisZ = new Vector3(0f, 0f, 1f);
}
