package com.jengine.math;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Денис on 03.11.2015.
 */
public final class MathHelper
{
	/*public final class Line
	{
		public static boolean contains(Vector2 v1, Vector2 v2, Vector2 point)
		{
			return equals((point.x - v0.x) / (v1.x - v0.x), (point.y - v0.y) / (v1.y - v0.y));
		}
	}
	
	public final class LineSegment
	{
		public static boolean contains(Vector2 v1, Vector2 v2, Vector2 point)
		{
			float kx, ky;
			return inRange(kx = ((point.X - v0.X) / (v1.X - v0.X)), 0f, 1f) && 
				   inRange(ky = ((point.Y - v0.Y) / (v1.Y - v0.Y)), 0f, 1f) && 
				   equals(kx, ky);
		}
		
		public static Vector2 intersect(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 v4)
		{
			float k1 = (v2.y - v1.y) / (v2.x - v1.x);
			float k2 = (v4.y - v3.y) / (v4.x - v3.x);
			if(k1 == k2) return null; // ????? ??????????? ????? ??????????? ???? ??? ???? ?????????
			
			float b1 = (v2.x * v1.y - v1.x * v2.y) / (v2.x - v1.x);
			float b2 = (v4.x * v3.y - v3.x * v4.y) / (v4.x - v3.x);
			float x = (b2 - b1) / (k1 - k2);
			float y = k1 * (b2 - b1) / (k1 - k2) + b1;
			
			boolean cross = (x >= v1.x) && (x <= v2.x) && (x >= v3.x) && (x <= v4.x) &&
							(y >= v1.y) && (y <= v2.y) && (y >= v3.y) && (y <= v4.y);
			return cross ? new Vector2(x, y) : null;
		}
	}
	
	public final class Triangle
	{
		public static boolean contains(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 point)
		{
			return crossProd(point, v1, v2) < 0f &&
				   crossProd(point, v2, v3) < 0f &&
				   crossProd(point, v3, v1) < 0f;
		}
	}*/

	public final static float Epsilon = MathUtils.FLOAT_ROUNDING_ERROR;
    public final static float Pi = (float)Math.PI;
	public final static float TwoPi = (float)(2.0 * Math.PI);
	public final static float Sqrt2 = (float)Math.sqrt(2.0);
	public final static float InvSqrt2 = (float)(1.0 / Math.sqrt(2.0));

    public static Vector3 getVector3Zero() { return new Vector3(); }
	public static Vector3 getVector3One() { return new Vector3(1f, 1f, 1f); }
    public static Matrix4 getMatrixIdentity() { return new Matrix4(); }

	public static boolean equals(float x, float y) { return Math.abs(x - y) < Epsilon; }
	public static boolean equals(float x, float y, float epsilon) { return Math.abs(x - y) < epsilon; }
	public static boolean equals(Vector2 a, Vector2 b, float epsilon)
	{
		return Math.abs(a.x - b.x) < epsilon && Math.abs(a.y - b.y) < epsilon;
	}
	public static boolean equalZero(float v) { return Math.abs(v) < Epsilon; }
	
	public static boolean inRange(float v, float min, float max) { return v >= min && v <= max; }

    public static Vector2 avg(Vector2 ... points)
    {
        float x = 0f;
        float y = 0f;
        for(int i = 0; i < points.length; i++)
        {
            x += points[i].x;
            y += points[i].y;
        }
        return Vector.getVector2().set(x / points.length, y / points.length);
    }

    public static float clamp(float value)
    {
        return Math.max(Math.min(value, 1f), 0f);
    }

    public static float clamp(float value, float min, float max)
    {
        return Math.max(Math.min(value, max), min);
    }

    /**
     * Возвращает минимальные компоненты векторов в новом векторе. ВНИМАНИЕ!!! Возвращается объект из пула
     */
    public static Vector2 min(Vector2 v1, Vector2 v2)
    {
        return Vector.getVector2().set(Math.min(v1.x, v2.x), Math.min(v1.y, v2.y));
    }

    /**
     * Возвращает максимальные компоненты векторов в новом векторе. ВНИМАНИЕ!!! Возвращается объект из пула
     */
    public static Vector2 max(Vector2 v1, Vector2 v2)
    {
        return Vector.getVector2().set(Math.max(v1.x, v2.x), Math.max(v1.y, v2.y));
    }

    /**
     * Возвращает длину вектора V
     */
    public static float len(Vector2 v)
    {
        return (float)Math.sqrt(v.x * v.x + v.y * v.y);
    }

    /**
     * Возвращает длину вектора AB
     */
    public static float len(Vector2 a, Vector2 b)
    {
        float dx = b.x - a.x;
        float dy = b.y - a.y;
        return (float)Math.sqrt(dx * dx + dy * dy);
    }

	/**
	 * Возвращает квадрат длины вектора V
	 */
	public static float len2(Vector2 v)
	{
		return v.x * v.x + v.y * v.y;
	}

	/**
	 * Возвращает квадрат длины вектора V
	 */
	public static float len2(float vx, float vy)
	{
		return vx * vx + vy * vy;
	}

	/**
	 * Возвращает квадрат длины вектора V
	 */
	public static float len2(Vector3 v)
	{
		return v.x * v.x + v.y * v.y + v.z * v.z;
	}

	/**
	 * Возвращает квадрат длины вектора AB
	 */
	public static float len2(Vector2 a, Vector2 b)
	{
		float dx = b.x - a.x;
		float dy = b.y - a.y;
		return dx * dx + dy * dy;
	}

	/**
	 * Возвращает квадрат длины вектора AB
	 */
	public static float len2(Vector2 a, float bx, float by)
	{
		float dx = bx - a.x;
		float dy = by - a.y;
		return dx * dx + dy * dy;
	}

	/**
	 * Возвращает квадрат длины вектора AB
	 */
	public static float len2(float ax, float ay, float bx, float by)
	{
		float dx = bx - ax;
		float dy = by - ay;
		return (dx * dx) + (dy * dy);
	}

	/**
	 * Возвращает скалярное произведение векторов A * B
	 */
	public static float dotProd(Vector2 a, Vector2 b)
	{
		return a.x * b.x + a.y * b.y;
	}

	/**
	 * Возвращает скалярное произведение векторов A * B
	 */
	public static float dotProd(float ax, float ay, float bx, float by)
	{
		return ax * bx + ay * by;
	}

	/**
	 * Возвращает скалярное произведение векторов AB * AC
	 */
	public static float dotProd(Vector2 a, Vector2 b, Vector2 c)
	{
		return (b.x - a.x) * (c.x - a.x) + (b.y - a.y) * (c.y - a.y);
	}

	/**
	 * Возвращает скалярное произведение векторов AB * AC
	 */
	public static float dotProd(float ax, float ay, float bx, float by, float cx, float cy)
	{
		return (bx - ax) * (cx - ax) + (by - ay) * (cy - ay);
	}

	/**
	 * Возвращает псевдоскалярное произведение векторов A * B
	 */
	public static float crossProd(Vector2 a, Vector2 b)
	{
		return a.x * b.y - a.y * b.x; 
	}

	/**
	 * Возвращает псевдоскалярное произведение векторов A * B
	 */
	public static float crossProd(Vector2 a, float bx, float by)
	{
		return a.x * by - a.y * bx;
	}

	/**
	 * Возвращает псевдоскалярное произведение векторов AB * AC
	 */
	public static float crossProd(Vector2 a, Vector2 b, Vector2 c)
	{
		return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y);
	}

	/**
	 * Возвращает псевдоскалярное произведение векторов AB * AC
	 */
	public static float crossProd(float ax, float ay, float bx, float by, float cx, float cy)
	{
		return (bx - ax) * (cy - ay) - (cx - ax) * (by - ay);
	}

	/**
	 * Возвращает псевдоскалярное произведение векторов AB * CD
	 */
	public static float crossProd(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
	{
		return (b.x - a.x) * (d.y - c.y) - (d.x - c.x) * (b.y - a.y);
	}

	/**
	 * Возвращает псевдоскалярное произведение векторов AB * CD
	 */
	public static float crossProd(float ax, float ay, float bx, float by, float cx, float cy, float dx, float dy)
	{
		return (bx - ax) * (dy - cy) - (dx - cx) * (by - ay);
	}

	/**
	 * Возвращает проекцию точки на вектор AB (косинус угла)
	 */
	public float proj(Vector2 a, Vector2 b, Vector2 p)
	{
		return dotProd(a, b, p) / len2(a, b);
	}

	/**
	 * Возвращает проекцию точки на вектор AB (косинус угла)
	 */
	public static float proj(float ax, float ay, float bx, float by, float px, float py)
	{
		return dotProd(ax, ay, bx, by, px, py) / len2(ax, ay, bx, by);
	}


	/**
	 * Возвращает позицию спроецированной точки на вектор AB. ВНИМАНИЕ!!! Возвращается объект из пула
	 */
	public static Vector2 projPos(Vector2 a, Vector2 b, Vector2 p)
	{
		float abx = b.x - a.x;
		float aby = b.y - a.y;
		float apx = p.x - a.x;
		float apy = p.y - a.y;
		float proj = dotProd(abx, aby, apx, apy) / len2(abx, aby);
		return Vector.getVector2().set(a.x + proj * abx, a.y + proj * aby);
	}

	/**
	 * Возвращает позицию спроецированной точки на вектор AB. ВНИМАНИЕ!!! Возвращается объект из пула
	 */
	public static Vector2 projPos(float ax, float ay, float bx, float by, float px, float py)
	{
		float abx = bx - ax;
		float aby = by - ay;
		float apx = px - ax;
		float apy = py - ay;
		float proj = dotProd(abx, aby, apx, apy) / len2(abx, aby);
		return Vector.getVector2().set(ax + proj * abx, ay + proj * aby);
	}

	/**
	 * Выполняет линейную интерполяцию между двумя точками
	 * @return Позиция интерполированной точки ВОЗВРАЩАЕТ ОБЪЕКТ ИЗ ПУЛА!!!
	 */
	public static Vector2 lerp(Vector2 a, Vector2 b, float k)
	{
		return Vector.getVector2().set(a.x + k * (b.x - a.x), a.y + k * (b.y - a.y));
	}

	/**
	 * Возвращает нормаль к заданному вектору (инвертирование координаты X)
	 * @param v Вектор для которого вычисляется нормаль
	 * @return Вектор нормали (не нормализованный) ВОЗВРАЩАЕТ ОБЪЕКТ ИЗ ПУЛА!!!
	 */
	public static Vector2 getNormalX(Vector2 v)
	{
		return Vector.getVector2().set(v.y, -v.x);
	}

	/**
	 * Возвращает нормаль к вектору AB (инвертирование координаты X)
	 * @param a
	 * @param b
	 * @return Вектор нормали (не нормализованный) ВОЗВРАЩАЕТ ОБЪЕКТ ИЗ ПУЛА!!!
	 */
	public static Vector2 getNormalX(Vector2 a, Vector2 b)
	{
		return Vector.getVector2().set(b.y - a.y, -(b.x - a.x));
	}

	/**
	 * Возвращает нормаль к заданному вектору (инвертирование координаты X)
	 * @param vx Координата X
	 * @param vy Координата Y
	 * @return Вектор нормали (не нормализованный) ВОЗВРАЩАЕТ ОБЪЕКТ ИЗ ПУЛА!!!
	 */
	public static Vector2 getNormalX(float vx, float vy)
	{
		return Vector.getVector2().set(vy, -vx);
	}

	/**
	 * Возвращает нормаль к заданному вектору (инвертирование координаты Y)
	 * @param v Вектор для которого вычисляется нормаль
	 * @return Вектор нормали (не нормализованный) ВОЗВРАЩАЕТ ОБЪЕКТ ИЗ ПУЛА!!!
	 */
	public static Vector2 getNormalY(Vector2 v)
	{
		return Vector.getVector2().set(-v.y, v.x);
	}

	/**
	 * Возвращает нормаль к вектору AB (инвертирование координаты Y)
	 * @param a
	 * @param b
	 * @return Вектор нормали (не нормализованный) ВОЗВРАЩАЕТ ОБЪЕКТ ИЗ ПУЛА!!!
	 */
	public static Vector2 getNormalY(Vector2 a, Vector2 b)
	{
		return Vector.getVector2().set(a.y - b.y, b.x - a.x);
	}

	/**
	 * Возвращает нормаль к заданному вектору (инвертирование координаты Y)
	 * @param vx Координата X
	 * @param vy Координата Y
	 * @return Вектор нормали (не нормализованный) ВОЗВРАЩАЕТ ОБЪЕКТ ИЗ ПУЛА!!!
	 */
	public static Vector2 getNormalY(float vx, float vy)
	{
		return Vector.getVector2().set(-vy, vx);
	}

	/**
	 * Нормализует переданный вектор и возвращает его
	 * @param v Нормализуемый вектор
	 * @return Нормализованный вектор
	 */
	public static Vector2 normalize(Vector2 v)
	{
		float invLen = MathHelper.fastInvSqrt(MathHelper.len2(v));
		v.x = v.x * invLen;
		v.y = v.y * invLen;
		return v;
	}

	/**
	 * Нормализует переданный вектор и возвращает его
	 * @param v Нормализуемый вектор
	 * @return Нормализованный вектор
	 */
	public static Vector3 normalize(Vector3 v)
	{
		float invLen = MathHelper.fastInvSqrt(MathHelper.len2(v));
		v.x = v.x * invLen;
		v.y = v.y * invLen;
		v.z = v.z * invLen;
		return v;
	}

	/**
	 * Вычисляет обратный корень от числа, используя хитрые манипуляции и немного магии.
	 * По результатам тестов ~50% быстрее чем вычисление "в лоб"
	 */
	public static float fastInvSqrt(float x) 
	{
        float half = 0.5f * x;
        int temp = Float.floatToRawIntBits(x);
        temp = 0x5F3759DF - (temp >> 1);
        float result = Float.intBitsToFloat(temp);
        return result * (1.5f - half * result * result);
    }

	/**
	 * Конвертирует радианы в градусы
	 */
	public static float toDegrees(float radians)
    {
        final float degreesRate = (180f / Pi);
        return radians * degreesRate;
    }

	/**
	 * Конвертирует градусы в радианы
	 * @param degrees
	 * @return
	 */
    public static float toRadians(float degrees)
    {
        final float radiansRate = (Pi / 180f);
        return degrees * radiansRate;
    }

	/**
	 * Определяет пересекаются ли указанный круг и отрезок AB
	 * @param a первая точка отрезка
	 * @param b вторая точка отрезка
	 * @param center центральная точка круга
	 * @param radius радиус
	 * @return Возвращает true если пересекаются, false в противном случае
	 */
	public boolean overlapLineAndCircle(Vector2 a, Vector2 b, Vector2 center, float radius)
	{
		// Находим проекцию центра круга на линию (косинус угла)
		float centerProj =  proj(a, b, center);

		// Если точки a и b совпадают получится NaN, просто проверяем расстояние до одной из точек
		if(Float.isNaN(centerProj))
		{
			return len2(a, center) <= radius * radius;
		}

		// Находим координаты спроецированной точки и расстояние от нее до центра круга
		float pcx = a.x + centerProj * (b.x - a.x);
		float pcy = a.y + centerProj * (b.y - a.y);
		float dist2ToLine = len2(center, pcx, pcy);

		// Если дистанция до спроецированной точки больше чем радиус круга то они не пересекаются
		if(dist2ToLine > radius * radius) return false;
		// Если дистанция меньше и проекция центра круга попадает в границы отрезка значит пересечение есть
		if(centerProj > 0f && centerProj < 1f) return true;

		// Если попали сюда значит дистанция меньше т.е. пересечение с линией есть
		// но не факт что есть пересечение с отрезком, нужно проверить одну из граничных точек
		// Проверяем ближайшую к кругу точку отрезка на пересечение с кругом
		return len2(centerProj < 0.5f ? a : b, center) <= radius * radius;
	}
}
