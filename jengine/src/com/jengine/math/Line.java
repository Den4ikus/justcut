package com.jengine.math;

import com.badlogic.gdx.math.Vector2;
import com.jengine.common.IFactory;
import com.jengine.common.Pool;

//TODO: перейти на использование методов MathHelper

/**
 * Класс для инкапсуляции линий в вычислениях.
 * Для получения экземпляра рекомендуется использовать Line.get().
 * Для возвращения экземпляра в пул Line.put().
 */
public final class Line
{
	// Это интересно!))
	/*1. Уравнение прямой в общем виде (ax + by + c = 0) можно записать как вектор (a, b, c)
	Тогда чтобы найти точку пересечения двух таких прямых — достаточно взять векторное произведение: Cross( (a1, b1, c1), (a2, b2, c2) ). Мы получим однородные координат в которых лежит наша точка пересечения.
	2. У нас есть две точки: (x1, y1) и (x2, y2). Чтобы найти общее уравнение прямой, проходящей через эти точки достаточно взять векторное произведение наших точек в однородных координатах: Cross( (x1, y1, 1), (x2, y2, 1) )*/

	//region LinePool

	private static final Pool<Line> LinePool = new Pool<Line>(new IFactory<Line>()
	{
		public Line create()
		{
			return new Line();
		}
	}, 1024);

	public static Line get()
	{
		return LinePool.get();
	}

	public static void put(Line line)
	{
		LinePool.put(line);
	}

	//endregion

	//region Trigonometry

	/**
	 * Определяет пересекается ли текущий отрезок, отрезком AB
	 * @param a
	 * @param b
	 * @return Возвращает точку пересечения отрезков или null если отрезки не пересекаются. ВНИМАНИЕ!!! Возвращается объект из пула
	 */
	public Vector2 crossPoint(Vector2 a, Vector2 b)
	{
		// Проверяем отрезки на параллельность
		float delimiter = MathHelper.crossProd(v1, v2, a, b);
		if(MathHelper.equalZero(delimiter)) return null;

		// Проверяем что точка пересечения лежит в границах отрезков
		float k1 = MathHelper.crossProd(a, b, a, v1) / delimiter;
		float k2 = MathHelper.crossProd(v1, v2, a, v1) / delimiter;
		return k1 > 0f && k1 < 1f && k2 > 0f && k2 < 1f ? MathHelper.lerp(v1,v2,k1) : null;
	}

	//endregion

	public final Vector2 v1 = new Vector2();
	public final Vector2 v2 = new Vector2();

	public Line()
	{
	}

	public void set(Line line)
	{
		this.v1.set(line.v1);
		this.v2.set(line.v2);
	}

	public void set(Vector2 v1, Vector2 v2)
	{
		this.v1.set(v1);
		this.v2.set(v2);
	}

	public void set(float x1, float y1, float x2, float y2)
	{
		this.v1.set(x1, y1);
		this.v2.set(x2, y2);
	}
}
