package com.jengine.sound;

/**
 *
 */
public interface IAudioInstance
{
    void stop();
    void pause();
    void resume();
}
