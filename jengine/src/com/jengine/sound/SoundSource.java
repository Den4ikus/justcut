package com.jengine.sound;

import com.badlogic.gdx.audio.Sound;

/**
 *
 */
public final class SoundSource implements IAudioSource
{
    private Sound _sound;

    public SoundSource(Sound sound)
    {
        _sound = sound;
    }

    @Override
    public IAudioInstance play()
    {
        long id = _sound.play();
        return new SoundInstance(_sound, id);
    }
}
