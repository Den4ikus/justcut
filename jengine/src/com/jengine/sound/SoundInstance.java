package com.jengine.sound;

import com.badlogic.gdx.audio.Sound;

/**
 *
 */
public final class SoundInstance implements IAudioInstance
{
    private Sound _sound;
    private long _id;

    public SoundInstance(Sound sound, long id)
    {
        _sound = sound;
        _id = id;
    }

    @Override
    public void stop()
    {
        _sound.stop(_id);
    }

    @Override
    public void pause()
    {
        _sound.pause(_id);
    }

    @Override
    public void resume()
    {
        _sound.resume(_id);
    }
}
