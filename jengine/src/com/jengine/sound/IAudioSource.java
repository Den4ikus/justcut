package com.jengine.sound;

/**
 *
 */
public interface IAudioSource
{
    IAudioInstance play();
}