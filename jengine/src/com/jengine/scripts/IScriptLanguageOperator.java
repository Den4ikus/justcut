package com.jengine.scripts;

/**
 * Created by Денис on 15.04.2015.
 */

/**
 * Интерфейс оператора скрипта
 */
public interface IScriptLanguageOperator extends IScript<String>
{
    /**
     * Возвращает название оператора
     */
    String getName();
}