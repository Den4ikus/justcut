package com.jengine.scripts;

import java.util.HashMap;

/**
 * Created by Денис on 15.04.2015.
 * Description:
 */
public final class ScriptLanguageInterpretator
{
    private static HashMap<String, IScriptLanguageOperator> operators = new HashMap<String, IScriptLanguageOperator>();

    static
    {
        //operators.put();
    }

    private ScriptLanguageInterpretator()
    {}

    public static void execute(String scriptLine) throws ScriptLanguageException
    {
        // разделяем название оператора и аргументы
        String[] operatorParts = scriptLine.split(" ");
        String[] args = operatorParts[1].split(",");
        // ищем оператор и выполняем с указанными аргументами
        IScriptLanguageOperator operator = operators.get(operatorParts[0]);
        if (operator != null)
        {
            operator.execute(args);
        }
        else throw new ScriptLanguageException(String.format("Неизвестный оператор скрипта: %s", operatorParts[0]));
    }
}
