package com.jengine.scripts;
/**
 * Created by Денис on 13.06.2015.
 *
 */

import com.jengine.common.logs.Logger;

import java.util.ArrayList;

/**
 *
 */
public final class ScriptSource implements IScript<Void>
{
    private ArrayList<String> lines;

    public ArrayList<String> getLines()
    {
        return this.lines;
    }

    private ScriptSource()
    {}

    public ScriptSource(ArrayList<String> lines)
    {
        this.lines = lines;
    }

    //region IScript<Void>

    @Override
    public void execute(Void... args)
    {
        for(String line : lines)
        {
            try
            {
                ScriptLanguageInterpretator.execute(line);
            }
            catch (ScriptLanguageException ex)
            {
                Logger.error(String.format("Ошибка выполнения скрипта: %s (%s)", line, ex.getMessage()));
            }
        }
    }

    //endregion
}