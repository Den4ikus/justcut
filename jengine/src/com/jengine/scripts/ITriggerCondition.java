package com.jengine.scripts;

/**
 * Created by Денис on 10.06.2015.
 * Условие срабатывания триггера
 */
public interface ITriggerCondition
{
    /**
     * Условие которое всегда выполняется
     */
    public static final ITriggerCondition alwaysTrue = new ITriggerCondition()
    {
        @Override
        public boolean check()
        {
            return true;
        }
    };

    /**
     * Условие которое никогда не выполняется
     */
    public static final ITriggerCondition alwaysFalse = new ITriggerCondition()
    {
        @Override
        public boolean check()
        {
            return false;
        }
    };

    /**
     * Возвращает значение выполняется ли условие срабатывания
     */
    boolean check();
}
