package com.jengine.scripts;

/**
 * Created by Денис on 15.04.2015.
 */

/**
 * Исключение генерируется при ошибке выполнения скрпита
 */
public class ScriptLanguageException extends Exception
{
    /**
     *
     * @param message текст ошибки
     */
    public ScriptLanguageException(String message)
    {
        super(message);
    }
}
