package com.jengine.scripts;

/**
 * Created by Денис on 24.03.2015.
 */
public interface Delegate<TRes, T1, T2>
{
    TRes Invoke(T1 param1, T2 param2);
}