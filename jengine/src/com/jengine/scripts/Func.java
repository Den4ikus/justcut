package com.jengine.scripts;

/**
 * Created by Денис on 24.03.2015.
 */
public abstract class Func<TRes> implements Delegate<TRes, Void, Void>
{
    public TRes Invoke() { return this.Invoke(null, null); }
}
