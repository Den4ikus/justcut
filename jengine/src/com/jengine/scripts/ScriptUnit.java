package com.jengine.scripts;

/**
 * Created by Денис on 11.07.2015.
 * Представляет независимую комбинацию скрипта и провайдера параметров скрипта
 * @param <T> тип параметров скрипта
 */
public final class ScriptUnit<T>
{
    private IScript<T> script;
    private IScriptArgsProvider<T> argsProvider;

    public ScriptUnit(IScript<T> script, IScriptArgsProvider<T> argsProvider)
    {
        this.script = script;
        this.argsProvider = argsProvider;
    }

    public void execute()
    {
        script.execute(argsProvider.getArgs());
    }
}
