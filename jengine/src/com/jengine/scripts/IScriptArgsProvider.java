package com.jengine.scripts;

/**
 * Created by Денис on 10.06.2015.
 * Провайдер аргументов для скрипта
 * @param <T> тип аргументов
 */
public interface IScriptArgsProvider<T>
{
    public static final IScriptArgsProvider empty = new IScriptArgsProvider()
    {
        private Object[] args = new Object[0];
        @Override
        public Object[] getArgs()
        {
            return args;
        }
    };

    /**
     * Возвращает аргументы для скрипта
     */
    T[] getArgs();
}