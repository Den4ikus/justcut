package com.jengine.scripts;

/**
 * Created by Денис on 24.03.2015.
 */
public abstract class Func1<TRes, T> implements Delegate<TRes, T, Void>
{
    public TRes Invoke(T param) { return this.Invoke(param, null); }
}
