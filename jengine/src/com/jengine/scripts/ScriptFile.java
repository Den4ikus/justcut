package com.jengine.scripts;
/**
 * Created by Денис on 13.06.2015.
 *
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.jengine.Const;
import com.jengine.common.logs.Logger;
import com.jengine.resources.IFileHandleResolverProvider;

import java.util.ArrayList;

/**
 *
 */
public final class ScriptFile implements IScript<Void>, IFileHandleResolverProvider
{
    private ArrayList<String> lines;

    public ArrayList<String> getLines()
    {
        return this.lines;
    }

    private ScriptFile()
    {}

    public ScriptFile(ArrayList<String> lines)
    {
        this.lines = lines;
    }

    //region IScript<Void>

    @Override
    public void execute(Void... args)
    {
        for(String line : lines)
        {
            try
            {
                ScriptLanguageInterpretator.execute(line);
            }
            catch (ScriptLanguageException ex)
            {
                Logger.error(String.format("Ошибка выполнения скрипта: %s (%s)", line, ex.getMessage()));
            }
        }
    }

    //endregion

    //region IFileHandleResolverProvider

    private static final FileHandleResolver resolver = new FileHandleResolver() {
        @Override
        public FileHandle resolve(String fileName)
        {
            return Gdx.files.internal(Const.RESOURCE_SCRIPTS_DIRECTORY + fileName);
        }
    };

    public FileHandleResolver getResolver()
    {
        return this.resolver;
    }

    //endregion
}