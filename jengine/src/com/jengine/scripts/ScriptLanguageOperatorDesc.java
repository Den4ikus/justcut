package com.jengine.scripts;

/**
 * Created by Денис on 15.04.2015.
 */

/**
 * Класс описывающий оператор скрипта
 */
public final class ScriptLanguageOperatorDesc
{
    private String name;
    private IScriptLanguageOperator operator;

    /**
     * Возвращает имя оператора
     * @return имя оператора
     */
    public String getName() { return name; }

    /**
     * Возвращает оператор скрипта
     * @return оператор скрипта
     */
    public IScriptLanguageOperator getOperator() { return operator; }

    public ScriptLanguageOperatorDesc(String name, IScriptLanguageOperator operator)
    {
        this.name = name;
        this.operator = operator;
    }
}
