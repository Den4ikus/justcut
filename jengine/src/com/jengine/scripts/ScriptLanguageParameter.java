package com.jengine.scripts;

import com.badlogic.gdx.graphics.Color;
import com.jengine.ColorHelper;

/**
 * Created by Денис on 15.04.2015.
 * Description:
 */
public final class ScriptLanguageParameter
{
    private ScriptLanguageParameter() {}

    public static float toFloat(String value)
    {
        return Float.parseFloat(value);
    }

    public static double toDouble(String value)
    {
        return Double.parseDouble(value);
    }

    public static Color toColor(String value)
    {
        return ColorHelper.byName(value);
    }
}
