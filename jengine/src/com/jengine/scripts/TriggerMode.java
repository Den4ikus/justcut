package com.jengine.scripts;

/**
 * Created by Денис on 10.06.2015.
 * Режим срабатывания триггера
 */
public enum TriggerMode
{
    /**
     * Триггер срабатывает один раз, только при выполнении условий срабатывания
     */
    ExecuteOnce,

    /**
     * Триггер срабатывает при изменении условий срабатывания в обе стороны
     */
    ExecuteSwitch,

    /**
     * Триггер срабатывает постоянно пока выполняются условия срабатывания
     */
    ExecuteAlways
}
