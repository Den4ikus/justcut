package com.jengine.scripts;

import com.jengine.collections.linq.IPredicate;
import com.jengine.collections.linq.Linq;
import com.jengine.game.IGameTime;

/**
 * Created by Денис on 11.07.2015.
 * Триггер, выполняет скрипты при возникновении определенных условий
 */
public class Trigger
{
    protected ITriggerCondition[] conditions;
    protected ScriptUnit[] scripts;

    protected TriggerMode mode;
    public TriggerMode getMode()
    {
        return mode;
    }

    protected boolean state = false;
    public boolean getState()
    {
        return state;
    }

    public Trigger(TriggerMode mode, ITriggerCondition[] conditions, ScriptUnit[] scripts)
    {
        this.mode = mode;
        this.conditions = conditions;
        this.scripts = scripts;
    }

    public void reset()
    {
        state = false;
    }


    public void update(IGameTime gameTime)
    {
        switch(mode)
        {
            case ExecuteOnce:
                // если триггер уже сработал выходим
                if(state == true) return;
                // если условия выполняются запускаем скрипты
                if(checkConditions()) executeScripts();
                break;

            case ExecuteSwitch:
                boolean newState = checkConditions();
                // если триггер перешел в другое состояние запускаем скрипты
                if(newState != state)
                {
                    state = newState;
                    executeScripts();
                }
                break;

            case ExecuteAlways:
                if(checkConditions()) executeScripts();
                break;
        }
    }

    private boolean checkConditions()
    {
        return Linq.all(conditions, new IPredicate<ITriggerCondition>()
        {
            @Override
            public boolean evaluate(ITriggerCondition arg)
            {
                return arg.check();
            }
        });
    }

    private void executeScripts()
    {
        for(ScriptUnit script : scripts)
            script.execute();
    }
}
