package com.jengine.scripts;

/**
 * Created by Денис on 25.03.2015.
 */

public interface IScript<T>
{
    void execute(T ... args);
}