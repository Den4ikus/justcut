package com.jengine.viewports;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

/**
 * Представляет область окна, в которую выполняется рендеринг
 */
public final class Viewport
{
	private static Vector2 temp = new Vector2();
	
    protected int screenX;
    protected int screenY;
    protected int screenWidth;
    protected int screenHeight;

    public int getScreenX()
    {
        return screenX;
    }

    public int getScreenY() { return screenY; }

    public void setScreenX(int x)
    {
        this.screenX = x;
    }

    public void setScreenY(int y)
    {
        this.screenY = y;
    }

    public void setScreenOffset(int x, int y)
    {
        this.screenX = x;
        this.screenY = y;
    }

    public int getScreenWidth()
    {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth)
    {
        this.screenWidth = screenWidth;
    }

    public int getScreenHeight()
    {
        return screenHeight;
    }

    public final void setScreenHeight(int screenHeight)
    {
        this.screenHeight = screenHeight;
    }

    public final void setScreenSize(int screenWidth, int screenHeight)
    {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

	public Viewport()
	{}
	
    public Viewport(int screenX, int screenY, int screenWidth, int screenHeight)
    {
        this.screenX = screenX;
        this.screenY = screenY;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

	public boolean contains(Vector2 viewportPoint)
	{
		return viewportPoint.x >= screenX &&
               viewportPoint.y >= screenY &&
               viewportPoint.x <= screenX + screenWidth &&
               viewportPoint.y <= screenY + screenHeight;
	}
	
	public Vector2 toViewport(Vector2 windowPoint)
	{
		temp.set(windowPoint.x, Gdx.graphics.getHeight() - windowPoint.y);
		return temp;
	}
	
	public Vector2 toWindow(Vector2 viewportPoint)
	{
		temp.set(viewportPoint.x, Gdx.graphics.getHeight() - viewportPoint.y);
		return temp;
	}
	
    /**
     * Возвращает ширину области видимости, по заданной высоте, с учетом сохранения соотношения сторон
     * @param expectedHeight Высота области видимости
     * @return Ширина области видимости которую нужно установить
     */
    public float getExpectedWidth(float expectedHeight)
    {
        return (expectedHeight / screenHeight) * screenWidth;
    }

    /**
     * Возвращает высоту области видимости, по заданной ширине, с учетом сохранения соотношения сторон
     * @param expectedWidth Ширина области видимости
     * @return Высота области видимости которую нужно установить
     */
    public float getExpectedHeight(float expectedWidth)
    {
        return (expectedWidth / screenWidth) * screenHeight;
    }

    /**
     * Применяет текущие параметры области вывода для рендеринга
     */
    public final void apply()
    {
        Gdx.gl.glViewport(screenX, screenY, screenWidth, screenHeight);
    }
}
