package com.justcut.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.justcut.JustCutGame;

public class DesktopLauncher
{
    public static void main(String[] arg)
    {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration()
			{{
                width = 960;
                height = 540;
                backgroundFPS = 60;
                foregroundFPS = 60;
                vSyncEnabled = true;
            }};
        
		// TODO нужно грузить конфиг из файла, типа настройка языка и т.п.
        JustCutGame game = new JustCutGame();
        new LwjglApplication(game, config);
    }
}
