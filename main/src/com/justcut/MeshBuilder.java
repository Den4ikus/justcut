package com.justcut;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.jengine.graphics.vertices.VertexPositionNormal;
import com.jengine.math.MathHelper;
import com.jengine.math.Vector;

/**
 *
 */
public final class MeshBuilder
{
    private static final Vector3 v1 = new Vector3();
    private static final Vector3 v2 = new Vector3();
    private static final Vector3 v3 = new Vector3();
    private static final Vector3 a = new Vector3();
    private static final Vector3 b = new Vector3();

    private static final float MeshDepth = -0.33f;

    private MeshBuilder()
    {}

    /**
     * Генерирует меш многоугольника и его боковые грани
     * @param poly Вектора описывающие полигон (по часовой стрелке)
     */
    public static VertexPositionNormal[] createFromPoly(Vector2[] poly)
    {
        int polyVertexCount = poly.length;
        int polyTriangleCount = polyVertexCount - 2;
        int polySideTriangleCount = polyVertexCount * 2;
        int totalTriangleCount = polyTriangleCount + polySideTriangleCount;
        VertexPositionNormal[] mesh = new VertexPositionNormal[totalTriangleCount * 3];
        int meshIndex = 0;

        // генерируем лицевую грань
        a.set(Vector.Backward); // нормаль уже известна

		final int firstForwardIndex = 0;
		final int firstBackwardIndex = polyVertexCount - 1;
		int forwardIndex = firstForwardIndex;
		int backwardIndex = firstBackwardIndex;
		
		while(meshIndex < polyTriangleCount * 3)
		{
			// определяем в какую сторону двигаться по контуру
			if(forwardIndex - firstForwardIndex <= firstBackwardIndex - backwardIndex)
			{
                // двигаемся вперед
                v1.set(poly[forwardIndex++], 0f);
                v2.set(poly[forwardIndex], 0f);
                v3.set(poly[backwardIndex], 0f);
			}
			else
			{
				// двигаемся назад
                v1.set(poly[backwardIndex--], 0f);
                v2.set(poly[forwardIndex], 0f);
                v3.set(poly[backwardIndex], 0f);
			}

            mesh[meshIndex++] = new VertexPositionNormal(v1, a);
            mesh[meshIndex++] = new VertexPositionNormal(v2, a);
            mesh[meshIndex++] = new VertexPositionNormal(v3, a);
		}
		
        // генерируем боковые грани
        for(int i = 1; i < polyVertexCount; i++)
        {
            // формируем пару треугольников описывающих грань
            // по пути приходится сохранять точки т.к. нужно потом рассчитать нормаль
            v1.set(poly[i], 0f);
            v2.set(poly[i - 1], 0f);
            v3.set(poly[i], MeshDepth);


            // рассчитываем нормаль
            a.set(v3).sub(v1); // вектор А
            b.set(v2).sub(v1); // вектор В
            a.crs(b);
            MathHelper.normalize(a);

            // один треуольник
            mesh[meshIndex++] = new VertexPositionNormal(v1, a);
            mesh[meshIndex++] = new VertexPositionNormal(v2, a);
            mesh[meshIndex++] = new VertexPositionNormal(v3, a);

            v1.set(poly[i - 1], 0f);
            v2.set(poly[i - 1], MeshDepth);
            v3.set(poly[i], MeshDepth);

            // нормаль второго треугольника совпадает с первым т.к. они находятся в одной плоскости
            mesh[meshIndex++] = new VertexPositionNormal(v1, a);
            mesh[meshIndex++] = new VertexPositionNormal(v2, a);
            mesh[meshIndex++] = new VertexPositionNormal(v3, a);
        }

        // осталась последняя грань
        v1.set(poly[polyVertexCount - 1], 0f);
        v2.set(poly[0], MeshDepth);
        v3.set(poly[0], 0f);

        // рассчитываем нормаль
        a.set(v3).sub(v1); // вектор А
        b.set(v2).sub(v1); // вектор В
        a.crs(b);
        MathHelper.normalize(a);

        mesh[meshIndex++] = new VertexPositionNormal(v1, a);
        mesh[meshIndex++] = new VertexPositionNormal(v2, a);
        mesh[meshIndex++] = new VertexPositionNormal(v3, a);

        v1.set(poly[polyVertexCount - 1], 0f);
        v2.set(poly[polyVertexCount - 1], MeshDepth);
        v3.set(poly[0], MeshDepth);

        mesh[meshIndex++] = new VertexPositionNormal(v1, a);
        mesh[meshIndex++] = new VertexPositionNormal(v2, a);
        mesh[meshIndex] = new VertexPositionNormal(v3, a);

        return mesh;
    }
}
