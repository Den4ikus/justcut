package com.justcut.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.jengine.game.IGameTime;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.JustCutGame;

/**
 *
 */
public final class GameMenuScreen extends UIScreen
{
    //region Handlers

    private final ClickListener _resumeGameClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
            game.getScreenManager().removeScreen(GameMenuScreen.this);
        }
    };

    private final ClickListener _settingsClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
        }
    };

    private final ClickListener _restartGameClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
        }
    };

    private final ClickListener _mainMenuClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
            game.getScreenManager().removeScreen(GameMenuScreen.this);
            game.getScreenManager().removeScreen();
        }
    };

    //endregion

    //region UI

    private Image _background;
    private TextButton _resumeGameButton;
    private TextButton _restartGameButton;
    private TextButton _settingsGameButton;
    private TextButton _mainMenuButton;

    //endregion

    //region Resources

    private ResHandle<TextureAtlas> _texAtlas;

    private TextureRegion _blackTexture;

    //endregion

    public GameMenuScreen(JustCutGame game, ScreenOverlapMode overlapMode)
    {
        super(game, overlapMode);
    }

    @Override
    public void initialize()
    {
        super.initialize();

        _blackTexture = _texAtlas.get().findRegion("black");
        _background = new Image(_blackTexture);
        _background.setFillParent(true);
        stage.addActor(_background);
        _background.setColor(new Color(1f, 1f, 1f, 0.5f));

        Table table = new Table();
        table.center().pad(Value.percentWidth(0.30f));
        table.setFillParent(true);
        stage.addActor(table);

        //TODO: Debug
        //table.setDebug(true);

        // Start Game
        _resumeGameButton = new TextButton("Continue", skin.get(), "default_md");
        _resumeGameButton.addListener(_resumeGameClickEventHandler);
        table.add(_resumeGameButton).height(70f).pad(10f).expandX().fill();
        table.row();

        // Settings
        _restartGameButton = new TextButton("Restart", skin.get(), "default_md");
        _restartGameButton.addListener(_restartGameClickEventHandler);
        table.add(_restartGameButton).height(70f).pad(10f).expandX().fill();
        table.row();

        // About
        _settingsGameButton = new TextButton("Settings", skin.get(), "default_md");
        _settingsGameButton.addListener(_settingsClickEventHandler);
        table.add(_settingsGameButton).height(70f).pad(10f).expandX().fill();
        table.row();

        // Exit
        _mainMenuButton = new TextButton("Main menu", skin.get(), "default_md");
        _mainMenuButton.addListener(_mainMenuClickEventHandler);
        table.add(_mainMenuButton).height(70f).pad(10f).expandX().fill();
    }

    @Override
    public void load(ResourceManager resourceManager)
    {
        super.load(resourceManager);
        _texAtlas = resourceManager.get("GrayscaleTexAtlas", TextureAtlas.class);
    }

    @Override
    public void unload(ResourceManager resourceManager)
    {
        super.unload(resourceManager);
        resourceManager.free(_texAtlas);
    }

    @Override
    public void render(IGameTime gameTime)
    {
        // Экран отображается поверх игрового и т.к. последним шагом рендерится мини-карта въюпорт не подходит для рендеринга менюшки
        stage.getViewport().apply();
        super.render(gameTime);
    }
}
