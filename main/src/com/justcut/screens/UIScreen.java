package com.justcut.screens;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.jengine.game.BaseScreen;
import com.jengine.game.IGameTime;
import com.jengine.input.Input;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.JustCutGame;

/**
 * Базовый класс для экранов содержащих UI интерфейс
 */
public abstract class UIScreen extends BaseScreen<JustCutGame>
{
    protected final Stage stage = new Stage();
    protected ResHandle<Skin> skin;

    public UIScreen(JustCutGame game, ScreenOverlapMode overlapMode)
    {
        super(game, overlapMode);
    }

    @Override
    public void load(ResourceManager resourceManager)
    {
        super.load(resourceManager);
        skin = resourceManager.get("UISkin", Skin.class);
    }

    @Override
    public void unload(ResourceManager resourceManager)
    {
        super.unload(resourceManager);
        resourceManager.free(skin);
    }

    @Override
    public void activate()
    {
        super.activate();
        Input.addProcessor(stage);
    }

    @Override
    public void deactivate()
    {
        super.deactivate();
        Input.removeProcessor(stage);
    }

    @Override
    public void resize(int width, int height)
    {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void update(IGameTime gameTime)
    {
        super.update(gameTime);
        stage.act(gameTime.getElapsedTime());
    }

    @Override
    public void render(IGameTime gameTime)
    {
        super.render(gameTime);
        stage.draw();
    }

    @Override
    public void dispose()
    {
        if(stage != null)
            stage.dispose();
    }
}
