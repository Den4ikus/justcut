package com.justcut.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.jengine.ColorHelper;
import com.jengine.game.BaseScreen;
import com.jengine.game.IGameTime;
import com.jengine.math.RandomHelper;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.JustCutGame;

/**
 * Экран главного меню
 */
public final class MainMenuScreen extends FadeScreen
{
    private BaseScreen _nextScreen;

    //region UI

    private Image _coloredBackroundImage;
    private Image _textBackroundImage;
    private TextButton _startGameButton;
    private TextButton _settingsButton;
    private TextButton _aboutButton;
    private TextButton _exitButton;

    //endregion

    //region Handlers

    private final ClickListener _startGameClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);

            ChooseLevelScreen chooseLevelScreen = new ChooseLevelScreen(game, ScreenOverlapMode.Overlay);
            //GameWinScreen winScreen = new GameWinScreen(game, ScreenOverlapMode.UpdateOverlayRenderTransparent);

            //TODO нужно сделать экран выбора уровня
            //GameLvlScene gameLvl = new GameLvlScene("TestLevel", new BoundingBox2D(Vector2.Zero, 20f, 20f));
            performChangeScreen(chooseLevelScreen);
        }
    };

    private final ClickListener _settingsClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
        }
    };

    private final ClickListener _aboutClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
        }
    };

    private final ClickListener _exitClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
            Gdx.app.exit();
            // System.exit решение бага при котором не работает ввод после сворачивания или закрытия приложения
            System.exit(0);
        }
    };

    private ResHandle<Texture> _coloredBackground;
    private ResHandle<Texture> _textBackground;

    private final Color backgroundColor = new Color();

    //endregion

    public MainMenuScreen(JustCutGame game, ScreenOverlapMode overlapMode)
    {
        super(game, overlapMode, 0.5f, 0.5f);
    }

    @Override
    public void initialize()
    {
        super.initialize();

        // Включаем билинейную фильтрацию для текстур со шрифтом
        skin.get().getFont("font32").getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        skin.get().getFont("font48").getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        // Генерируем цвет фона
        float brightness = 0f;
        while(brightness < 0.6f)
        {
            RandomHelper.color(backgroundColor);
            brightness = ColorHelper.brightness(backgroundColor);
        }

        // Background
        _textBackroundImage = new Image(_textBackground.get());
        _textBackroundImage.setFillParent(true);
        stage.addActor(_textBackroundImage);

        _coloredBackroundImage = new Image(_coloredBackground.get());
        _coloredBackroundImage.setFillParent(true);

        Table table = new Table();
        table.right().bottom().pad(Value.zero, Value.percentWidth(0.65f), Value.percentWidth(0.05f), Value.percentWidth(0.05f));
        table.setBackground( _coloredBackroundImage.getDrawable());
        table.setColor(backgroundColor);
        table.setFillParent(true);
        stage.addActor(table);

        //TODO: Debug
        //table.setDebug(true);

        // Start Game
        _startGameButton = new TextButton("Start Game", skin.get(), "default_md");
        _startGameButton.addListener(_startGameClickEventHandler);
        table.add(_startGameButton).height(80f).pad(10f).expandX().fill();
        table.row();

        // Settings
        _settingsButton = new TextButton("Settings", skin.get(), "default_md");
        _settingsButton.addListener(_settingsClickEventHandler);
        table.add(_settingsButton).height(60f).pad(10f).expandX().fill();//.pad(10).expandX().fill();
        table.row();

        // About
        _aboutButton = new TextButton("About", skin.get(), "default_md");
        _aboutButton.addListener(_aboutClickEventHandler);
        table.add(_aboutButton).height(60f).pad(10f).expandX().fill();//.pad(10).expandX().fill();
        table.row();

        // Exit
        _exitButton = new TextButton("Exit", skin.get(), "default_md");
        _exitButton.addListener(_exitClickEventHandler);
        table.add(_exitButton).height(60f).pad(10f).expandX().fill();//.pad(10).expandX().fill();
    }

    @Override
    public void load(ResourceManager resourceManager)
    {
        super.load(resourceManager);
        _coloredBackground = resourceManager.get("ColoredMainMenuBackground", Texture.class);
        _textBackground = resourceManager.get("TextMainMenuBackground", Texture.class);
    }

    @Override
    public void unload(ResourceManager resourceManager)
    {
        super.unload(resourceManager);
        resourceManager.free(_coloredBackground);
        resourceManager.free(_textBackground);
    }

    @Override
    public void activate()
    {
        super.activate();
        performFadeOut();
    }

    /**
     * Метод реализующий рендеринг
     * @param gameTime класс представляющий игровое время
     */
    @Override
    public void render(IGameTime gameTime)
    {
        super.render(gameTime);
        //Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //super.render(gameTime);
    }

    @Override
    protected void onFadeIn()
    {
        LoadingScreen loadingScreen = new LoadingScreen(game, ScreenOverlapMode.Overlay, _nextScreen);
        game.getScreenManager().addScreen(loadingScreen);
    }

    @Override
    protected void onFadeOut()
    {

    }

    private void performChangeScreen(BaseScreen screen)
    {
        _nextScreen = screen;
        performFadeIn();
    }
}
