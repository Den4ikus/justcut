package com.justcut.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.jengine.game.IGameTime;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.JustCutGame;

/**
 *
 */
public final class GameWinScreen extends UIScreen
{
    //region Handlers

    private final ClickListener _resumeGameClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
            //game.getScreenManager().removeScreen(GameWinScreen.this);
            game.getScreenManager().removeScreen(GameWinScreen.this);
            game.getScreenManager().removeScreen();
        }
    };

    private final ClickListener _settingsClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
        }
    };

    private final ClickListener _restartGameClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
        }
    };

    private final ClickListener _mainMenuClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
            game.getScreenManager().removeScreen(GameWinScreen.this);
            game.getScreenManager().removeScreen();
        }
    };

    //endregion

    //region UI

    private Image _background;
    private TextButton _resumeGameButton;
    private TextButton _restartGameButton;
    private TextButton _settingsGameButton;
    private TextButton _mainMenuButton;

    //endregion

    //region Resources

    private ResHandle<TextureAtlas> _texAtlas;

    private TextureRegion _blackTexture;
    private ResHandle<Texture> _bronzeStar;
    private ResHandle<Texture> _silverStar;
    private ResHandle<Texture> _goldStar;

    //endregion

    public GameWinScreen(JustCutGame game, ScreenOverlapMode overlapMode)
    {
        super(game, overlapMode);
    }

    @Override
    public void initialize()
    {
        super.initialize();

        _blackTexture = _texAtlas.get().findRegion("black");
        _background = new Image(_blackTexture);
        _background.setFillParent(true);
        stage.addActor(_background);
        _background.setColor(new Color(1f, 1f, 1f, 0.5f));

        Table table = new Table();
        table.center();
        table.setFillParent(true);
        stage.addActor(table);

        //TODO: Debug
        //table.setDebug(true);

        // Start Game
        Button panelButton = new Button(skin.get(), "default");
        //panelButton.setFillParent(true);
        //Container panel = new Container(panelButton);

        _bronzeStar.get().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        _silverStar.get().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        _goldStar.get().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Image bronzeStar = new Image(_bronzeStar.get());
        Image silverStar = new Image(_silverStar.get());
        Image goldStar = new Image(_goldStar.get());

        HorizontalGroup row2 = new HorizontalGroup();
        Image siccors = new Image(_texAtlas.get().findRegion("siccors"));
        Label cutCountLabel = new Label("68 ", skin.get(), "black_label");
        row2.addActor(cutCountLabel);
        row2.addActor(siccors);

        Table tablePanel = new Table();
        tablePanel.add(bronzeStar).bottom();//.center().bottom();
        tablePanel.add(silverStar).bottom();
        tablePanel.add(goldStar).bottom();
        tablePanel.row();
        tablePanel.add(row2).colspan(3);

        panelButton.add(tablePanel).expand().fill();

        //stage.addActor(tablePanel);
        //panel.setFillParent(true);

        /*panel.prefWidth(Value.percentWidth(0.1f));
        panel.prefHeight(Value.percentHeight(0.1f));*/

        panelButton.addListener(_resumeGameClickEventHandler);
        table.add(panelButton).width(Value.percentWidth(0.5f, table)).height(Value.percentWidth(0.3f, table)).expand().fill();
        //.pad(20f).width(Value.percentWidth(0.8f)).height(Value.percentHeight(0.8f)).expand();
        //table.row();

        // Settings
        /*_restartGameButton = new TextButton("Restart", skin.get(), "default_md");
        _restartGameButton.addListener(_restartGameClickEventHandler);
        table.add(_restartGameButton).height(70f).pad(10f).expandX().fill();
        table.row();

        // About
        _settingsGameButton = new TextButton("Settings", skin.get(), "default_md");
        _settingsGameButton.addListener(_settingsClickEventHandler);
        table.add(_settingsGameButton).height(70f).pad(10f).expandX().fill();
        table.row();

        // Exit
        _mainMenuButton = new TextButton("Main menu", skin.get(), "default_md");
        _mainMenuButton.addListener(_mainMenuClickEventHandler);
        table.add(_mainMenuButton).height(70f).pad(10f).expandX().fill();*/
        //stage.setDebugAll(true);
    }

    @Override
    public void load(ResourceManager resourceManager)
    {
        super.load(resourceManager);
        _texAtlas = resourceManager.get("GrayscaleTexAtlas", TextureAtlas.class);
        _goldStar = resourceManager.get("GoldStar", Texture.class);
        _silverStar = resourceManager.get("SilverStar", Texture.class);
        _bronzeStar = resourceManager.get("BronzeStar", Texture.class);
    }

    @Override
    public void unload(ResourceManager resourceManager)
    {
        super.unload(resourceManager);
        resourceManager.free(_texAtlas);
    }

    @Override
    public void render(IGameTime gameTime)
    {
        // Экран отображается поверх игрового и т.к. последним шагом рендерится мини-карта въюпорт не подходит для рендеринга менюшки
        stage.getViewport().apply();
        super.render(gameTime);
    }
}
