package com.justcut.screens;

import android.view.KeyEvent;
import com.jengine.common.EventArgs;
import com.jengine.common.IEventHandler;
import com.jengine.game.SceneScreen;
import com.jengine.input.Input;
import com.jengine.input.InputKeyEventArgs;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.JustCutGame;
import com.justcut.graphics.views.CutLineView;
import com.justcut.graphics.views.GameLvlMiniMapView;
import com.justcut.graphics.views.GameShapeView;
import com.justcut.scene.GameLvlScene;


/**
 * Экран отображающий игровой уровень
 */
public class GameLvlScreen extends SceneScreen<JustCutGame, GameLvlScene>
{
	//TODO убрать обработчик для тестирования на ПК
	private IEventHandler<InputKeyEventArgs> _pcTest = new IEventHandler<InputKeyEventArgs>()
	{
		@Override
		public void doHandle(Object sender, InputKeyEventArgs args)
		{
			if(args.keyCode == KeyEvent.KEYCODE_TAB)
			{
				GameMenuScreen gameMenuScreen = new GameMenuScreen(game, ScreenOverlapMode.UpdateOverlayRenderTransparent);
				game.getScreenManager().addScreen(gameMenuScreen);
			}
		}
	};

	private IEventHandler<EventArgs> _touchBackMenuHandler = new IEventHandler<EventArgs>()
	{
		@Override
		public void doHandle(Object sender, EventArgs args)
		{
			GameMenuScreen gameMenuScreen = new GameMenuScreen(game, ScreenOverlapMode.UpdateOverlayRenderTransparent);
			game.getScreenManager().addScreen(gameMenuScreen);
		}
	};

    private IEventHandler<EventArgs> _winHandler = new IEventHandler<EventArgs>()
    {
        @Override
        public void doHandle(Object sender, EventArgs args)
        {
            GameWinScreen gameWinScreen = new GameWinScreen(game, ScreenOverlapMode.UpdateOverlayRenderTransparent);
            game.getScreenManager().addScreen(gameWinScreen);
        }
    };

    public GameLvlScreen(JustCutGame game, ScreenOverlapMode overlapMode, GameLvlScene scene)
    {
		super(game, overlapMode, scene,
                new GameShapeView(),
                /*new Box2DDebugView(),*/
                new CutLineView(),
                new GameLvlMiniMapView());

        scene.eventWin.attach(_winHandler);
    }

	/**
	 * Вызывается при активации экрана (активным считается текущий экран)
	 */
	@Override
	public void activate()
	{
		Input.eventKeyUp.attach(_pcTest);
		Input.eventKeyUpBack.attach(_touchBackMenuHandler);
		Input.eventKeyUpMenu.attach(_touchBackMenuHandler);
	}

	/**
	 * Вызывается при деактивации экрана (активным считается текущий экран)
	 */
	@Override
	public void deactivate()
	{
		Input.eventKeyUp.detach(_pcTest);
		Input.eventKeyUpBack.detach(_touchBackMenuHandler);
		Input.eventKeyUpMenu.detach(_touchBackMenuHandler);
	}

	/**
	 * Вызывается при активации окна приложения (получение фокуса ввода)
	 */
	@Override
	public void resume()
	{

	}

	/**
	 * Вызывается при деактивации окна приложения (потеря фокуса ввода)
	 */
	@Override
	public void suspend()
	{

	}
}
