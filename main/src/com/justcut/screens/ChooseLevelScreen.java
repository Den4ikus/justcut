package com.justcut.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.jengine.game.BaseScreen;
import com.jengine.game.IGameTime;
import com.jengine.math.bounds.BoundingBox2D;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.JustCutGame;
import com.justcut.resource.GameLevelComplete;
import com.justcut.resource.GameLevelDescription;
import com.justcut.scene.GameLvlScene;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Экран главного меню
 */
public final class ChooseLevelScreen extends FadeScreen
{
    private BaseScreen _nextScreen;

    private ArrayList<GameLevelDescription> _levels = new ArrayList<GameLevelDescription>()
        {{
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.Gold));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));

            //
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level1", "TestLevel", "Level1Preview", GameLevelComplete.None));
            add(new GameLevelDescription("Level2", "Level2", "Level2Preview", GameLevelComplete.None));

        }};

    //region UI

    private Image _coloredBackroundImage;
    private TextButton _startGameButton;
    private TextButton _settingsButton;
    private TextButton _aboutButton;
    private TextButton _exitButton;

    //endregion

    //region Handlers

    private final ClickListener _startGameClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
            game.getScreenManager().removeScreen();
        }
    };

    private final ClickListener _settingsClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
            GameLevelDescription levelDesc = (GameLevelDescription)event.getListenerActor().getUserObject();
            //TODO: границы уровня должны грузиться из SceneData
            GameLvlScene gameLvl = new GameLvlScene(levelDesc.sceneName, new BoundingBox2D(0f, 0f, 20f, 20f));
            GameLvlScreen gameLvlScreen = new GameLvlScreen(game, ScreenOverlapMode.Overlay, gameLvl);
            performChangeScreen(gameLvlScreen);
        }
    };

    private final ClickListener _aboutClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
        }
    };

    private final ClickListener _exitClickEventHandler = new ClickListener()
    {
        @Override
        public void clicked(InputEvent event, float x, float y)
        {
            super.clicked(event, x, y);
            Gdx.app.exit();
            // System.exit решение бага при котором не работает ввод после сворачивания или закрытия приложения
            System.exit(0);
        }
    };

    //endregion

    private ResHandle<Texture> _bronzeStar;
    private ResHandle<Texture> _silverStar;
    private ResHandle<Texture> _goldStar;


    private HashMap<String, ResHandle<Texture>> _previewImages = new HashMap<String, ResHandle<Texture>>();

    public ChooseLevelScreen(JustCutGame game, ScreenOverlapMode overlapMode)
    {
        super(game, overlapMode, 0.5f, 0.5f);
    }

    @Override
    public void initialize()
    {
        super.initialize();

        // Включаем билинейную фильтрацию для текстур со шрифтом
        skin.get().getFont("font32").getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        skin.get().getFont("font48").getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        _bronzeStar.get().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        _silverStar.get().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        _goldStar.get().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        // Background
        /*_textBackroundImage = new Image(_textBackground.get());
        _textBackroundImage.setFillParent(true);
        stage.addActor(_textBackroundImage);*/

        // Start Game
        _startGameButton = new TextButton("Main menu", skin.get(), "default_md");
        _startGameButton.addListener(_startGameClickEventHandler);

        // Settings
        _settingsButton = new TextButton("Settings--------------------", skin.get(), "default_md");
        _settingsButton.addListener(_settingsClickEventHandler);

        // About
        _aboutButton = new TextButton("About-----------------------", skin.get(), "default_md");
        _aboutButton.addListener(_aboutClickEventHandler);

        // Exit
        _exitButton = new TextButton("Exit--------------------------", skin.get(), "default_md");
        _exitButton.addListener(_exitClickEventHandler);

        Table table = new Table();
        ScrollPane scrollPane = new ScrollPane(table, skin.get(), "default");

        Table layoutTable = new Table();
        layoutTable.setFillParent(true);

        layoutTable.add(scrollPane).expand().fill();
        layoutTable.row();
        layoutTable.add(_startGameButton).right().height(80f).pad(10f);

        stage.addActor(layoutTable);

        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 6; j++)
            {
                GameLevelDescription level = _levels.get(j + i * 6);

                Image previewImage = new Image(_previewImages.get(level.name).get());

                Container previewImageContainer = new Container(previewImage);
                previewImageContainer.pad(10f);
                previewImageContainer.setFillParent(true);

                Button imageButton = new Button(skin.get(), "default");
                imageButton.setFillParent(true);
                imageButton.addActor(previewImageContainer);
                imageButton.setUserObject(level);
                imageButton.addListener(_settingsClickEventHandler);

                if(level.completeLevel.ordinal() >= GameLevelComplete.Bronze.ordinal())
                {
                    Image bronzeStar = new Image(_bronzeStar.get());
                    bronzeStar.setPosition(0, 0);
                    bronzeStar.setSize(40, 40);
                    imageButton.addActor(bronzeStar);
                }

                if(level.completeLevel.ordinal() >= GameLevelComplete.Silver.ordinal())
                {
                    Image silverStar = new Image(_silverStar.get());
                    silverStar.setPosition(40, 0);
                    silverStar.setSize(40, 40);
                    imageButton.addActor(silverStar);
                }

                if(level.completeLevel.ordinal() >= GameLevelComplete.Gold.ordinal())
                {
                    Image goldStar = new Image(_goldStar.get());
                    goldStar.setPosition(80, 0);
                    goldStar.setSize(40, 40);
                    imageButton.addActor(goldStar);
                }

                //imageButton.setWidth(500f);
                Container tileContainer = new Container(imageButton);
                tileContainer.prefWidth(Value.percentWidth(0.166f, layoutTable));
                tileContainer.prefHeight(Value.percentWidth(0.166f, layoutTable));
                //previewImageContainer.prefWidth(Value.percentWidth(0.8f, tileContainer));
                //previewImageContainer.prefHeight(Value.percentHeight(0.8f, tileContainer));


                //imageButton.background(previewImage.getDrawable());
                table.add(tileContainer).expand().fill();
            }
            table.row();
        }

        //TODO: Debug
        //stage.setDebugAll(true);
    }

    @Override
    public void load(ResourceManager resourceManager)
    {
        super.load(resourceManager);

        _goldStar = resourceManager.get("GoldStar", Texture.class);
        _silverStar = resourceManager.get("SilverStar", Texture.class);
        _bronzeStar = resourceManager.get("BronzeStar", Texture.class);

        for(GameLevelDescription level : _levels)
        {
            ResHandle<Texture> levelPreview = resourceManager.get(level.previewImageName, Texture.class);
            _previewImages.put(level.name, levelPreview);
        }
    }

    @Override
    public void activate()
    {
        super.activate();
        performFadeOut();
    }

    /**
     * Метод реализующий рендеринг
     * @param gameTime класс представляющий игровое время
     */
    @Override
    public void render(IGameTime gameTime)
    {
        //super.render(gameTime);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.render(gameTime);
    }

    @Override
    protected void onFadeIn()
    {
        LoadingScreen loadingScreen = new LoadingScreen(game, ScreenOverlapMode.Overlay, _nextScreen);
        game.getScreenManager().addScreen(loadingScreen);
    }

    @Override
    protected void onFadeOut()
    {

    }

    private void performChangeScreen(BaseScreen screen)
    {
        _nextScreen = screen;
        performFadeIn();
    }
}
