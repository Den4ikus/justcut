package com.justcut.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.jengine.game.BaseScreen;
import com.jengine.game.IGameTime;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.JustCutGame;

/**
 *
 */
public final class LoadingScreen extends UIScreen
{
    private BaseScreen _loadableScreen;

    private float _loadingTime = 0f;
    private float _loadingValueTime = 0f;
    private int _loadingValueIndex = 0;
    // TODO: нужны ли точки
    private String[] _loadingValues = new String[]
            {
                    "Loading   ",
                    "Loading.  ",
                    "Loading.. ",
                    "Loading..."
            };

    //region UI

    private Label _loadingLabel;
    private ProgressBar _loadingProgressBar;

    //endregion

    public LoadingScreen(JustCutGame game, ScreenOverlapMode overlapMode, BaseScreen loadableScreen)
    {
        super(game, overlapMode);
        _loadableScreen = loadableScreen;
    }

    /**
     * Вызывается для инициализации после загрузки ресурсов
     */
    @Override
    public void initialize()
    {
        super.initialize();

        Table table = new Table();
        table.bottom();
        table.pad(Value.percentWidth(0.05f));
        table.setFillParent(true);
        stage.addActor(table);

        //TODO: Debug
        //table.setDebug(true);

        // Loading...
        _loadingLabel = new Label(_loadingValues[0], skin.get(), "white_label");
        table.add(_loadingLabel);
        table.row();

        _loadingProgressBar = new ProgressBar(0f, 1f, 0.01f, false, skin.get(), "default");
        // Изменение размеров прогрессбара через setSize не работает
        /*_loadingProgressBar.getStyle().background.setMinHeight(50f);
        _loadingProgressBar.getStyle().knobBefore.setMinHeight(50f);*/
        _loadingProgressBar.setValue(0f);

        table.add(_loadingProgressBar).expandX().fill();

        _loadableScreen.load(game.getResourceManager());
    }

    /**
     * Метод в котором реализуется обновление игровой логики
     *
     * @param gameTime класс представляющий игровое время
     */
    @Override
    public void update(IGameTime gameTime)
    {
        super.update(gameTime);

        // анимация точек
        _loadingValueTime += gameTime.getElapsedTime();
        if(_loadingValueTime > 0.3f)
        {
            _loadingValueTime = 0f;
            _loadingValueIndex++;
            if(_loadingValueIndex >= _loadingValues.length) _loadingValueIndex = 0;
            _loadingLabel.setText(_loadingValues[_loadingValueIndex]);
        }

        _loadingTime += gameTime.getElapsedTime();
        float timeProgress = _loadingTime;
        float loadProgress = game.getResourceManager().getProgress();
        _loadingProgressBar.setValue(timeProgress > loadProgress ? loadProgress : timeProgress);

        //если загрузка закончилась закрываем экран
        if(game.getResourceManager().update() && _loadingTime > 0f)
        {
            _loadableScreen.initialize();
            game.getScreenManager().replaceScreen(this, _loadableScreen);
        }
    }

    /**
     * Метод в котором реализуется рендеринг
     *
     * @param gameTime класс представляющий игровое время
     */
    @Override
    public void render(IGameTime gameTime)
    {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.render(gameTime);
    }
}
