package com.justcut.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.jengine.game.IGameTime;
import com.jengine.graphics.renderers.ScreenSpaceTextureRenderer;
import com.jengine.graphics.shaders.ScreenSpaceTextureShader;
import com.jengine.math.MathHelper;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.JustCutGame;

/**
 * Экран реализующий затемнение при отображении
 */
public abstract class FadeScreen extends UIScreen
{
    private ScreenSpaceTextureRenderer _renderer;
    private Color _fadeColor = new Color(1f, 1f, 1f, 0f);
    private boolean _fadeIn = false;
    private float _fadeInTime;
    private boolean _fadeOut = false;
    private float _fadeOutTime;
    private float _elapsedTime = 0f;

    //region Resources

    private ResHandle<ShaderProgram> _shader;
    private ResHandle<TextureAtlas> _texAtlas;

    private TextureRegion _blackTexture;

    //endregion

    public FadeScreen(JustCutGame game, ScreenOverlapMode overlapMode, float fadeInTime, float fadeOutTime)
    {
        super(game, overlapMode);
        _fadeInTime = fadeInTime;
        _fadeOutTime = fadeOutTime;
    }

    @Override
    public void initialize()
    {
        super.initialize();
        _renderer = new ScreenSpaceTextureRenderer(new ScreenSpaceTextureShader(_shader.get()), 6);
        _blackTexture = _texAtlas.get().findRegion("black");
    }

    @Override
    public void load(ResourceManager resourceManager)
    {
        super.load(resourceManager);
        _shader = resourceManager.get("ScreenSpaceTextureShader", ShaderProgram.class);
        _texAtlas = resourceManager.get("GrayscaleTexAtlas", TextureAtlas.class);
    }

    @Override
    public void unload(ResourceManager resourceManager)
    {
        super.unload(resourceManager);
        resourceManager.free(_shader);
        resourceManager.free(_texAtlas);
    }

    @Override
    public void render(IGameTime gameTime)
    {
        super.render(gameTime);

        if(_fadeIn || _fadeOut)
        {
            _elapsedTime += gameTime.getElapsedTime();

            float fadeValue;
            if(_fadeIn) fadeValue = MathHelper.clamp(_elapsedTime / _fadeInTime);
            else fadeValue = MathHelper.clamp(1f - _elapsedTime / _fadeOutTime);

            Gdx.gl20.glClear(GL20.GL_DEPTH_BUFFER_BIT);
            Gdx.gl20.glEnable(GL20.GL_BLEND);

            _fadeColor.a = fadeValue;
            _renderer.begin();
            _renderer.draw(_blackTexture, 0f, 0f, width, height, _fadeColor);
            _renderer.end();

            if(_fadeIn && _elapsedTime >= _fadeInTime)
            {
                _fadeIn = false;
                onFadeIn();
            }
            if(_fadeOut && _elapsedTime >= _fadeOutTime)
            {
                _fadeOut = false;
                onFadeOut();
            }
        }
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_renderer != null)
            _renderer.dispose();
    }

    protected final void performFadeIn()
    {
        _fadeIn = true;
        _elapsedTime = 0f;
    }

    protected final void performFadeOut()
    {
        _fadeOut = true;
        _elapsedTime = 0f;
    }

    protected abstract void onFadeIn();
    protected abstract void onFadeOut();
}
