package com.justcut;

/**
 * Created by Денис on 09.06.2015.
 * Description:
 */

/**
 * Класс содержит константы для конфигурации движка игры
 */
public final class Const
{
    public static final String LOG_FILE_NAME = "log.txt";

    public static final String LOCALIZATION_DEFAULT = "russian.loc";
    public static final String LOCALIZATION_LANGUAGE = "Language";
    public static final String LOCALIZATION_MAINMENU_PLAY = "MainMenuPlay";
    public static final String LOCALIZATION_MAINMENU_QUIT = "MainMenuQuit";

    public static final String RESOURCE_LOCALIZATION_DIRECTORY = "localization\\";
    public static final String RESOURCE_SCRIPTS_DIRECTORY = "";
}
