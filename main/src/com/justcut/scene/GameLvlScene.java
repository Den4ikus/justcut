package com.justcut.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.jengine.cameras.OrthographicCamera;
import com.jengine.cameras.PerspectiveCamera;
import com.jengine.common.Event;
import com.jengine.common.EventArgs;
import com.jengine.common.IEvent;
import com.jengine.game.*;
import com.jengine.math.bounds.BoundingBox2D;
import com.jengine.physics.Physics2D;
import com.jengine.physics.RigidBody;
import com.jengine.viewports.Viewport;
import com.justcut.entities.CutLine;
import com.justcut.entities.GameShape;
import com.justcut.entities.controllers.CutLineController;
import com.justcut.entities.controllers.GameLvlCameraController;
import com.justcut.entities.controllers.TargetVolumeController;
import com.justcut.entities.factories.*;
import com.justcut.entities.types.*;
import com.justcut.scene.serialization.GameLvlSceneData;

import java.util.HashMap;

/**
 * Базовый класс для игровых уровней
 */
public final class GameLvlScene extends SpartialScene2D<GameLvlSceneData>
{
	private static final HashMap<String, IEntityFactory> factoryMap = new HashMap<String, IEntityFactory>()
		{{
            put(PhysicalPolyShapeType.class.getSimpleName(), new PhysicalPolyShapeFactory());
			put(PhysicalBoxShapeType.class.getSimpleName(), new BoxFactory());
			put(PhysicalLineShapeType.class.getSimpleName(), new EdgeFactory());
            put(PhysicalCircleShapeType.class.getSimpleName(), new CircleFactory());
            put(PolyShapeType.class.getSimpleName(), new PolyShapeFactory());
            put(TargetVolumeType.class.getSimpleName(), new PolyShapeFactory());
		}};

	public final OrthographicCamera miniMapCamera;
	public final Viewport miniMapViewport;

    public final IEvent eventWin = new Event();
    public final IEvent eventLoose = new Event();

    public void performWin()
    {
        ((Event)eventWin).doEvent(this, EventArgs.Empty);
    }

    public void performLoose()
    {
        ((Event)eventLoose).doEvent(this, EventArgs.Empty);
    }

	private static final float CheckBoundsInterval = 1f;
	private float _checkBoundsElapsedTime = 0f;

    //TODO перенести в SceneData
	public final Physics2D physics = new Physics2D(new Vector2(0f, -9.8f));

	public GameLvlScene(String gameLevelName, BoundingBox2D worldBounds)
	{
		super(gameLevelName, GameLvlSceneData.class, factoryMap, new PerspectiveCamera(worldBounds.getWidth(), worldBounds.getHeight(), 1f, 20f),
				new Viewport(), worldBounds);
		new GameLvlCameraController(this, camera);

		// устанавливаем основную камеру по центру сцены
		Vector2 center = worldBounds.getCenter();
		camera.transformation.setPosition(center.x, center.y, 5f);

		// задаем камере мини-карты в область видимости размером во всю сцену
		miniMapCamera = new OrthographicCamera(worldBounds.getWidth(), worldBounds.getHeight(), 1f, 20f);
		miniMapCamera.transformation.setPosition(center.x, center.y, 5f);
		miniMapViewport = new Viewport();
;	}

	/**
	 * Вызывается для инициализации после загрузки ресурсов
	 */
	@Override
	public void initialize()
	{
        super.initialize();

        // Создаем фигуры из данных о сцене
        for(HashMap.Entry shapeEntry : data.getShapes().entrySet())
        {
            EntityType shapeType = data.getType((String)shapeEntry.getKey());
            GameShape shape = (GameShape)((Entity) createInstance(shapeType));
            shape.transformation.set((Transformation)shapeEntry.getValue());
        }

        // Создаем режущую линию
        CutLine cutLine = new CutLine();
        addInstance(cutLine);
        new CutLineController(this, cutLine);

        // Вешаем контроллер на target volume
        //TODO: перенести контроллер в данные сцены
        for(int i = 0; i < entities.size(); i++)
        {
            Entity entity = entities.get(i);
            if(entity.type instanceof TargetVolumeType)
            {
                new TargetVolumeController(this, (GameShape)entity);
            }
        }
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);

		// пересчитываем область видимости камеры и обновляем вьюпорт мини-карты
		BoundingBox2D worldBounds = (BoundingBox2D)bounds;
		// корректируем высоту вьюпорта камеры чтобы сохранить соотношение сторон
		camera.setViewportHeight(viewport.getExpectedHeight(worldBounds.getWidth()));
		updateMiniMapViewport(width, height);
		Gdx.app.debug("test", "Обновлены вьюпорты");
	}

	@Override
	public void update(IGameTime gameTime)
	{
		super.update(gameTime);
		// Проверяем выход за границы уровня с определенным интервалом времени
		_checkBoundsElapsedTime += gameTime.getElapsedTime();
		if(_checkBoundsElapsedTime > CheckBoundsInterval)
		{
			_checkBoundsElapsedTime = 0f;

			for(int i = entities.size() - 1; i >= 0; i--)
			{
				Entity e = entities.get(i);
				if(e instanceof SpartialEntity2D)
				{
					SpartialEntity2D se = (SpartialEntity2D)e;

					// статические объекты не перемещаются и не могут улететь за границы
					if(!se.isStatic)
					{
						float x = se.transformation.getPositionX();
						float y = se.transformation.getPositionY();
						if(bounds.contains(x, y)) continue;

						// если объект улетел за границы удаляем его
						deleteInstance(e);
					}
				}
			}
		}

		physics.update(gameTime.getElapsedTime());
		miniMapCamera.update(gameTime);
	}

	@Override
	public void dispose()
	{
		super.dispose();
		
		if(physics != null)
			physics.dispose();
	}

	@Override
	public void deleteInstance(Entity entity)
	{
		GameShape shape = entity instanceof GameShape ? (GameShape)entity : null;
		if(shape != null && shape.transformation instanceof RigidBody)
		{
			physics.deleteBody((RigidBody) shape.transformation);
		}
		super.deleteInstance(entity);
	}

	/**
	 * Возвращает необходимую ширину области экрана для отображения сцены, по заданной высоте
	 * @param expectedViewportHeight Высота области экрана
	 * @return Ширина области экрана в которую поместится сцена
	 */
	public float getExpectedViewportWidth(float expectedViewportHeight)
	{
		BoundingBox2D worldBounds = (BoundingBox2D)bounds;
		return (expectedViewportHeight / worldBounds.getHeight()) * worldBounds.getWidth();
	}

	/**
	 * Возвращает необходимую высоту области экрана для отображения сцены, по заданной ширине
	 * @param expectedViewportWidth Ширина области экрана
	 * @return Высота области экрана в которую поместится сцена
	 */
	public float getExpectedViewportHeight(float expectedViewportWidth)
	{
		BoundingBox2D worldBounds = (BoundingBox2D)bounds;
		return (expectedViewportWidth / worldBounds.getWidth()) * worldBounds.getHeight();
	}

	private void updateMiniMapViewport(int screenWidth, int screenHeight)
	{
		// вычисляем размер вьюпорта для мини-карты в который полностью поместится сцена
		int miniMapViewportWidth = Math.round(screenWidth / 5);
		int miniMapViewportHeight = Math.round(getExpectedViewportHeight(miniMapViewportWidth));
		miniMapViewport.setScreenSize(miniMapViewportWidth, miniMapViewportHeight);
	}
}
