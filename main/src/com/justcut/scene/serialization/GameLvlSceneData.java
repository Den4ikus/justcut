package com.justcut.scene.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.jengine.game.EntityType;
import com.jengine.game.Transformation;
import com.jengine.game.scene.serialization.SceneData;

import java.util.HashMap;

/**
 * Сериализуемые данные игровой сцены
 */
public final class GameLvlSceneData extends SceneData
{
    private HashMap<String, Transformation> _shapes = new HashMap<String, Transformation>();

    public HashMap<String, Transformation> getShapes()
    {
        return _shapes;
    }

    /**
     * Конструктор без параметров для десериализации
     */
    protected GameLvlSceneData()
    {}

    public GameLvlSceneData(HashMap<String, EntityType> typeMap, HashMap<String, Transformation> shapes)
    {
        super(typeMap);
        _shapes = shapes;
    }

    //region Json.serializable

    @Override
    public void write(Json json)
    {
        super.write(json);
        json.writeValue("Shapes", _shapes);
    }

    @Override
    public void read(Json json, JsonValue jsonData)
    {
        super.read(json, jsonData);
        _shapes = json.readValue(HashMap.class, jsonData.get("Shapes"));
    }

    //endregion
}
