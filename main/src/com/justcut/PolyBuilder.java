package com.justcut;

import com.badlogic.gdx.math.Vector2;
import com.jengine.math.MathHelper;
import com.jengine.math.bounds.BoundingBox2D;
import com.jengine.math.bounds.BoundingCircle;

import java.util.ArrayList;
import java.util.Arrays;

public final class PolyBuilder
{
    private final static Vector2[] circleCorners = new Vector2[360];

    static
    {
        final float stepAngle = MathHelper.TwoPi / 360f;
        // рассчитываем единичные вектора по кругу в плоскости XY
		// элементов 361 для сокращения проверок
        for (int i = 0; i < 360; i++)
        {
            float angle = i * stepAngle;
            circleCorners[i] = new Vector2((float)Math.cos(angle), (float)Math.sin(angle));
        }
    }
	
	private PolyBuilder()
	{}
	
	public static Vector2[] createFromBox(BoundingBox2D box)
	{
		Vector2[] poly = new Vector2[4];
		return box.getCorners(poly);
	}
	
	public static Vector2[] createFromCircle(BoundingCircle circle, float accuracy)
	{
		return createCircle(circle.center.x, circle.center.y, circle.radius, accuracy);
	}
	
	public static Vector2[] createBox(float x, float y, float width, float height)
	{
		float halfWidth = width * 0.5f;
		float halfHeight = height * 0.5f;
		Vector2[] poly = new Vector2[]
		{
			new Vector2(x - halfWidth, y - halfHeight),
			new Vector2(x - halfWidth, y + halfHeight),
			new Vector2(x + halfWidth, y + halfHeight),
			new Vector2(x + halfWidth, y - halfHeight)
		};
		return poly;
	}
	
	public static Vector2[] createCircle(float x, float y, float radius, float accuracy)
	{
		int vertexCount = Math.round(MathHelper.TwoPi * radius / accuracy);
		float step = 360f / vertexCount;

		Vector2[] poly = new Vector2[vertexCount];
		for(int i = 0; i < vertexCount; i++)
		{
			float angle = i * step;
			int index = Math.round(angle);
			poly[i] = new Vector2(circleCorners[359 - index]).scl(radius);
		}
		return poly;
	}

	public static Vector2[] createConvex(int vertexCount, float radius)
	{
		float accuracy = MathHelper.TwoPi * radius / vertexCount;
		float delta = MathHelper.InvSqrt2 * accuracy;
		Vector2[] convex = createCircle(0f,0f,radius, accuracy);
		/*for(int i = 0; i < convex.length; i++)
			convex[i].add(RandomHelper.floatValue(-delta, delta), RandomHelper.floatValue(-delta, delta));*/

		//if(!Poly.isConvex(convex)) return createConvex(vertexCount, radius);
		return convex;
	}

    public static Vector2[] optimizeConvex(Vector2[] convex, int vertexCount)
    {
        if(vertexCount >= convex.length) return convex;

        ArrayList<Vector2> optimizedConvex = new ArrayList<Vector2>(Arrays.asList(convex));
        // Находим грани минимальной длины и удаляем, смежные точки объединяются в одну в средней точке грани
        while(optimizedConvex.size() > vertexCount)
        {
            float minLength = MathHelper.len2(optimizedConvex.get(0), optimizedConvex.get(optimizedConvex.size() - 1));
            int startMinEdgeIndex = optimizedConvex.size() - 1;
            for(int j = 0; j < optimizedConvex.size() - 1; j++)
            {
                float edgeLength = MathHelper.len2(optimizedConvex.get(j), optimizedConvex.get(j + 1));
                if(edgeLength < minLength)
                {
                    minLength = edgeLength;
                    startMinEdgeIndex = j;
                }
            }

            int endMinEdgeIndex = startMinEdgeIndex < optimizedConvex.size() - 1 ? startMinEdgeIndex + 1 : 0;
            Vector2 middlePoint = MathHelper.avg(optimizedConvex.get(startMinEdgeIndex), optimizedConvex.get(endMinEdgeIndex));

            // Заменяем начальную точку грани на среднюю точку грани, конечную точку удаляем
            optimizedConvex.set(startMinEdgeIndex, middlePoint);
            optimizedConvex.remove(endMinEdgeIndex);
        }

        return optimizedConvex.toArray(new Vector2[vertexCount]);
    }
}
