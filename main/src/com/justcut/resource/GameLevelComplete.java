package com.justcut.resource;

/**
 *
 */
public enum GameLevelComplete
{
    None,
    Bronze,
    Silver,
    Gold
}
