package com.justcut.resource;

/**
 *
 */
public class GameLevelDescription
{
    public String name;
    public String sceneName;
    public String previewImageName;
    public GameLevelComplete completeLevel;

    public GameLevelDescription(String name, String sceneName, String previewImageName, GameLevelComplete completeLevel)
    {
        this.name = name;
        this.sceneName = sceneName;
        this.previewImageName = previewImageName;
        this.completeLevel = completeLevel;
    }
}
