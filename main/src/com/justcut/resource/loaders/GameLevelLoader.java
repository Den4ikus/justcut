package com.justcut.resource.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.jengine.common.logs.Logger;
import com.justcut.scene.serialization.GameLvlSceneData;

/**
 * Загрузчик игровых уровней
 */
public class GameLevelLoader extends SynchronousAssetLoader<GameLvlSceneData, GameLevelLoader.ShaderLoaderParameter>
{
    public GameLevelLoader()
    {
        super(new FileHandleResolver()
        {
            @Override
            public FileHandle resolve(String fileName)
            {
                try
                {
                    FileHandle file = Gdx.files.internal(fileName);
                    Logger.info(GameLevelLoader.class.getSimpleName(), "resolve", fileName, "Succeed", "resource resolved");
                    return file;
                }
                catch (Exception ex)
                {
                    Logger.warning(GameLevelLoader.class.getSimpleName(), "resolve", fileName, "Failed", ex.getMessage());
                    return null;
                }
            }
        });
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, ShaderLoaderParameter parameter)
    {
        return null;
    }

    @Override
    public GameLvlSceneData load(AssetManager assetManager, String fileName, FileHandle file, ShaderLoaderParameter parameter)
    {
        try
        {
            Json json = new Json();
            GameLvlSceneData gameLevel = json.fromJson(GameLvlSceneData.class, file);

            Logger.info(GameLevelLoader.class.getSimpleName(), "load", fileName, "Succeed", "resource loaded");

            return gameLevel;
        }
        catch (Exception ex)
        {
            Logger.warning(GameLevelLoader.class.getSimpleName(), "load", fileName, "Failed", ex.getMessage());
            return null;
        }
    }

    public static class ShaderLoaderParameter extends AssetLoaderParameters<GameLvlSceneData>
    {}
}
