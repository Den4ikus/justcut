package com.justcut;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jengine.game.FPSCounter;
import com.jengine.game.Game;
import com.jengine.game.IGameTime;
import com.jengine.resources.ResHandle;
import com.jengine.screen.ScreenOverlapMode;
import com.justcut.resource.loaders.GameLevelLoader;
import com.justcut.scene.serialization.GameLvlSceneData;
import com.justcut.screens.LoadingScreen;
import com.justcut.screens.MainMenuScreen;

public final class JustCutGame extends Game
{
    private final FPSCounter _fpsCounter = new FPSCounter();
    private SpriteBatch _spriteBatch;
    private BitmapFont _fpsFont;

    @Override
    public void appInitialize()
    {
        super.appInitialize();

        Gdx.gl20.glEnable(GL20.GL_CULL_FACE);
        Gdx.gl20.glFrontFace(GL20.GL_CW);
        Gdx.gl20.glCullFace(GL20.GL_BACK);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl20.glEnable(GL20.GL_BLEND);
        Gdx.gl20.glEnable(GL20.GL_DEPTH_TEST);
        Gdx.gl20.glClearDepthf(1f);
        Gdx.gl20.glDepthFunc(GL20.GL_LEQUAL);
        Gdx.gl20.glDepthMask(true);

        // Добавляем загрузчик уровней
        resourceManager.setLoader(GameLvlSceneData.class, new GameLevelLoader());

        ResHandle<BitmapFont> font = resourceManager.get("FPSFont", BitmapFont.class);
        resourceManager.waitLoad();

        _fpsFont = font.get();
        _spriteBatch = new SpriteBatch(100);

        MainMenuScreen mainMenuScreen = new MainMenuScreen(this, ScreenOverlapMode.Overlay);
        LoadingScreen loadingScreen = new LoadingScreen(this, ScreenOverlapMode.Overlay, mainMenuScreen);

        screenManager.addScreen(loadingScreen);
    }

    @Override
    public void appResize(int width, int height)
    {
        super.appResize(width, height);
        _spriteBatch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);
    }

    @Override
    public void appRender(IGameTime gameTime)
    {
        super.appRender(gameTime);

        _fpsCounter.frame(gameTime.getElapsedTime());
        Gdx.gl20.glViewport(0,0,Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        _spriteBatch.begin();
        _spriteBatch.setColor(1f, 0f, 0f, 1f);
        String fps = String.format("%.2f FPS (%.2f ms)", _fpsCounter.getFPS(), gameTime.getElapsedMilliseconds());
        _fpsFont.draw(_spriteBatch, fps, 0, Gdx.graphics.getHeight());
        _spriteBatch.end();
    }
}
