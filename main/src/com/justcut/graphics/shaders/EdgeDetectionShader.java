package com.justcut.graphics.shaders;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.jengine.graphics.shaders.BaseShader;

/**
 * Шейдер реализующий постпроцессинг с помощью фильтра выделения граней
 */
public final class EdgeDetectionShader extends BaseShader
{
    private final int _u_texture;
    private final int _u_texelSize;

    private Texture _texture;
    private float[] _texelSize = new float[3];

    public Texture getTexture()
    {
        return _texture;
    }
    public void setTexture(Texture texture)
    {
        _texture = texture;
    }

    public void setTexelSize(float x, float y)
    {
        _texelSize[0] = x;
        _texelSize[1] = y;
    }

    public EdgeDetectionShader(ShaderProgram shader)
    {
        super(shader);
        _u_texture = shader.getUniformLocation("u_texture");
        _u_texelSize = shader.getUniformLocation("u_texelSize");
    }

    /**
     * Передает текущие настройки рендеринга в шейдер перед процессом рендеринга
     * @param shader
     */
    @Override
    protected void doSetParameters(ShaderProgram shader)
    {
        _texture.bind(0);
        shader.setUniformi(_u_texture, 0);
        shader.setUniform2fv(_u_texelSize, _texelSize, 0, 2);
    }

    /**
     * Передает текущие настройки рендеринга в шейдер в процессе рендеринга
     * @param shader
     */
    @Override
    protected void doChangeParameters(ShaderProgram shader)
    {
    }
}
