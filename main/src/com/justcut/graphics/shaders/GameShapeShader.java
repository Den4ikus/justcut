package com.justcut.graphics.shaders;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.jengine.graphics.shaders.BaseShader;

/**
 * Шейдер для рендеринга игровых фигур
 */
public final class GameShapeShader extends BaseShader
{
    /*private static final Vector3 tempVec3 = new Vector3();
    private static final Color tempColor = new Color();*/

    private final int _u_world;
    private final int _u_viewProj;
    private final int _u_lightPos;
    private final int _u_color;

    private Matrix4 _world;
    private Matrix4 _viewProj;
    private float[] _lightPos = new float[3];
    private float[] _color = new float[4];

    public Matrix4 getWorld()
    {
        return _world;
    }
    public void setWorld(Matrix4 world)
    {
        _world = world;
    }

    public Matrix4 getViewProj()
    {
        return _viewProj;
    }
    public void setViewProj(Matrix4 viewProj)
    {
        _viewProj = viewProj;
    }

    /*public Vector3 getLightPos()
    {
        tempVec3.set(_lightPos[0], _lightPos[1], _lightPos[2]);
        return tempVec3;
    }*/
    public void setLightPos(Vector3 lightPos)
    {
        _lightPos[0] = lightPos.x;
        _lightPos[1] = lightPos.y;
        _lightPos[2] = lightPos.z;
    }

    /*public Color getColor()
    {
        tempColor.set(color[0], color[1], color[2], 1f);
        return tempColor;
    }*/
    public void setColor(Color color)
    {
        _color[0] = color.r;
        _color[1] = color.g;
        _color[2] = color.b;
        _color[3] = color.a;
    }

    public GameShapeShader(ShaderProgram shader)
    {
        super(shader);
        _u_world = shader.getUniformLocation("u_world");
        _u_viewProj = shader.getUniformLocation("u_viewProj");
        _u_lightPos = shader.getUniformLocation("u_lightPos");
        _u_color = shader.getUniformLocation("u_color");
    }

    /**
     * Передает текущие настройки рендеринга в шейдер перед процессом рендеринга
     * @param shader
     */
    @Override
    protected void doSetParameters(ShaderProgram shader)
    {
        // Матрица viewProj и позиция источника света меняется только для всех фигур сразу
        shader.setUniformMatrix(_u_viewProj, _viewProj);
        shader.setUniform3fv(_u_lightPos, _lightPos, 0, 3);
    }

    /**
     * Передает текущие настройки рендеринга в шейдер в процессе рендеринга
     * @param shader
     */
    @Override
    protected void doChangeParameters(ShaderProgram shader)
    {
        // Матрица world и цвет меняется для каждой фигуры
        shader.setUniformMatrix(_u_world, _world);
        shader.setUniform4fv(_u_color, _color, 0, 4);
    }
}
