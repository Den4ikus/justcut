//package com.justcut.graphics.stages;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.graphics.GL20;
//import com.jengine.game.Entity;
//import com.jengine.graphics.RenderToScreenStage;
//import com.justcut.entities.GameShape;
//import com.justcut.scene.GameLvlScene;
//import com.justcut.scene.renderers.MeshRenderer;
//
///**
// *
// */
//public class GameLvlToScreenStage extends RenderToScreenStage<GameLvlScene>
//{
//	private MeshRenderer _renderer;
//
//    public GameLvlToScreenStage(MeshRenderer renderer)
//    {
//        _renderer = renderer;
//    }
//
//    @Override
//    public void render(GameLvlScene scene)
//    {
//        //Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
//        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
//        /*Gdx.gl.glEnable(GL20.GL_STENCIL_TEST);
//        Gdx.gl.glStencilFunc(GL20.GL_ALWAYS, 1, 1);
//        Gdx.gl.glStencilOp(GL20.GL_REPLACE, GL20.GL_REPLACE, GL20.GL_REPLACE);
//        Gdx.gl.glStencilMask(0xFF);*/
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT/* | GL20.GL_STENCIL_BUFFER_BIT*/);
//
//        scene.viewport.apply();
//
//        _renderer.begin(scene.camera, scene.camera.transformation.getPosition());
//        for(int i = 0; i < scene.entities.size(); i++)
//        {
//            Entity entity = scene.entities.get(i);
//            if (!(entity instanceof GameShape)) continue;
//
//            GameShape shape = (GameShape) entity;
//            _renderer.render(shape);
//			//_renderer.addMesh(shape.type.vertices, shape.color, shape.transformation.get());
//        }
//        _renderer.end();
//    }
//}
