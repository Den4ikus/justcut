//package com.justcut.graphics.stages;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.graphics.GL20;
//import com.badlogic.gdx.graphics.Pixmap;
//import com.jengine.game.Entity;
//import com.jengine.graphics.RenderToTextureStage;
//import com.justcut.entities.GameShape;
//import com.justcut.scene.GameLvlScene;
//import com.justcut.scene.renderers.MeshRenderer;
//
///**
// *
// */
//public class GameLvlToTextureStage extends RenderToTextureStage<GameLvlScene>
//{
//    private MeshRenderer _renderer;
//
//    public GameLvlToTextureStage(MeshRenderer renderer, int screenWidth, int screenHeight)
//    {
//        super(Pixmap.Format.RGB565, screenWidth, screenHeight, true);
//        _renderer = renderer;
//    }
//
//    @Override
//    protected void onRender(GameLvlScene scene)
//    {
//        //Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
//        Gdx.gl20.glClearColor(0f, 0f, 0f, 1f);
//        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
//
//        scene.viewport.apply();
//
//        _renderer.begin(scene.camera, scene.camera.transformation.getPosition());
//        for(int i = 0; i < scene.entities.size(); i++)
//        {
//            Entity entity = scene.entities.get(i);
//            if (!(entity instanceof GameShape)) continue;
//
//            GameShape shape = (GameShape) entity;
//            _renderer.render(shape);
//        }
//        _renderer.end();
//    }
//}
