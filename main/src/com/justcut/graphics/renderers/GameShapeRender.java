package com.justcut.graphics.renderers;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.jengine.common.EventArgs;
import com.jengine.common.IEventHandler;
import com.jengine.game.EntityType;
import com.jengine.graphics.RenderTarget;
import com.jengine.graphics.buffers.VertexStorage;
import com.jengine.graphics.buffers.VertexBufferSegment;
import com.jengine.graphics.renderers.BaseEntityRender;
import com.jengine.graphics.shaders.BaseShader;
import com.jengine.graphics.vertices.VertexPositionNormal;
import com.justcut.MeshBuilder;
import com.justcut.entities.GameShape;
import com.justcut.graphics.shaders.GameShapeShader;

import java.util.HashMap;

/**
 *
 */
public final class GameShapeRender extends BaseEntityRender<GameShape, GameShapeShader>
{
    private VertexStorage _vertexStorage = new VertexStorage(false, 256, 128, VertexPositionNormal.declaration);
    private HashMap<String, VertexBufferSegment> _vbMap = new HashMap<String, VertexBufferSegment>();

    private IEventHandler<EventArgs> _shapeDeleteHandler = new IEventHandler<EventArgs>()
    {
        @Override
        public void doHandle(Object sender, EventArgs args)
        {
            EntityType type = (EntityType)sender;
            if(type.getInstanceCount() == 0)
            {
                VertexBufferSegment segment = _vbMap.remove(type.getName());
                _vertexStorage.release(segment);
            }
        }
    };

    public GameShapeRender(GameShapeShader shader)
    {
        super(shader);
    }

    public void begin(RenderTarget target, boolean bindTarget, Matrix4 viewProj, Vector3 lightPos)
    {
        shader.setViewProj(viewProj);
        shader.setLightPos(lightPos);
        this.begin(target, bindTarget);
    }

    @Override
    public void render(GameShape entity)
    {
        // Ищем сегмент куда сохранен меш фигуры
        VertexBufferSegment segment = _vbMap.get(entity.type.getName());
        if(segment == null)
        {
            // Сегмент не найден, генерируем меш и сохраняем в свободный сегмент буфера
            VertexPositionNormal[] entityMesh = MeshBuilder.createFromPoly(entity.type.getVertices());
            segment = _vertexStorage.allocate(entityMesh);
            _vbMap.put(entity.type.getName(), segment);
            // Отслеживаем удаление инстансов типа т.к. если инстансов нет нужно освобождать занятый сегмент
            entity.type.eventDeleteInstance.attach(_shapeDeleteHandler);
        }

        shader.setWorld(entity.transformation.get());
        shader.setColor(entity.color);
        shader.render(BaseShader.PrimitiveType.Triangles, segment, true);
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_vertexStorage != null)
            _vertexStorage.dispose();
    }
}
