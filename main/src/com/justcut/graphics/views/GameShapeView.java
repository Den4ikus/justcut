package com.justcut.graphics.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.jengine.game.Entity;
import com.jengine.graphics.ISceneView;
import com.jengine.graphics.RenderTarget;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.justcut.entities.GameShape;
import com.justcut.entities.types.TargetVolumeType;
import com.justcut.graphics.processors.EdgeDetectionPostProcessor;
import com.justcut.graphics.renderers.GameShapeRender;
import com.justcut.graphics.shaders.EdgeDetectionShader;
import com.justcut.graphics.shaders.GameShapeShader;
import com.justcut.scene.GameLvlScene;

/**
 * Представление игровых фигур на сцене
 */
public final class GameShapeView implements ISceneView<GameLvlScene>
{
    private GameShapeRender _gameShapeRender;
    private EdgeDetectionPostProcessor _edgeDetectionPostProcessor;
    private RenderTarget _rt;

    //region Resources

    private ResHandle<ShaderProgram> _gameShapeShader;
    private ResHandle<ShaderProgram> _edgeDetectionShader;

    //endregion

    public GameShapeView()
    {
    }

    /**
     * Вызывается для инициализации после загрузки ресурсов
     */
    @Override
    public void initialize()
    {
        _gameShapeRender = new GameShapeRender(new GameShapeShader(_gameShapeShader.get()));
        _edgeDetectionPostProcessor = new EdgeDetectionPostProcessor(new EdgeDetectionShader(_edgeDetectionShader.get()));
    }

    /**
     * Вызывается для загрузки ресурсов
     *
     * @param resourceManager
     */
    @Override
    public void load(ResourceManager resourceManager)
    {
        _gameShapeShader = resourceManager.get("RGBMeshShader", ShaderProgram.class);
        _edgeDetectionShader = resourceManager.get("RobertsFilterShader", ShaderProgram.class);
    }

    /**
     * Вызывается для выгрузки ресурсов
     *
     * @param resourceManager
     */
    @Override
    public void unload(ResourceManager resourceManager)
    {
        resourceManager.free(_gameShapeShader);
    }

    /**
     * Вызывается при изменении размеров окна
     *
     * @param width
     * @param height
     */
    @Override
    public void resize(int width, int height)
    {
        if(_rt != null)
        {
            _rt.dispose();
            _rt = null;
        }

        int rtWidth = (int)(width * 0.8f);
        int rtHeight = (int)(width * 0.8f);
        _rt = new RenderTarget(Pixmap.Format.RGBA8888, rtWidth, rtHeight, true);
    }

    @Override
    public void render(GameLvlScene scene)
    {
        scene.viewport.apply();

        Gdx.gl20.glEnable(GL20.GL_BLEND);
        
        // Чистим область отрисовки
        Gdx.gl20.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | GL20.GL_STENCIL_BUFFER_BIT);

        _gameShapeRender.begin(_rt, true, scene.camera.viewProj, scene.camera.transformation.getPosition());

        // Очищаем рендер таргет
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        GameShape targetVolume = null;
        for(int i = 0; i < scene.entities.size(); i++)
        {
            Entity entity = scene.entities.get(i);
            if(entity instanceof GameShape)
            {
                if(entity.type instanceof TargetVolumeType)
                {
                    targetVolume = (GameShape)entity;
                    continue;
                }
                _gameShapeRender.render((GameShape)entity);
            }
        }
        _gameShapeRender.render(targetVolume);
        _gameShapeRender.end(true);

        _edgeDetectionPostProcessor.render(_rt.getTexture().getTexture());

    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_gameShapeRender != null)
            _gameShapeRender.dispose();
    }
}