package com.justcut.graphics.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.jengine.game.Entity;
import com.jengine.graphics.ISceneView;
import com.jengine.graphics.ShapeRenderer;
import com.jengine.math.bounds.BoundingBox2D;
import com.jengine.math.bounds.BoundingCircle;
import com.jengine.math.bounds.BoundingPoly;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.justcut.entities.GameShape;
import com.justcut.graphics.renderers.GameShapeRender;
import com.justcut.graphics.shaders.GameShapeShader;
import com.justcut.scene.GameLvlScene;

/**
 * Рендер мини-карты игрового уровня
 */
public final class GameLvlMiniMapView implements ISceneView<GameLvlScene>
{
    private GameShapeRender _gameShapeRender;
    private ShapeRenderer _renderer;

    //region Resources

    private ResHandle<ShaderProgram> _gameShapeShader;
    private ResHandle<ShaderProgram> _shapeContourShader;

    //endregion

    private Vector3 lightPos = new Vector3(10, 10, 10);

	public GameLvlMiniMapView()
	{
	}

    /**
     * Вызывается для инициализации после загрузки ресурсов
     */
    @Override
    public void initialize()
    {
        _gameShapeRender = new GameShapeRender(new GameShapeShader(_gameShapeShader.get()));
        _renderer = new ShapeRenderer(_shapeContourShader.get(), 1000);
    }

    /**
     * Вызывается для загрузки ресурсов
     *
     * @param resourceManager
     */
    @Override
    public void load(ResourceManager resourceManager)
    {
        _gameShapeShader = resourceManager.get("RGBMeshShader", ShaderProgram.class);
        _shapeContourShader = resourceManager.get("ShapeContourShader", ShaderProgram.class);
    }

    /**
     * Вызывается для выгрузки ресурсов
     *
     * @param resourceManager
     */
    @Override
    public void unload(ResourceManager resourceManager)
    {
        resourceManager.free(_shapeContourShader);
    }

    /**
     * Вызывается при изменении размеров окна
     *
     * @param width
     * @param height
     */
    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void render(GameLvlScene scene)
    {
		scene.miniMapViewport.apply();

		// устанавливаем область отсечения по вьюпорту т.к.
		// очистка должна идти только в пределах вьюпорта чтобы не захерачить уже отрисованную сцену вне вьюпорта

		Gdx.gl.glScissor(scene.miniMapViewport.getScreenX(),
                scene.miniMapViewport.getScreenY(),
                scene.miniMapViewport.getScreenWidth(),
                scene.miniMapViewport.getScreenHeight());
        Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST);
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST);

        /*_gameShapeRender.begin(null, false, scene.miniMapCamera.viewProj, lightPos);

        GameShape targetVolume = null;
        for(int i = 0; i < scene.entities.size(); i++)
        {
            Entity entity = scene.entities.get(i);
            if(entity instanceof GameShape)
            {
                if(entity.type instanceof TargetVolumeType)
                {
                    targetVolume = (GameShape)entity;
                    continue;
                }
                _gameShapeRender.render((GameShape)entity);
            }
        }
        _gameShapeRender.render(targetVolume);
        _gameShapeRender.end(false);*/

        for (Entity entity : scene.entities)
        {
            /*if(entity instanceof CutLine)
            {
                CutLine line = (CutLine)entity;
                _renderer.box(line.v1, line.v2, Color.NAVY);
            }*/

            if(!(entity instanceof GameShape)) continue;
            GameShape se = (GameShape) entity;

            if (se.bounds instanceof BoundingPoly)
                _renderer.poly((BoundingPoly)se.bounds, se.color);
            else if(se.bounds instanceof BoundingBox2D)
                _renderer.box((BoundingBox2D)se.bounds, se.color);
            else if(se.bounds instanceof BoundingCircle)
                _renderer.circle((BoundingCircle)se.bounds, se.color);
        }

        //_renderer.box((BoundingBox2D) scene.bounds, Color.RED);
        _renderer.render(scene.miniMapCamera);
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_renderer != null)
            _renderer.dispose();
    }
}
