package com.justcut.graphics.views;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.jengine.graphics.ISceneView;
import com.jengine.graphics.renderers.ScreenSpaceTextureRenderer;
import com.jengine.graphics.shaders.ScreenSpaceTextureShader;
import com.jengine.math.MathHelper;
import com.jengine.math.Vector;
import com.jengine.resources.ResHandle;
import com.jengine.resources.ResourceManager;
import com.justcut.entities.CutLine;
import com.justcut.scene.GameLvlScene;

/**
 * Представление режущей линии
 */
public class CutLineView implements ISceneView<GameLvlScene>
{
    private ScreenSpaceTextureRenderer _renderer;

    //region Resources

    private ResHandle<ShaderProgram> _shader;
    private ResHandle<TextureAtlas> _texAtlas;

    private TextureRegion _dottedTileTexture;
    private TextureRegion _siccorsTexture;

    //endregion

    /**
     * Вызывается для инициализации после загрузки ресурсов
     */
    @Override
    public void initialize()
    {
        _renderer = new ScreenSpaceTextureRenderer(new ScreenSpaceTextureShader(_shader.get()), 12);
        _dottedTileTexture = _texAtlas.get().findRegion("dotted_tile");
        _siccorsTexture = _texAtlas.get().findRegion("siccors");
    }

    /**
     * Вызывается для загрузки ресурсов
     *
     * @param resourceManager
     */
    @Override
    public void load(ResourceManager resourceManager)
    {
        _shader = resourceManager.get("ScreenSpaceTextureShader", ShaderProgram.class);
        _texAtlas = resourceManager.get("GrayscaleTexAtlas", TextureAtlas.class);

    }

    /**
     * Вызывается для выгрузки ресурсов
     *
     * @param resourceManager
     */
    @Override
    public void unload(ResourceManager resourceManager)
    {
        resourceManager.free(_shader);
        resourceManager.free(_texAtlas);
    }

    /**
     * Вызывается при изменении размеров окна
     *
     * @param width
     * @param height
     */
    @Override
    public void resize(int width, int height)
    {

    }

    /**
     * Реализует рендеринг сцены
     *
     * @param scene
     */
    @Override
    public void render(GameLvlScene scene)
    {
        _renderer.begin();

        for(int i = 0; i < scene.entities.size(); i++)
        {
            if(scene.entities.get(i) instanceof CutLine)
            {
                CutLine cutLine = (CutLine)scene.entities.get(i);
                if(cutLine.visible)
                {
                    // Вычисляем коэфф. тайлинга текстуры и рисуем режущую линию
                    float uScale = MathHelper.len(cutLine.screenV1, cutLine.screenV2) / 40f;
                    _renderer.draw(_dottedTileTexture, cutLine.screenV1, cutLine.screenV2, 5f, uScale, 1f);

                    // Вычисляем центральную точку положения ножниц и единичный вектор направления режущей линии
                    Vector2 siccorsPos = MathHelper.lerp(cutLine.screenV1, cutLine.screenV2, cutLine.siccorsPosition);
                    Vector2 offset = MathHelper.normalize(Vector.getVector2().set(cutLine.screenV2).sub(cutLine.screenV1)).scl(_siccorsTexture.getRegionWidth() * 0.5f);
                    // Получаем координаты точек между которыми отображать текстуру ножниц
                    Vector2 v1 = Vector.getVector2().set(siccorsPos).sub(offset);
                    Vector2 v2 = Vector.getVector2().set(siccorsPos).add(offset);
                    _renderer.draw(_siccorsTexture, v1, v2, _siccorsTexture.getRegionHeight());

                    Vector.put(siccorsPos);
                    Vector.put(offset);
                    Vector.put(v1);
                    Vector.put(v2);
                }
                break;
            }
        }

        _renderer.end();
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_renderer != null)
            _renderer.dispose();
    }
}
