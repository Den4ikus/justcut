package com.justcut.graphics.views;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.jengine.cameras.Camera;
import com.jengine.graphics.ISceneView;
import com.jengine.resources.ResourceManager;
import com.justcut.scene.GameLvlScene;

/**
 *
 */
public final class Box2DDebugView implements ISceneView<GameLvlScene>
{
    private SpriteBatch _spriteBatch;
    private Box2DDebugRenderer _box2dRenderer;
    private Matrix4 _proj = new Matrix4();

    /**
     * Вызывается для инициализации после загрузки ресурсов
     */
    @Override
    public void initialize()
    {
        _spriteBatch = new SpriteBatch();
        _box2dRenderer = new Box2DDebugRenderer(true, false, false, false, true, true);
    }

    /**
     * Вызывается для загрузки ресурсов
     *
     * @param resourceManager
     */
    @Override
    public void load(ResourceManager resourceManager)
    {

    }

    /**
     * Вызывается для выгрузки ресурсов
     *
     * @param resourceManager
     */
    @Override
    public void unload(ResourceManager resourceManager)
    {

    }

    /**
     * Вызывается при изменении размеров окна
     *
     * @param width
     * @param height
     */
    @Override
    public void resize(int width, int height)
    {

    }

    /**
     * Реализует рендеринг сцены
     *
     * @param scene
     */
    @Override
    public void render(GameLvlScene scene)
    {
        Camera camera = scene.camera;
        float halfWidth =  camera.getViewportWidth() * 0.5f;
        float halfHeight = camera.getViewportHeight() * 0.5f;
        _proj.setToOrtho(-halfWidth, halfWidth, -halfHeight, halfHeight, camera.getNear(), camera.getFar());

        _spriteBatch.begin();
        _box2dRenderer.render(scene.physics.getBox2dWorld(), _proj.mul(scene.camera.view));
        _spriteBatch.end();
    }

    /**
     * Releases all resources of this object.
     */
    @Override
    public void dispose()
    {
        if(_spriteBatch != null)
            _spriteBatch.dispose();

        if(_box2dRenderer != null)
            _box2dRenderer.dispose();
    }
}
