package com.justcut.graphics.processors;

import com.badlogic.gdx.graphics.Texture;
import com.jengine.graphics.processors.PostProcessor;
import com.justcut.graphics.shaders.EdgeDetectionShader;

/**
 *
 */
public final class EdgeDetectionPostProcessor extends PostProcessor<EdgeDetectionShader>
{
    public EdgeDetectionPostProcessor(EdgeDetectionShader shader)
    {
        super(shader);
    }

    /**
     * Передает текущие настройки рендеринга в шейдер
     *
     * @param shader
     * @param source
     */
    @Override
    protected void doChangeParameters(EdgeDetectionShader shader, Texture source)
    {
        shader.setTexelSize(1f / source.getWidth(), 1f / source.getHeight());
        shader.setTexture(source);
    }
}
