package com.justcut.entities;

import com.badlogic.gdx.math.Vector2;
import com.jengine.game.Entity;
import com.jengine.game.EntityType;
import com.jengine.game.IGameTime;

/**
 * Режущая линия
 */
public final class CutLine extends Entity
{
    public final Vector2 v1 = new Vector2();
    public final Vector2 v2 = new Vector2();
    public final Vector2 screenV1 = new Vector2();
    public final Vector2 screenV2 = new Vector2();
    public float siccorsPosition;

	public CutLine()
	{
		super(new EntityType("CutLineType"));
	}

	@Override
	public void update(IGameTime gameTime)
	{
	}
}
