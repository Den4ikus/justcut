package com.justcut.entities.controllers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.jengine.cameras.Camera;
import com.jengine.game.EntityInputController;
import com.jengine.game.IGameTime;
import com.jengine.input.InputTouchEventArgs;
import com.jengine.input.InputTouchMoveEventArgs;
import com.justcut.scene.GameLvlScene;

/**
 * Контроллер камеры игровой сцены
 */
public final class GameLvlCameraController extends EntityInputController<GameLvlScene, Camera>
{
	private Vector3 _startCameraPosition = new Vector3();
	private Vector3 _touchDownPosition = new Vector3();
	private boolean _touched = false;
	
	public GameLvlCameraController(GameLvlScene scene, Camera camera)
	{
		super(scene, camera);
	}

    @Override
	public boolean touchDown(InputTouchEventArgs args)
	{
		if(args.pointer == 0)
		{
			_touched = true;
			_touchDownPosition.set(scene.miniMapCamera.toWorld(args.position, 0.5f, scene.miniMapViewport));
			_startCameraPosition.set(scene.camera.transformation.getPosition());
		}
		return false;
	}
	
	@Override
	public boolean touchUp(InputTouchEventArgs args)
	{
		if(args.pointer == 0)
		{
			_touched = false;
		}
		return false;
	}
	
	@Override
	public boolean touchMove(InputTouchMoveEventArgs args)
	{
		// перемещаем камеру по указателю на мини-карте
		if(_touched)
		{
			Vector2 point = scene.miniMapViewport.toViewport(args.start);
			if(scene.miniMapViewport.contains(point))
			{
				point = scene.miniMapViewport.toViewport(args.current);
				if(scene.miniMapViewport.contains(point))
				{
					// обе точки находятся в пределах вьюпорта мини-карты
					Vector3 currentPoint = scene.miniMapCamera.toWorld(args.current, 0f, scene.miniMapViewport);
					entity.transformation.setPosition(
							_startCameraPosition.x + (currentPoint.x - _touchDownPosition.x),
							_startCameraPosition.y + (currentPoint.y - _touchDownPosition.y),
							entity.transformation.getPositionZ());
				}
			}
		}
		return false;
	}

	@Override
	public boolean scroll(int scrollValue)
	{
		final float nearBorder = 3f;
		final float farBorder = 20f;
		final float moveVelocity = 1f;

		float deltaZ =  scrollValue * moveVelocity;
		Vector3 position = entity.transformation.getPosition();
		if(deltaZ > 0 && position.z >= farBorder) return false;
		if(deltaZ < 0 && position.z <= nearBorder) return false;
		entity.transformation.setPosition(position.x, position.y, position.z + deltaZ);

		return false;
	}

    /**
     * Вызывается при обновлении сцены
     * @param gameTime
     */
    @Override
    protected void onUpdate(IGameTime gameTime)
    {

    }
}
