package com.justcut.entities.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.jengine.ColorHelper;
import com.jengine.collections.linq.IPredicate;
import com.jengine.collections.linq.ISelector;
import com.jengine.collections.linq.Linq;
import com.jengine.game.EntityInputController;
import com.jengine.game.IGameTime;
import com.jengine.game.SpartialEntity2D;
import com.jengine.input.InputTouchEventArgs;
import com.jengine.input.InputTouchMoveEventArgs;
import com.jengine.math.*;
import com.jengine.physics.RigidBody;
import com.justcut.PolyBuilder;
import com.justcut.entities.CutLine;
import com.justcut.entities.GameShape;
import com.justcut.entities.types.PhysicalPolyShapeType;
import com.justcut.entities.types.PhysicalShapeType;
import com.justcut.scene.GameLvlScene;

import java.util.ArrayList;

/**
 * Контроллер режущей линии
 */
public final class CutLineController extends EntityInputController<GameLvlScene, CutLine>
{
	private IPredicate<SpartialEntity2D> _shapeFilter = new IPredicate<SpartialEntity2D>()
	{
		@Override
		public boolean evaluate(SpartialEntity2D value)
		{
			return value instanceof GameShape && value.type instanceof PhysicalShapeType && !((GameShape)value).type.getIsStatic();
		}		
	};

    private ISelector<SpartialEntity2D, Float> _shapeMassSelector = new ISelector<SpartialEntity2D, Float>()
    {
        @Override
        public Float select(SpartialEntity2D value)
        {
            RigidBody rigidBody = (RigidBody)(value.transformation);
            return rigidBody.getMass();
        }
    };

    private boolean _siccorsAnimationEnabled = false;
    private float _siccorsAnimationElapsedTime = 0f;

	//private Line _siccorLine = new Line();
	private Ray _startRay = new Ray(Vector3.Zero, Vector3.Zero);
	private Ray _endRay = new Ray(Vector3.Zero, Vector3.Zero);
    private Vector2 _touchUpPos = new Vector2();
    private boolean _splitTriggered = false;

    private Ray2D _ray = new Ray2D();
    private ArrayList<SpartialEntity2D> _partList = new ArrayList<SpartialEntity2D>();
	
	public CutLineController(GameLvlScene scene, CutLine entity)
	{
		super(scene, entity);
	}

    @Override
	public boolean touchDown(InputTouchEventArgs args)
	{
        entity.visible = true;
        // Инвертируем ось Y т.к. координаты оконные
        entity.screenV1.set(args.position.x, Gdx.graphics.getHeight() - args.position.y);
        entity.screenV2.set(args.position.x, Gdx.graphics.getHeight() - args.position.y);

		//float z = scene.camera.transformation.getPositionZ() / scene.camera.getFar();
		Vector3 worldPos = scene.camera.toWorld(args.position, 1f, scene.viewport);
		_startRay.origin.set(scene.camera.transformation.getPosition());
		_startRay.direction.set(worldPos).sub(_startRay.origin);

		Vector3 temp = Vector.getVector3();

		MathHelper.normalize(_startRay.direction);
		Intersector.intersectRayPlane(_startRay, Planes.XY, temp);
        entity.v1.set(temp.x, temp.y);

		Vector.put(temp);

        _siccorsAnimationEnabled = false;
        _siccorsAnimationElapsedTime = 0f;
        entity.siccorsPosition = 0f;

		return false;
	}

	@Override
	public boolean touchUp(InputTouchEventArgs args)
	{
        _touchUpPos.set(args.position);
        // Инвертируем ось Y т.к. координаты оконные
        entity.screenV2.set(args.position.x, Gdx.graphics.getHeight() - args.position.y);
		//float z = scene.camera.transformation.getPositionZ() / scene.camera.getFar();
		Vector3 pos = scene.camera.toWorld(args.position, 1f, scene.viewport);
		_endRay.origin.set(scene.camera.transformation.getPosition());
		_endRay.direction.set(pos).sub(_endRay.origin);

		Vector3 temp = Vector.getVector3();

		MathHelper.normalize(_endRay.direction);
		Intersector.intersectRayPlane(_endRay, Planes.XY, temp);
        entity.v2.set(temp.x, temp.y);

		Vector.put(temp);

		//_siccorLine.v2.set(v1.x, v1.y);

		if(MathHelper.equals(entity.v1, entity.v2, 0.5f))
		{
            Vector2[] newShape = PolyBuilder.createConvex(RandomHelper.intValue(3, 8), RandomHelper.floatValue(1f, 1.5f));
            Color shapeColor;
            while(ColorHelper.brightness(shapeColor = RandomHelper.color()) > 0.7)
            {}
            PhysicalPolyShapeType newType = new PhysicalPolyShapeType(RandomHelper.string(4), false, newShape, shapeColor, 0.5f, 0.2f, RandomHelper.floatValue(0.1f, 0.5f));
            SpartialEntity2D newEntity = (SpartialEntity2D) scene.createInstance(newType);
            //GameShape shape = scene.createInstance("circle", GameShape.class);
            newEntity.transformation.setPosition(entity.v2.x, entity.v2.y, 0f);

            entity.visible = false;
		}
        else
        {
            _siccorsAnimationEnabled = true;
            _siccorsAnimationElapsedTime = 0f;
            entity.siccorsPosition = 0f;
            _splitTriggered = false;
        }

		return false;
	}

	@Override
	public boolean touchMove(InputTouchMoveEventArgs args)
	{
        // Инвертируем ось Y т.к. координаты оконные
        entity.screenV2.set(args.current.x, Gdx.graphics.getHeight() - args.current.y);
		//float z = scene.camera.transformation.getPositionZ() / scene.camera.getFar();
		Vector3 pos = scene.camera.toWorld(args.current, 1f, scene.viewport);
		_endRay.origin.set(scene.camera.transformation.getPosition());
		_endRay.direction.set(pos).sub(_endRay.origin);

		Vector3 temp = Vector.getVector3();

		MathHelper.normalize(_endRay.direction);
		Intersector.intersectRayPlane(_endRay, Planes.XY, temp);
        entity.v2.set(temp.x, temp.y);

		Vector.put(temp);

		return false;
	}

	@Override
	public boolean scroll(int scrollValue)
	{
		return false;
	}

    /**
     * Вызывается при обновлении сцены
     * @param gameTime
     */
    @Override
    protected void onUpdate(IGameTime gameTime)
    {
        if(!_siccorsAnimationEnabled) return;

        final float SiccorsAnimationTime = 0.5f;
        if(_siccorsAnimationElapsedTime <= SiccorsAnimationTime)
        {
            _siccorsAnimationElapsedTime += gameTime.getElapsedTime();
            entity.siccorsPosition = _siccorsAnimationElapsedTime / SiccorsAnimationTime;

            if(!_splitTriggered && entity.siccorsPosition > 0.5f)
            {
                doCutShapes();
                _splitTriggered = true;
            }
        }
        else
        {
            _siccorsAnimationEnabled = false;
            entity.visible = false;
        }
    }

    private void doCutShapes()
    {
        if(!MathHelper.equals(entity.v1, entity.v2, 0.5f))
        {
            Vector2 cursorPos = scene.miniMapViewport.toViewport(_touchUpPos);
            if (!scene.miniMapViewport.contains(cursorPos))
            {
                // Находим ограничивающий прямоугольник между двумя точкам через которые проходит линия разреза
                _ray.set(entity.v1, entity.v2);
                final ArrayList<GameShape> shapes = scene.rayCast(_ray, _shapeFilter).cast(GameShape.class).toList();
                for (GameShape shape : shapes)
                {
                    // TODO: исправить это блядство)
                    Line localLine = Line.get();
                    Vector2 localV1 = shape.transformation.toLocal(entity.v1);
                    Vector2 localV2 = shape.transformation.toLocal(entity.v2);
                    localLine.v1.set(localV1);
                    localLine.v2.set(localV2);
                    Vector.put(localV1);
                    Vector.put(localV2);

                    Vector2[][] parts = Poly.split(shape.type.getVertices(), localLine);
                    if (parts != null)
                    {
                        // Удаляем мелкие ребра полигонов
                        // т.к. количество граней слишком большое геометрия может не войти в сегмент вершинного буфера
                        Vector2[] optimizedPart1 = PolyBuilder.optimizeConvex(parts[0], 8);
                        Vector2[] optimizedPart2 = PolyBuilder.optimizeConvex(parts[1], 8);

                        PhysicalShapeType shapeType = (PhysicalShapeType)shape.type;
                        PhysicalPolyShapeType firstPartType = new PhysicalPolyShapeType(RandomHelper.string(4), false, optimizedPart1, shape.color, shapeType.getDensity(), shapeType.getFriction(), shapeType.getRestitution());
                        PhysicalPolyShapeType secondPartType = new PhysicalPolyShapeType(RandomHelper.string(4), false, optimizedPart2, shape.color, shapeType.getDensity(), shapeType.getFriction(), shapeType.getRestitution());

                        SpartialEntity2D part1 = (SpartialEntity2D) scene.createInstance(firstPartType);
                        if(part1 != null)
                        {
                            part1.transformation.set(shape.transformation);
                            _partList.add(part1);
                        }

                        SpartialEntity2D part2 = (SpartialEntity2D) scene.createInstance(secondPartType);
                        if(part2 != null)
                        {
                            part2.transformation.set(shape.transformation);
                            _partList.add(part2);
                        }

                        scene.deleteInstance(shape);
                        if(_partList.contains(shape))
                            _partList.remove(shape);
                    }

                    Line.put(localLine);
                }

                // Проверяем что количество частей не слишком большое т.к. размер вершинного буфера для отрисовки ограничен
                if(_partList.size() >= 20)
                {
                    int partsForDelete = _partList.size() - 20;
                    // Удаляем самые мелкие части фигур
                    for(SpartialEntity2D part : Linq.orderBy(_partList, _shapeMassSelector).take(partsForDelete))
                    {
                        scene.deleteInstance(part);
                        _partList.remove(part);
                    }
                }
            }
            else entity.visible = false;
        }
    }
}