package com.justcut.entities.controllers;

import com.jengine.collections.linq.IPredicate;
import com.jengine.collections.linq.Linq;
import com.jengine.game.Entity;
import com.jengine.game.EntityController;
import com.jengine.game.IGameTime;
import com.justcut.entities.GameShape;
import com.justcut.scene.GameLvlScene;

/**
 *
 */
public final class TargetVolumeController extends EntityController<GameLvlScene, GameShape>
{
    private final IPredicate<Entity> _greenShapeFilter = new IPredicate<Entity>()
    {
        @Override
        public boolean evaluate(Entity value)
        {
            return value.type != null ? value.type.getName().equals("GreenShape") : false;
        }
    };

    private float _alphaValue;
    private float _elapsedTime;
    private float _containsTime;

    public TargetVolumeController(GameLvlScene scene, GameShape entity)
    {
        super(scene, entity);
        _alphaValue = entity.color.a;
    }

    /**
     * Вызывается при обновлении сцены
     *
     * @param gameTime
     */
    @Override
    protected void onUpdate(IGameTime gameTime)
    {
        _elapsedTime += gameTime.getElapsedTime();
        entity.color.a = 0.1f + _alphaValue * Math.abs((float)Math.sin(_elapsedTime));

        Entity targetGreenEntity = Linq.first(scene.entities, _greenShapeFilter);
        if(targetGreenEntity != null)
        {
            if(entity.bounds.contains(((GameShape)targetGreenEntity).bounds))
            {
                _containsTime += gameTime.getElapsedTime();
                if (_containsTime > 3f)
                {
                    scene.performWin();
                }
            }
            else _containsTime = 0f;

            float scaleValue = 0.05f * (float)Math.sin(_containsTime * 10f);
            entity.transformation.setScale(1f + scaleValue, 1f + scaleValue, entity.transformation.getScaleZ());
        }
    }

    /**
     * Вызывается при удалении контролируемой сущности
     */
    @Override
    protected void onDelete()
    {

    }
}
