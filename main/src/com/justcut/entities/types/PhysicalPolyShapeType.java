package com.justcut.entities.types;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Тип игровой фигуры - многоугольник с физическим взаимодействием
 */
public final class PhysicalPolyShapeType extends PhysicalShapeType
{
    private PhysicalPolyShapeType()
    {}

    public PhysicalPolyShapeType(String typeName, boolean isStatic, Vector2[] vertices, Color color, float density, float friction, float restitution)
    {
        super(typeName, isStatic, vertices, color, density, friction, restitution);
    }
}
