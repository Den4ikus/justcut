package com.justcut.entities.types;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.justcut.PolyBuilder;

/**
 * Тип игровой фигуры - прямоугольник с физическим взаимодействием
 */
public final class PhysicalBoxShapeType extends PhysicalShapeType
{
	private float width;
	private float height;

    //region Properties

    public float getWidth()
    {
        return width;
    }

    public float getHeight()
    {
        return height;
    }

    //endregion

    /**
     * Конструктор без параметров для десериализации
     */
    protected PhysicalBoxShapeType()
    {}

    public PhysicalBoxShapeType(String typeName, float width, float height, Color color, float density, float friction, float restitution)
    {
		super(typeName, false, PolyBuilder.createBox(0f, 0f, width, height), color, density, friction, restitution);
		this.width = width;
		this.height = height;
    }

    //region Json.Serializable

    @Override
    public void write(Json json)
    {
        // Выпиливаем вершины чтобы не сериализовались
        Vector2[] temp = vertices;
        vertices = null;
        super.write(json);
        vertices = temp;

        json.writeValue("width", width);
        json.writeValue("height", height);
    }

    @Override
    public void read(Json json, JsonValue jsonMap)
    {
        super.read(json, jsonMap);
        width = jsonMap.get("width").asFloat();
        height = jsonMap.get("height").asFloat();
        vertices = PolyBuilder.createBox(0f, 0f, width, height);
    }

    //endregion
}
