package com.justcut.entities.types;

/**
 * Тип фигуры в которую нужно поместить TargetShape чтобы выйграть
 */
public class TargetVolumeType extends PolyShapeType
{
    /**
     * Конструктор без параметров для десериализации
     */
    protected TargetVolumeType()
    {}
}
