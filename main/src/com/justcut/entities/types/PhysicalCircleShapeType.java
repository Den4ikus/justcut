package com.justcut.entities.types;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.justcut.PolyBuilder;

/**
 * Тип игровой фигуры - круг c физическим взаимодействием
 */
public final class PhysicalCircleShapeType extends PhysicalShapeType
{
	protected float radius;

    public float getRadius()
    {
        return radius;
    }

    /**
     * Конструктор без параметров для десериализации
     */
    protected PhysicalCircleShapeType()
    {}

    public PhysicalCircleShapeType(String typeName, float radius, Color color, float density, float friction, float restitution)
    {
		super(typeName, true, PolyBuilder.createCircle(0f, 0f, radius, 0.5f), color, density, friction, restitution);
		this.radius = radius;
    }

    //region Json.Serializable

    @Override
    public void write(Json json)
    {
        // Выпиливаем вершины чтобы не сериализовались
        Vector2[] temp = vertices;
        vertices = null;
        super.write(json);
        vertices = temp;

        json.writeValue("radius", radius);
    }

    @Override
    public void read(Json json, JsonValue jsonMap)
    {
        super.read(json, jsonMap);
        radius = jsonMap.get("radius").asFloat();
        vertices = PolyBuilder.createCircle(0f, 0f, radius, 0.5f);
    }

    //endregion
}
