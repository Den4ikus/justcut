package com.justcut.entities.types;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Тип игровой фигуры - многоугольник без физического взаимодействия
 */
public class PolyShapeType extends BaseShapeType
{
    /**
     * Конструктор без параметров для десериализации
     */
    protected PolyShapeType()
    {}

    public PolyShapeType(String typeName, boolean isStatic, Vector2[] vertices, Color color)
    {
        super(typeName, isStatic, vertices, color);
    }
}
