package com.justcut.entities.types;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.jengine.collections.linq.Linq;
import com.jengine.game.EntityType;

import java.util.ArrayList;

/**
 * Базовый тип игровой фигуры
 */
public abstract class BaseShapeType extends EntityType
{
    protected boolean isStatic;
	protected Vector2[] vertices;
    protected Color color;

    //region Properties

    public boolean getIsStatic()
    {
        return isStatic;
    }

    public Vector2[] getVertices()
    {
        return vertices;
    }

    public Color getColor()
    {
        return color;
    }

    //endregion

    protected BaseShapeType()
    {}

    protected BaseShapeType(String typeName, boolean isStatic, Vector2[] vertices, Color color)
    {
        super(typeName);
		this.isStatic = isStatic;
		this.vertices = vertices;
		this.color = new Color(color);
    }

    //region Json.Serializable

    @Override
    public void write(Json json)
    {
        super.write(json);
        json.writeValue("isStatic", isStatic);
        json.writeValue("color", color);

        // Вершины могут быть не заданы т.к. для боксов и кругов они генерируются
        if(vertices != null)
        {
            json.writeArrayStart("vertices");
            for (Vector2 vertex : vertices)
            {
                json.writeValue(vertex);
            }
            json.writeArrayEnd();
        }
    }

    @Override
    public void read(Json json, JsonValue jsonMap)
    {
        super.read(json, jsonMap);
        isStatic = jsonMap.get("isStatic").asBoolean();
        color = json.readValue(Color.class, jsonMap.get("color"));

        // Вершины могут быть не заданы т.к. для боксов и кругов они генерируются
        if(jsonMap.has("vertices"))
        {
            ArrayList<Vector2> vertexList = new ArrayList<Vector2>();
            for (JsonValue jsonVertex : jsonMap.get("vertices").iterator())
            {
                vertexList.add(json.readValue(Vector2.class, jsonVertex));
            }
            vertices = Linq.toArray(vertexList, Vector2.class);
        }
    }

    //endregion
}
