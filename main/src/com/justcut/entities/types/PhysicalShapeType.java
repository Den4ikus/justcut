package com.justcut.entities.types;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Тип игровой фигуры с физическим взаимодействием
 */
public abstract class PhysicalShapeType extends BaseShapeType
{
    protected float density;
    protected float friction;
    protected float restitution;

    //region Properties

    public float getDensity()
    {
        return density;
    }

    public float getFriction()
    {
        return friction;
    }

    public float getRestitution()
    {
        return restitution;
    }

    //endregion

    protected PhysicalShapeType()
    {}

    protected PhysicalShapeType(String typeName, boolean isStatic, Vector2[] vertices, Color color, float density, float friction, float restitution)
    {
        super(typeName, isStatic, vertices, color);
        this.density = density;
        this.friction = friction;
        this.restitution = restitution;
    }

    //region Json.Serializable

    @Override
    public void write(Json json)
    {
        super.write(json);
        json.writeValue("density", density);
        json.writeValue("friction", friction);
        json.writeValue("restitution", restitution);
    }

    @Override
    public void read(Json json, JsonValue jsonMap)
    {
        super.read(json, jsonMap);
        density = jsonMap.get("density").asFloat();
        friction = jsonMap.get("friction").asFloat();
        restitution = jsonMap.get("restitution").asFloat();
    }

    //endregion
}
