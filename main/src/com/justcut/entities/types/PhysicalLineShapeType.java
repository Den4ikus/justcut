package com.justcut.entities.types;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.justcut.PolyBuilder;

/**
 * Тип игровой фигуры - линия с физическим взаимодействием
 */
public final class PhysicalLineShapeType extends PhysicalShapeType
{
    protected float width;
    protected float thickness;

    //region Properties

    public float getWidth()
    {
        return width;
    }

    public float getThickness()
    {
        return thickness;
    }

    //endregion

    /**
     * Конструктор без параметров для десериализации
     */
    protected PhysicalLineShapeType()
    {}

    public PhysicalLineShapeType(String typeName, float width, float thickness, Color color, float density, float friction, float restitution)
    {
        super(typeName, true, PolyBuilder.createBox(0f, 0f, width, thickness), color, density, friction, restitution);
        this.thickness = thickness;
        this.width = width;
    }

    //region Json.Serializable

    @Override
    public void write(Json json)
    {
        // Выпиливаем вершины чтобы не сериализовались
        Vector2[] temp = vertices;
        vertices = null;
        super.write(json);
        vertices = temp;

        json.writeValue("width", width);
        json.writeValue("thikness", thickness);
    }

    @Override
    public void read(Json json, JsonValue jsonMap)
    {
        super.read(json, jsonMap);
        width = jsonMap.get("width").asFloat();
        thickness = jsonMap.get("thikness").asFloat();
        vertices = PolyBuilder.createBox(0f, 0f, width, thickness);
    }

    //endregion
}
