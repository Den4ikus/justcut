package com.justcut.entities.factories;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.jengine.game.Entity;
import com.jengine.game.IEntityFactory;
import com.jengine.math.bounds.BoundingPoly;
import com.jengine.physics.RigidBody;
import com.justcut.entities.GameShape;
import com.justcut.entities.types.PhysicalPolyShapeType;
import com.justcut.scene.GameLvlScene;

/**
 * Фабрика создающая объекты полигонов которые являются физическими объектами
 */
public final class PhysicalPolyShapeFactory implements IEntityFactory<GameLvlScene, PhysicalPolyShapeType>
{
    /**
     * Создает инстанс сущности заданного типа на указанной сцене
     *
     * @param scene
     * @param type
     * @return
     */
    @Override
    public Entity<PhysicalPolyShapeType> create(GameLvlScene scene, PhysicalPolyShapeType type)
    {
        BoundingPoly poly = new BoundingPoly(type.getVertices());
        RigidBody body = scene.physics.createConvexPoly(type.getIsStatic() ? BodyDef.BodyType.StaticBody : BodyDef.BodyType.DynamicBody,
                Vector2.Zero,
                type.getVertices(),
                type.getDensity(),
                type.getFriction(),
                type.getRestitution());
        return body != null ? (Entity)new GameShape(type, body, poly) : null;
    }
}