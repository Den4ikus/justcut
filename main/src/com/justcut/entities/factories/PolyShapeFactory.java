package com.justcut.entities.factories;

import com.jengine.game.Entity;
import com.jengine.game.IEntityFactory;
import com.jengine.game.Transformation;
import com.jengine.math.bounds.BoundingPoly;
import com.justcut.entities.GameShape;
import com.justcut.entities.types.PolyShapeType;
import com.justcut.scene.GameLvlScene;

/**
 * Фабрика создающая многоугольники без физического взаимодействия
 */
public final class PolyShapeFactory implements IEntityFactory<GameLvlScene, PolyShapeType>
{
    /**
     * Создает инстанс сущности заданного типа на указанной сцене
     * @param scene
     * @param type
     * @return
     */
    @Override
    public Entity<PolyShapeType> create(GameLvlScene scene, PolyShapeType type)
    {
        BoundingPoly poly = new BoundingPoly(type.getVertices());
        return (Entity)new GameShape(type, new Transformation(), poly);
    }
}
