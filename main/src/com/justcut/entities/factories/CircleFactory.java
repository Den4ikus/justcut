package com.justcut.entities.factories;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.jengine.game.Entity;
import com.jengine.game.IEntityFactory;
import com.jengine.math.bounds.BoundingCircle;
import com.jengine.physics.RigidBody;
import com.justcut.entities.GameShape;
import com.justcut.entities.types.PhysicalCircleShapeType;
import com.justcut.scene.GameLvlScene;

public final class CircleFactory implements IEntityFactory<GameLvlScene,PhysicalCircleShapeType>
{
    /**
     * Создает инстанс сущности заданного типа на указанной сцене
     *
     * @param scene
     * @param type
     * @return
     */
    @Override
    public Entity<PhysicalCircleShapeType> create(GameLvlScene scene, PhysicalCircleShapeType type)
    {
        BoundingCircle circle = new BoundingCircle(0f, 0f, type.getRadius());
        RigidBody body = scene.physics.createCircle(BodyDef.BodyType.DynamicBody, Vector2.Zero, type.getRadius(), type.getDensity(), type.getFriction(), type.getRestitution());
        return (Entity)new GameShape(type, body, circle);
    }
}