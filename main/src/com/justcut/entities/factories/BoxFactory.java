package com.justcut.entities.factories;

import com.jengine.game.Entity;
import com.jengine.game.IEntityFactory;
import com.jengine.game.Transformation;
import com.jengine.math.bounds.BoundingBox2D;
import com.jengine.math.bounds.BoundingPoly;
import com.justcut.entities.GameShape;
import com.justcut.entities.types.PhysicalBoxShapeType;
import com.justcut.scene.GameLvlScene;

public final class BoxFactory implements IEntityFactory<GameLvlScene, PhysicalBoxShapeType>
{
    /**
     * Создает инстанс сущности заданного типа на указанной сцене
     *
     * @param scene
     * @param type
     * @return
     */
    @Override
    public Entity<PhysicalBoxShapeType> create(GameLvlScene scene, PhysicalBoxShapeType type)
    {
        BoundingBox2D box = new BoundingBox2D(0f, 0f, type.getWidth(), type.getHeight());
        return (Entity)new GameShape(type, new Transformation(), new BoundingPoly(box));
    }
}