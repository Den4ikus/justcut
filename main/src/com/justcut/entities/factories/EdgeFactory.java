package com.justcut.entities.factories;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.jengine.game.Entity;
import com.jengine.game.IEntityFactory;
import com.jengine.math.bounds.BoundingBox2D;
import com.jengine.math.bounds.BoundingPoly;
import com.jengine.physics.RigidBody;
import com.justcut.entities.GameShape;
import com.justcut.entities.types.PhysicalLineShapeType;
import com.justcut.scene.GameLvlScene;

public final class EdgeFactory implements IEntityFactory<GameLvlScene,PhysicalLineShapeType>
{
    /**
     * Создает инстанс сущности заданного типа на указанной сцене
     *
     * @param scene
     * @param type
     * @return
     */
    @Override
    public Entity<PhysicalLineShapeType> create(GameLvlScene scene, PhysicalLineShapeType type)
    {
        float halfWidth = type.getWidth() * 0.5f;
        float halfHeight = type.getThickness() * 0.5f;
        BoundingBox2D box = new BoundingBox2D(-halfWidth, -halfHeight, type.getWidth(), type.getThickness());
        RigidBody body = scene.physics.createEdge(
                BodyDef.BodyType.StaticBody,
                Vector2.Zero,
                new Vector2(-halfWidth, 0),
                new Vector2(halfWidth, 0f),
                type.getDensity(),
                type.getFriction(),
                type.getRestitution());
        return (Entity)new GameShape(type, body, new BoundingPoly(box));
    }
}