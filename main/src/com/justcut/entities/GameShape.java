package com.justcut.entities;

import com.badlogic.gdx.graphics.Color;
import com.jengine.game.IGameTime;
import com.jengine.game.ITransformationProvider;
import com.jengine.game.SpartialEntity2D;
import com.jengine.math.bounds.IBoundingShape2D;
import com.justcut.entities.types.BaseShapeType;

public class GameShape extends SpartialEntity2D<BaseShapeType>
{
	public Color color;
	
	public GameShape(BaseShapeType type, ITransformationProvider transformation, IBoundingShape2D boundingShape)
	{
		super(type, transformation, boundingShape, false);
		color = new Color(type.getColor());
	}
	
	@Override
	protected void onUpdate(IGameTime gameTime)
	{}
}
